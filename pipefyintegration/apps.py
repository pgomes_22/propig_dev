from django.apps import AppConfig


class PipefyintegrationConfig(AppConfig):
    name = 'pipefyintegration'
