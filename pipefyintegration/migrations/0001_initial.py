# Generated by Django 2.1.3 on 2018-11-27 18:27

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Clientes',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('flex', models.CharField(blank=True, max_length=255, null=True)),
                ('cartao_cnpj', models.CharField(blank=True, max_length=1024, null=True)),
                ('cnae', models.CharField(blank=True, max_length=255, null=True)),
                ('comprovante_atividade', models.CharField(blank=True, max_length=1024, null=True)),
                ('contrato_social', models.CharField(blank=True, max_length=1024, null=True)),
                ('documento_foto', models.CharField(blank=True, max_length=1024, null=True)),
                ('cnpj_cpf', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('ficha_cadastro', models.CharField(blank=True, max_length=1024, null=True)),
                ('update', models.DateTimeField(blank=True, null=True)),
                ('mcc', models.CharField(blank=True, max_length=255, null=True)),
                ('nome', models.CharField(blank=True, max_length=255, null=True)),
                ('ponto_venda', models.CharField(blank=True, max_length=255, null=True)),
                ('status', models.CharField(blank=True, max_length=255, null=True)),
                ('status_documentacao', models.CharField(blank=True, max_length=255, null=True)),
                ('tipo_pessoa', models.CharField(blank=True, max_length=255, null=True)),
                ('created', models.DateTimeField(blank=True, null=True)),
                ('inscricao_estadual', models.CharField(blank=True, max_length=20, null=True)),
                ('tipo', models.CharField(blank=True, max_length=20, null=True)),
                ('proposal_id', models.CharField(blank=True, max_length=20, null=True)),
                ('rede_errors', models.TextField(blank=True, null=True)),
            ],
            options={
                'db_table': 'clientes',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Domicilios',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('agencia', models.CharField(blank=True, max_length=255, null=True)),
                ('comprovante', models.CharField(blank=True, max_length=255, null=True)),
                ('conta', models.CharField(blank=True, max_length=255, null=True)),
                ('digito', models.CharField(blank=True, max_length=255, null=True)),
                ('bco', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'domicilios',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Enderecos',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('bairro', models.CharField(blank=True, max_length=255, null=True)),
                ('cep', models.CharField(blank=True, max_length=255, null=True)),
                ('cidade', models.CharField(blank=True, max_length=255, null=True)),
                ('complemento', models.CharField(blank=True, max_length=255, null=True)),
                ('comprovante', models.CharField(blank=True, max_length=1024, null=True)),
                ('endereco', models.CharField(blank=True, max_length=255, null=True)),
                ('estado', models.CharField(blank=True, max_length=255, null=True)),
                ('numero', models.CharField(blank=True, max_length=255, null=True)),
                ('inscricao_estadual', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'db_table': 'enderecos',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Equipamentos',
            fields=[
                ('numero_logico', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('numero_serie', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'db_table': 'equipamentos',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Fases',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('fase', models.CharField(blank=True, max_length=255, null=True)),
                ('fim', models.DateTimeField(blank=True, null=True)),
                ('inicio', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'fases',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pedidos',
            fields=[
                ('pedido', models.IntegerField(primary_key=True, serialize=False)),
                ('comprovante_maquina', models.CharField(blank=True, max_length=1024, null=True)),
                ('emitir_boleto', models.CharField(blank=True, max_length=255, null=True)),
                ('bairro', models.CharField(blank=True, max_length=255, null=True)),
                ('cep', models.CharField(blank=True, max_length=255, null=True)),
                ('cidade', models.CharField(blank=True, max_length=255, null=True)),
                ('complemento', models.CharField(blank=True, max_length=255, null=True)),
                ('endereco', models.CharField(blank=True, max_length=255, null=True)),
                ('estado', models.CharField(blank=True, max_length=255, null=True)),
                ('numero', models.CharField(blank=True, max_length=255, null=True)),
                ('endereco_entrega_mesmo', models.CharField(blank=True, max_length=255, null=True)),
                ('estorno', models.CharField(blank=True, max_length=255, null=True)),
                ('forma_pagto', models.CharField(blank=True, max_length=255, null=True)),
                ('maquina', models.CharField(blank=True, max_length=255, null=True)),
                ('operadora', models.CharField(blank=True, max_length=255, null=True)),
                ('proposta', models.CharField(blank=True, max_length=255, null=True)),
                ('quantidade', models.IntegerField(blank=True, null=True)),
                ('tipo_pagto', models.CharField(blank=True, max_length=255, null=True)),
                ('cpf_vendedor', models.CharField(blank=True, max_length=255, null=True)),
                ('email_vendedor', models.CharField(blank=True, max_length=255, null=True)),
                ('tipo_frete', models.CharField(blank=True, max_length=3, null=True)),
                ('cod_nsu', models.CharField(blank=True, max_length=255, null=True)),
                ('cod_prod', models.CharField(blank=True, max_length=255, null=True)),
                ('qtde_prod', models.CharField(blank=True, max_length=5, null=True)),
                ('tipo_pedido_venda', models.CharField(blank=True, max_length=20, null=True)),
                ('dt_prev_entrega', models.DateTimeField(blank=True, null=True)),
                ('dt_entrega', models.DateTimeField(blank=True, null=True)),
                ('condicao_pagamento', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'db_table': 'pedidos',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Pessoas',
            fields=[
                ('id', models.IntegerField(primary_key=True, serialize=False)),
                ('cpf_cnpj', models.CharField(blank=True, max_length=255, null=True)),
                ('data_nasc', models.CharField(blank=True, max_length=255, null=True)),
                ('email', models.CharField(blank=True, max_length=255, null=True)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('rg', models.CharField(blank=True, max_length=255, null=True)),
                ('tel1', models.CharField(blank=True, max_length=255, null=True)),
                ('tel2', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'pessoas',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.CharField(max_length=255, primary_key=True, serialize=False)),
                ('token', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'users',
                'managed': False,
            },
        ),
    ]
