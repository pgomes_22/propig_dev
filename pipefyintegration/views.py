from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
import logging
from .models import PipefyModel, LinkGenerator, Mail
from pipefyintegration.models import Pedidos, Clientes
from django.conf import settings
import secrets
import boto3
import os
from django.core.files.uploadedfile import UploadedFile
from django.core.files.storage import default_storage
from django.core.files.storage import FileSystemStorage
from .forms import SalesForm, FormResendMail
from django.http import HttpResponse, JsonResponse
from pprint import pprint
# import xlsxwriter
# from openpyxl import load_workbook
import re

# Create your views here.
logger = logging.getLogger(__name__)
pipe = PipefyModel(settings.PIPEFY_TOKEN, False)

"""MOCK: {"data":{"action":"card.create","card":{"id":12611960,"pipe_id":"rHXSMK0R"}}}"""


def test_mail(request):
    # wb = load_workbook(filename=settings.BASE_DIR +'/media/18-09-24 - PCAE credenciamento BEVIPAG v_ajust_contrato_REDE_tx_ajustada.xlsx')
    # sheet = wb.active
    # b1 = sheet['D6']
    # sheet['C8'] = 'X'
    # b3 = sheet.cell(row=3, column=2)
    # wb.save(settings.BASE_DIR +'/media/PCAE-USUARIO.xlsx')

    # settings.BASE_DIR +'/media/PCAE.xlsx'
    Mail.py_mail("COMPLETE SEU CADASTRO", Mail.mountEmail('asdasdasd', 'fulano'), "daniel.nascimento@bevipag.com.br",
                 "email@grupoe2f.com.br", 'Cliente Pessoa Física (CPF)')
    return render(request, 'pipefyintegration/mail/complemento_compra_novo.html', {
        'rota': 'asdasdasd'
    })


def view_resend_mail(request):
    global naoexistentes
    naoexistentes = []
    if request.POST:
        form = FormResendMail(request.POST)
        if form.is_valid():
            pedidos = request.POST.get('pedidos').replace(" ", "")
            pedidos_split = str(pedidos).split(",")

            for pedido in pedidos_split:
                card = pipe.cards(619894, 1, {'title': str(pedido)})
                if len(card['edges']) == 0:
                    naoexistentes.append(pedido)
            if len(naoexistentes) == 0:
                for pedido in pedidos_split:
                    card = pipe.cards(619894, 1, {'title': str(pedido)})

                    fields = fill_fields(card['edges'][0]['node'])

                    token = secrets.token_hex(20)
                    link = LinkGenerator.mount(request,
                                               token + "/" + str(card['edges'][0]['node']['id']) + "/" + str(9999))
                    pprint(link)
                    Mail.py_mail("FINALIZE A COMPRA DA SUA BEVIPAG!",
                                 Mail.mountEmail(link, fields['nome_completo']),
                                 fields['email'],
                                 "e-commerce@bevipag.com.br",
                                 fields['cadastro'],
                                 True if fields['produtos'] == 'POS - BEVIMais D+2' else False)

                return render(request, 'pipefyintegration/form_resend_documentation_mail.html', {
                    'form': form,
                    'sucesso': True,
                    'cardnaoencontrado': False,
                })

            else:

                return render(request, 'pipefyintegration/form_resend_documentation_mail.html', {
                    'form': form,
                    'cardnaoencontrado': True,
                    'sucesso': False,
                    'cards': naoexistentes
                })

    else:
        form = FormResendMail()
        return render(request, 'pipefyintegration/form_resend_documentation_mail.html', {
            'form': form
        })


def fill_fields(dados_card):
    dict_default_fields = {}
    if len(dados_card['fields']):
        for card in dados_card['fields']:
            if card['name'] == 'Nome completo':
                dict_default_fields['nome_completo'] = card["value"]
            if card['name'] == 'Razão Social':
                dict_default_fields['razao_social'] = card["value"]
            if card['name'] == 'Cadastro':
                dict_default_fields['cadastro'] = card['value']
            if card['name'] == 'Quantidade maquina':
                dict_default_fields['quantidade_maquina'] = card['value']
            if card['name'] == 'É revendedor?':
                dict_default_fields['revendedor'] = card['value']
            if card['name'] == 'Email revendedor':
                dict_default_fields['email_revendedor'] = card['value']
            if card['name'] == 'Número do pedido':
                dict_default_fields['numero_pedido'] = card['value']
            if card['name'] == 'Forma de pagamento':
                dict_default_fields['forma_pagamento'] = card['value']
            if card['name'] == 'Taxa de entrega':
                dict_default_fields['taxa_entrega'] = card['value']
            if card['name'] == 'Subtotal do pedido':
                dict_default_fields['subtotal_pedido'] = card['value']
            if card['name'] == 'Total do pedido':
                dict_default_fields['total_pedido'] = card['value']
            if card['name'] == 'Produtos':
                dict_default_fields['produtos'] = card['value']
            if card['name'] == 'Observações':
                dict_default_fields['observacoes'] = card['value']
            if card['name'] == 'Operadora da Maquina':
                dict_default_fields['operadora'] = card['value']
            if card['name'] == 'Email':
                dict_default_fields['email'] = card['value']
            if card['name'] == 'Telefone 1':
                dict_default_fields['telefone1'] = card['value']
            if card['name'] == 'Telefone 2':
                dict_default_fields['telefone2'] = card['value']
            if card['name'] == 'Estado':
                dict_default_fields['estado'] = card['value']
            if card['name'] == 'Cidade':
                dict_default_fields['cidade'] = card['value']
            if card['name'] == 'Bairro':
                dict_default_fields['bairro'] = card['value']
            if card['name'] == 'Complemento':
                dict_default_fields['complemento'] = card['value']
            if card['name'] == 'Número':
                dict_default_fields['numero'] = card['value']
            if card['name'] == 'Rua/Av':
                dict_default_fields['rua_av'] = card['value']
            if card['name'] == 'CEP':
                dict_default_fields['cep'] = card['value']
            if card['name'] == 'CNPJ':
                dict_default_fields['cnpj'] = card['value']
            if card['name'] == 'CPF':
                dict_default_fields['CPF'] = card['value']

            if card['name'] == 'Agência bancária':
                dict_default_fields['agencia_bancaria'] = card['value']

            if card['name'] == 'Conta bancária':
                dict_default_fields['conta_bancaria'] = card['value']

            if card['name'] == 'Dígito conta':
                dict_default_fields['digito_conta'] = card['value']

            if card['name'] == 'Domicílio bancário':
                dict_default_fields['domicilio_bancario'] = card['value']


            if card['name'] == 'Tipo de pagamento':
                dict_default_fields['tipo_pagamento'] = card['value']
            if card['name'] == 'Prazo do boleto':
                dict_default_fields['prazo_boleto'] = card['value']
            else:
                dict_default_fields['prazo_boleto'] = ""

            if card['name'] == 'RG':
                dict_default_fields['rg'] = card['value']
            if card['name'] == 'Data de nascimento':
                dict_default_fields['data_nascimento'] = card['value']
            else:
                pass

        if dict_default_fields['cadastro'] == 'Cliente Pessoa Física (CPF)':
            dict_default_fields['razao_social'] = 'Pessoa Física'
            dict_default_fields['cnpj'] = '00.000.000/0000-00'
        elif dict_default_fields['cadastro'] == 'Cliente Pessoa Jurídica (CNPJ)':
            dict_default_fields['nome_completo'] = dict_default_fields['razao_social']
            dict_default_fields['CPF'] = '000.000.000-00'
    # pprint(dict_default_fields['cadastro'])
    return dict_default_fields


# def save_sheet(dict_default_fields):
#     """/media/Formulário de aquisição.pdf"""
#     wb = load_workbook(filename=settings.BASE_DIR + '/media/PCAE.xlsx')
#     sheet = wb.active
#     pprint(dict_default_fields)
#     if dict_default_fields['cadastro'] == 'Cliente Pessoa Física (CPF)':
#         sheet['C8'] = 'X'
#         splitcpf = list(dict_default_fields['CPF'])
#         sheet['L8'] = int(splitcpf[0])
#         sheet['M8'] = int(splitcpf[1])
#         sheet['N8'] = int(splitcpf[2])
#         sheet['O8'] = int(splitcpf[4])
#         sheet['P8'] = int(splitcpf[5])
#         sheet['Q8'] = int(splitcpf[6])
#         sheet['R8'] = int(splitcpf[8])
#         sheet['S8'] = int(splitcpf[9])
#         sheet['T8'] = int(splitcpf[10])
#         sheet['V8'] = int(splitcpf[12])
#         sheet['W8'] = int(splitcpf[13])
#         sheet['G10'] = dict_default_fields['nome_completo']
#         sheet['AN10'] = dict_default_fields['nome_completo']
#         sheet['AP17'] = dict_default_fields['nome_completo']
#     else:
#         sheet['C6'] = 'X'
#         splitcnpj = list(dict_default_fields['cnpj'])
#
#         sheet['M6'] = int(splitcnpj[0])
#         sheet['N6'] = int(splitcnpj[1])
#         sheet['O6'] = int(splitcnpj[3])
#         sheet['P6'] = int(splitcnpj[4])
#         sheet['Q6'] = int(splitcnpj[5])
#         sheet['R6'] = int(splitcnpj[7])
#         sheet['S6'] = int(splitcnpj[8])
#         sheet['T6'] = int(splitcnpj[9])
#         sheet['V6'] = int(splitcnpj[11])
#         sheet['W6'] = int(splitcnpj[12])
#         sheet['X6'] = int(splitcnpj[13])
#         sheet['Y6'] = int(splitcnpj[14])
#         sheet['Z6'] = int(splitcnpj[16])
#         sheet['AA6'] = int(splitcnpj[17])
#
#         sheet['G10'] = dict_default_fields['razao_social'] if dict_default_fields['razao_social'] else ""
#         sheet['AN10'] = dict_default_fields['nome_completo'] if dict_default_fields['nome_completo'] else ""
#         sheet['AP17'] = dict_default_fields['nome_completo'] if dict_default_fields['nome_completo'] else ""
#
#     cepsplit = list(dict_default_fields['cep'])
#
#     sheet['O17'] = int(cepsplit[0])
#     sheet['P17'] = int(cepsplit[1])
#     sheet['Q17'] = int(cepsplit[2])
#     sheet['R17'] = int(cepsplit[3])
#     sheet['S17'] = int(cepsplit[4])
#     sheet['U17'] = int(cepsplit[5])
#     sheet['V17'] = int(cepsplit[6])
#     sheet['W17'] = int(cepsplit[7])
#
#     sheet['F15'] = dict_default_fields['email']
#     sheet['C19'] = dict_default_fields['rua_av']
#     sheet['BG19'] = dict_default_fields['numero']
#     sheet['BG19'] = dict_default_fields['numero']
#     sheet['H21'] = dict_default_fields['complemento']
#     sheet['AP21'] = dict_default_fields['bairro']
#     sheet['F23'] = dict_default_fields['cidade']
#     wb.save(settings.BASE_DIR + '/media/PCAEUSUARIO.xlsx')

def download(request, path):
    file_path = os.path.join(settings.MEDIA_ROOT, path)
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/x-pdf")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404

@api_view(['POST'])
def new_sale_ecommerce(request):
    global move
    if request.method == 'POST':
        dados = request.data["data"]
        if dados["action"] == "card.create":
            card = dados["card"]
            if card["id"] != "":
                token = secrets.token_hex(20)

                registro = [{"field_id": "token", "field_value": token},
                            {"field_id": "card_id", "field_value": card["id"]}]

                retorno = pipe.createTableRecord(table_id='M4Y7gmqe', fields_attributes=registro)

                dados_card = pipe.card(card["id"])
                dict_fields_card = fill_fields(dados_card)
                pprint(dict_fields_card)
                # save_sheet(dict_fields_card)

                if (dict_fields_card['forma_pagamento'] == "CARTÃO"):
                    forma_pagamento = "Crédito"
                else:
                    forma_pagamento = "Débito"

                if (dict_fields_card['produtos'] == 'BEVIMais'):
                    dict_fields_card['produtos'] = 'POS - BEVIMais'

                elif (dict_fields_card['produtos'] == 'BEVIMais D+2'):
                    dict_fields_card['produtos'] = 'POS - BEVIMais D+2'

                if dict_fields_card['revendedor'] == 'SIM':
                    link = 'http://www.maquininha.bevipag.com.br'

                    Mail.py_mail("OLÁ PARCEIRO " + dict_fields_card['email_revendedor'] + "!",
                                 Mail.mountEmailRevendedor(link),
                                 dict_fields_card['email_revendedor'],
                                 "e-commerce@bevipag.com.br",
                                 dict_fields_card['cadastro'],
                                 True if dict_fields_card['produtos'] == 'POS - BEVIMais D+2' else False)

                    Mail.py_mail("BEM VINDO " + dict_fields_card['nome_completo'] + "!",
                                 Mail.mountEmailInfoRevendedorCliente(dict_fields_card['email_revendedor'],
                                                                      dict_fields_card['nome_completo']),
                                 dict_fields_card['email'],
                                 "e-commerce@bevipag.com.br",
                                 dict_fields_card['cadastro'],
                                 True if dict_fields_card['produtos'] == 'POS - BEVIMais D+2' else False)

                    move = pipe.moveCardToPhase(card["id"], 4392095)
                else:
                    link = LinkGenerator.mount(request, token + "/" + str(card["id"]) + "/" + retorno["id"])
                    Mail.py_mail("FINALIZE A COMPRA DA SUA BEVIPAG!",
                                 Mail.mountEmail(link, dict_fields_card['nome_completo']),
                                 dict_fields_card['email'],
                                 "e-commerce@bevipag.com.br",
                                 dict_fields_card['cadastro'],
                                 True if dict_fields_card['produtos'] == 'POS - BEVIMais D+2' else False)
                    move = pipe.moveCardToPhase(card["id"], 4221799)

                if move:
                    return Response(retorno, status=status.HTTP_200_OK)
            else:
                return Response("CARD NÃO ENCONTRADO", status=status.HTTP_204_NO_CONTENT)
        else:
            return Response("AÇÃO NÃO ENCONTRADA", status=status.HTTP_204_NO_CONTENT)
    return Response("Metodo GET para esta chamada não existe", status=status.HTTP_405_METHOD_NOT_ALLOWED)


@api_view(['POST'])
def find_card_agente_ecommerce(request):
    global move
    if request.method == 'POST':
        dados = request.data["data"]
        if dados["action"] == "card.create":
            card = dados["card"]
            if card["id"] != "":
                dados_card = pipe.card(card["id"])
                dict_fields_card = fill_fields(dados_card)
                cards = pipe.cards(619894, 999, {}, 'edges {node { id done fields { name value } url } }')

                for c in cards['edges']:
                    if c['node']['done'] is False:
                        dados_card_ecommerce = pipe.card(c['node']['id'])
                        pprint(dados_card_ecommerce)
                        dict_fields_card_ecommerce = fill_fields(dados_card_ecommerce)

                        if dict_fields_card_ecommerce['revendedor'] == 'SIM':

                            if dict_fields_card_ecommerce['cadastro'] == 'Cliente Pessoa Física (CPF)':
                                if dict_fields_card['CPF'] == dict_fields_card_ecommerce['CPF']:
                                    pipe.moveCardToPhase(c['node']['id'], 4230472)

                            else:
                                if dict_fields_card['cnpj'] == dict_fields_card_ecommerce['cnpj']:
                                    pipe.moveCardToPhase(c['node']['id'], 4230472)
                return Response(cards, status=status.HTTP_200_OK)


def create_pipe_manual(request, card_id):
    dados_card = pipe.card(card_id)
    pprint(dados_card)
    dict_fields_card = fill_fields(dados_card)
    pprint(dict_fields_card)
    # save_sheet(dict_fields_card)

    if (dict_fields_card['forma_pagamento'] == "CARTÃO"):
        forma_pagamento = "Crédito"
    else:
        forma_pagamento = "Débito"

    if (dict_fields_card['produtos'] == 'BEVIMais'):
        dict_fields_card['produtos'] = 'POS - BEVIMais'

    elif (dict_fields_card['produtos'] == 'BEVIMais D+2 Black Friday'):
        dict_fields_card['produtos'] = 'POS - BEVIMais D+2'

    new_pipe_attrs = [
        {'field_id': "rg", 'field_value': dict_fields_card['rg'] if 'rg' in dict_fields_card else ""},

        {'field_id': "cadastro", 'field_value': dict_fields_card['cadastro']},
        {'field_id': "ficha_de_cadastro",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "contrato_social",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "cart_o_cnpj",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "comprovante_de_atividade_crm_crea_oab_alvar_da_prefeitura_outros",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh_de_um_dos_s_cios",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "comprovante_de_endere_o",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "comprovante_de_domic_lio_banc_rio",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "comprovante_de_pagamento_da_m_quina",
         'field_value': 'Verificar pipe filho nº ' + str(dict_fields_card["numero_pedido"])},
        {'field_id': "raz_o_social", 'field_value': dict_fields_card['razao_social']},
        {'field_id': "tipo_de_pagamento", 'field_value': dict_fields_card['tipo_pagamento']},
        {'field_id': "n_mero_do_comprovante_de_pagamento", 'field_value': 9999999},
        {'field_id': "haver_envio_de_equipamento", 'field_value': 'Sim'},
        {'field_id': "o_endere_o_para_entrega_da_m_quina_o_mesmo_endere_o_do_agente", 'field_value': 'Sim'},
        {'field_id': "empresa", 'field_value': dict_fields_card['nome_completo']},
        {'field_id': "cnpj", 'field_value': dict_fields_card['cnpj']},
        {'field_id': "cpf", 'field_value': dict_fields_card['cpf']},
        {'field_id': "data_de_nascimento", 'field_value': dict_fields_card['data_nascimento']},
        {'field_id': "telefone_de_contato_1", 'field_value': dict_fields_card['telefone1']},
        {'field_id': "telefone_de_contato_2", 'field_value': dict_fields_card['telefone2']},
        {'field_id': "email", 'field_value': dict_fields_card['email']},
        {'field_id': "rua_av", 'field_value': dict_fields_card['rua_av']},
        {'field_id': "n_mero", 'field_value': dict_fields_card['numero']},
        {'field_id': "complemento", 'field_value': dict_fields_card['complemento']},
        {'field_id': "bairro", 'field_value': dict_fields_card['bairro']},
        {'field_id': "cep", 'field_value': dict_fields_card['cep']},
        {'field_id': "cidade", 'field_value': dict_fields_card['cidade']},
        {'field_id': "estado", 'field_value': dict_fields_card['estado']},
        {'field_id': "domic_lio_banc_rio", 'field_value': '000 - NÃO PREENCHIDO'},
        {'field_id': "ag_ncia", 'field_value': replaceMultiple(str(request.POST.get('agencia')), ['.', ',', '-', ' '], '')},
        {'field_id': "conta", 'field_value': replaceMultiple(str(request.POST.get('conta')), ['.', ',', '-', ' '], '')},
        {'field_id': "d_gito_da_conta", 'field_value': request.POST.get('digito_conta')},
        {'field_id': "tipo_de_conta", 'field_value': 'Conta corrente'},

        {'field_id': "aceita_antecipa_o_autom_tica", 'field_value': request.POST.get('cliente_flex')},
        {'field_id': "tipo_de_m_quina", 'field_value': dict_fields_card['produtos']},
        {'field_id': "quantidade_de_m_quinas", 'field_value': dict_fields_card['quantidade_maquina']},
        {'field_id': "operadora_de_prefer_ncia", 'field_value': dict_fields_card['operadora']},
        {'field_id': "forma_de_pagamento", 'field_value': forma_pagamento},
        {'field_id': "prazo_do_boleto", 'field_value': dict_fields_card['prazo_boleto']},
        {'field_id': "entrega_rua_av", 'field_value': dict_fields_card['rua_av']},
        {'field_id': "entrega_n_mero", 'field_value': dict_fields_card['numero']},
        {'field_id': "entrega_complemento", 'field_value': dict_fields_card['complemento']},
        {'field_id': "entrega_bairro", 'field_value': dict_fields_card['bairro']},
        {'field_id': "entrega_cep", 'field_value': dict_fields_card['cep']},
        {'field_id': "entrega_cidade", 'field_value': dict_fields_card['cidade']},
        {'field_id': "entrega_estado", 'field_value': dict_fields_card['estado']},
        {'field_id': "cpf_do_vendedor", 'field_value': dict_fields_card['cpf']},
        {'field_id': "email_solicitante", 'field_value': dict_fields_card['email']},
        {'field_id': "tipo_de_cliente", 'field_value': 'Cliente comum (com envio de equipamento)'},
    ]

    pipe.createCard(476976, new_pipe_attrs, [str(card_id)])
    return JsonResponse({'status': 'pipe criado'})


def replaceMultiple(mainString, toBeReplaces, newString):
    # Iterate over the strings to be replaced
    for elem in toBeReplaces:
        # Check if string is in the main string
        if elem in mainString:
            # Replace the string
            mainString = mainString.replace(elem, newString)

    return mainString

def file_extension(name):
    split = name.split(".")
    return split[1]


def upload(arquivo, identificador):
    fs = FileSystemStorage()
    filename = fs.save(identificador + "_" + secrets.token_hex(10) + "." + file_extension(arquivo.name), arquivo)
    return fs.url(filename)


def remove(filename):
    fs = FileSystemStorage()
    fs.delete(filename)


def upload_s3(s3, bucket_name, uploaded_file_url):
    s3.upload_file(settings.BASE_DIR + uploaded_file_url, bucket_name, uploaded_file_url.lstrip("/"),
                   {'ACL': 'public-read'})


def novo_card_pai(pipe_id, attributes):
    pipe.createCard(pipe_id, attributes)


def form_client_ecommerce(request, token, card_id, record_id):
    s3 = boto3.client(
        's3',
        aws_access_key_id='AKIAJ3VU2NH6RNPUXSUA',
        aws_secret_access_key='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'
    )
    bucket_name = 'documentacaobevipag'

    if record_id:
        dados_card = pipe.card(card_id)
        dict_fields_card = fill_fields(dados_card)

        form = SalesForm(
            initial=dict_fields_card
        )

        if request.method == 'POST' and \
                request.FILES['ficha_cadastro'] and \
                request.FILES['comprovante_domicilio_bancario'] and \
                request.FILES['documento_foto']:

            try:
                ficha_cadastro = upload(request.FILES['ficha_cadastro'], dict_fields_card['numero_pedido'])
                upload_s3(s3, bucket_name, ficha_cadastro)
                url_ficha_cadastral = "https://s3.amazonaws.com/{0}{1}".format('documentacaobevipag', ficha_cadastro)
                pipe.updateCardField(card_id, 'arquivo_ficha_de_cadastro', url_ficha_cadastral)
                remove(settings.BASE_DIR + ficha_cadastro)

                comprovante_domicilio_bancario = upload(request.FILES['comprovante_domicilio_bancario'],
                                                        dict_fields_card['numero_pedido'])
                upload_s3(s3, bucket_name, comprovante_domicilio_bancario)
                url_ficha_comprovante_bancario = "https://s3.amazonaws.com/{0}{1}".format('documentacaobevipag',
                                                                                          comprovante_domicilio_bancario)
                pipe.updateCardField(card_id, 'arquivo_comprovante_domic_lio_banc_rio', url_ficha_comprovante_bancario)
                remove(settings.BASE_DIR + comprovante_domicilio_bancario)

                if request.FILES['documento_foto']:
                    documento_foto = upload(request.FILES['documento_foto'], dict_fields_card['numero_pedido'])
                    upload_s3(s3, bucket_name, documento_foto)
                    url_documento_foto = "https://s3.amazonaws.com/{0}{1}".format('documentacaobevipag', documento_foto)
                    pipe.updateCardField(card_id, 'arquivo_cnh', url_documento_foto)
                    remove(settings.BASE_DIR + documento_foto)

                if (dict_fields_card['forma_pagamento'] == "CARTÃO"):
                    forma_pagamento = "Crédito"
                else:
                    forma_pagamento = "Débito"

                flex = "Sim"

                if (dict_fields_card['produtos'] == 'BEVIMais'):
                    dict_fields_card['produtos'] = 'POS - BEVIMais'
                    flex = request.POST.get('cliente_flex')

                elif (dict_fields_card['produtos'] == 'BEVITop'):
                    dict_fields_card['produtos'] = 'POS - BEVITop'
                    flex = request.POST.get('cliente_flex')

                elif (dict_fields_card['produtos'] == 'BEVIMais D+2'):
                    dict_fields_card['produtos'] = 'POS - BEVIMais D+2'
                    flex = 'Sim'

                elif (dict_fields_card['produtos'] == 'BEVIMais D+2 Black Friday'):
                    dict_fields_card['produtos'] = 'POS - BEVIMais D+2'
                    flex = 'Sim'

                new_pipe_attrs = [
                    {'field_id': "rg", 'field_value': replaceMultiple(str(dict_fields_card['rg'] if 'conta_bancaria' in dict_fields_card else request.POST.get('conta')), ['.', ',', '-', ' '], '') if 'rg' in dict_fields_card else ""},
                    {'field_id': "cadastro", 'field_value': dict_fields_card['cadastro']},
                    {'field_id': "ficha_de_cadastro",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "contrato_social",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "cart_o_cnpj",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "comprovante_de_atividade_crm_crea_oab_alvar_da_prefeitura_outros",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh_de_um_dos_s_cios",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "comprovante_de_endere_o",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "comprovante_de_domic_lio_banc_rio",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "comprovante_de_pagamento_da_m_quina",
                     'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                    {'field_id': "raz_o_social", 'field_value': dict_fields_card['razao_social']},
                    {'field_id': "tipo_de_pagamento", 'field_value': dict_fields_card['tipo_pagamento']},
                    {'field_id': "n_mero_do_comprovante_de_pagamento", 'field_value': dict_fields_card["numero_pedido"]},
                    {'field_id': "haver_envio_de_equipamento", 'field_value': 'Sim'},
                    {'field_id': "o_endere_o_para_entrega_da_m_quina_o_mesmo_endere_o_do_agente", 'field_value': 'Sim'},
                    {'field_id': "empresa", 'field_value': dict_fields_card['nome_completo']},
                    {'field_id': "cnpj", 'field_value': dict_fields_card['cnpj']},
                    {'field_id': "cpf", 'field_value': dict_fields_card['CPF']},
                    {'field_id': "data_de_nascimento", 'field_value': dict_fields_card['data_nascimento']},
                    {'field_id': "telefone_de_contato_1", 'field_value': dict_fields_card['telefone1']},
                    {'field_id': "telefone_de_contato_2", 'field_value': dict_fields_card['telefone2']},
                    {'field_id': "email", 'field_value': dict_fields_card['email']},
                    {'field_id': "rua_av", 'field_value': dict_fields_card['rua_av']},
                    {'field_id': "n_mero", 'field_value': dict_fields_card['numero']},
                    {'field_id': "complemento", 'field_value': dict_fields_card['complemento']},
                    {'field_id': "bairro", 'field_value': dict_fields_card['bairro']},
                    {'field_id': "cep", 'field_value': dict_fields_card['cep']},
                    {'field_id': "cidade", 'field_value': dict_fields_card['cidade']},
                    {'field_id': "estado", 'field_value': dict_fields_card['estado']},
                    {'field_id': "domic_lio_banc_rio", 'field_value': dict_fields_card['domicilio_bancario'] if 'domicilio_bancario' in dict_fields_card else request.POST.get('domicilio_bancario')},
                    {'field_id': "cod_tipo_de_conta", 'field_value': 'SELECIONE'},
                    {'field_id': "ag_ncia",
                     'field_value': replaceMultiple(str(dict_fields_card['agencia_bancaria'] if 'agencia_bancaria' in dict_fields_card else request.POST.get('agencia')), ['.', ',', '-', ' '], '')},
                    {'field_id': "conta",
                     'field_value': replaceMultiple(str(dict_fields_card['conta_bancaria'] if 'conta_bancaria' in dict_fields_card else request.POST.get('conta')), ['.', ',', '-', ' '], '')},
                    {'field_id': "d_gito_da_conta", 'field_value': replaceMultiple(str(dict_fields_card['digito_conta'] if 'digito_conta' in dict_fields_card else request.POST.get('digito_conta')), ['.', ',', '-', ' '], '')},
                    {'field_id': "tipo_de_conta", 'field_value': 'Conta corrente'},
                    {'field_id': "aceita_antecipa_o_autom_tica", 'field_value': flex},
                    {'field_id': "tipo_de_m_quina", 'field_value': dict_fields_card['produtos']},
                    {'field_id': "quantidade_de_m_quinas", 'field_value': dict_fields_card['quantidade_maquina']},
                    {'field_id': "operadora_de_prefer_ncia", 'field_value': request.POST.get('operadora')},
                    {'field_id': "forma_de_pagamento", 'field_value': forma_pagamento},
                    {'field_id': "prazo_do_boleto", 'field_value': dict_fields_card['prazo_boleto']},
                    {'field_id': "entrega_rua_av", 'field_value': dict_fields_card['rua_av']},
                    {'field_id': "entrega_n_mero", 'field_value': dict_fields_card['numero']},
                    {'field_id': "entrega_complemento", 'field_value': dict_fields_card['complemento']},
                    {'field_id': "entrega_bairro", 'field_value': dict_fields_card['bairro']},
                    {'field_id': "entrega_cep", 'field_value': dict_fields_card['cep']},
                    {'field_id': "entrega_cidade", 'field_value': dict_fields_card['cidade']},
                    {'field_id': "entrega_estado", 'field_value': dict_fields_card['estado']},
                    {'field_id': "cpf_do_vendedor", 'field_value': "016.797.427-01"},
                    {'field_id': "email_solicitante", 'field_value': 'ecommerce@bevipag.com.br'},
                    {'field_id': "tipo_de_cliente", 'field_value': 'Cliente comum (com envio de equipamento)'},
                ]


                pipe.createCard(476976, new_pipe_attrs, [str(card_id)])
                move = pipe.moveCardToPhase(card_id, 4230472)

                if move:
                    return render(
                            request,
                            'pipefyintegration/documentacao_enviada.html',
                            {'form': form, 'dict_default_fields': dict_fields_card})
                else:
                    return render(
                        request,
                        'pipefyintegration/documentacao_falha.html',
                        {'form': form, 'dict_default_fields': dict_fields_card})
            except Exception as e:
                pipe.createComment(card_id, str(e))
                pipe.updateCard(id=card_id, label_ids=['2477183'])
                pipe.createLabel(card_id, 'Envio de documentação com falha', '#124a88')
                return render(
                    request,
                    'pipefyintegration/documentacao_falha.html',
                    {'form': form, 'dict_default_fields': dict_fields_card})
        else:

            if (dict_fields_card['produtos'] == 'BEVIMais' or dict_fields_card['produtos'] == 'BEVITop'):
                BLACKFRIDAY = False

            elif (dict_fields_card['produtos'] == 'BEVIMais D+2'):
                BLACKFRIDAY = True

            elif (dict_fields_card['produtos'] == 'BEVIMais D+2 Black Friday'):
                BLACKFRIDAY = True


            if BLACKFRIDAY is True and dict_fields_card['cadastro'] == 'Cliente Pessoa Física (CPF)':
                attachment = 'PCAEBevimais_D2_CPF.pdf'
            elif BLACKFRIDAY is True and dict_fields_card['cadastro'] == 'Cliente Pessoa Jurídica (CNPJ)':
                attachment = 'PCAEBevimais_D2_CNPJ.pdf'
            elif BLACKFRIDAY is False and dict_fields_card['cadastro'] == 'Cliente Pessoa Física (CPF)':
                attachment = 'PCAE_REDE_tx_ajustada_CPF.pdf'
            elif BLACKFRIDAY is False and dict_fields_card['cadastro'] == 'Cliente Pessoa Jurídica (CNPJ)':
                attachment = 'PCAE_REDE_tx_ajustada_CNPJ.pdf'

            if BLACKFRIDAY:
                tem_flex = False
            else:
                tem_flex = True

            if 'conta_bancaria' in dict_fields_card:
                return render(
                    request,
                    'pipefyintegration/documentacao.html',
                    {'form': form, 'dict_default_fields': dict_fields_card, 'attachment': attachment, 'flex': tem_flex })
            else:
                return render(
                    request,
                    'pipefyintegration/documentacao_antiga.html',
                    {'form': form, 'dict_default_fields': dict_fields_card, 'attachment': attachment, 'flex': tem_flex})

def migrate_cards(request):
    dados_card = pipe.cards(619894, 150, {"label_ids": 2584495})
    pprint(len(dados_card['edges']))

    for dc in dados_card['edges']:
        if dc['node']['current_phase']['name'] == 'Concluído':
            dados_card = pipe.card(dc['node']['id'])
            dict_fields_card = fill_fields(dados_card)
            if (dict_fields_card['forma_pagamento'] == "CARTÃO"):
                forma_pagamento = "Crédito"
            else:
                forma_pagamento = "Débito"

            if (dict_fields_card['produtos'] == 'BEVIMais'):
                dict_fields_card['produtos'] = 'POS - BEVIMais'
                flex = request.POST.get('cliente_flex')

            elif (dict_fields_card['produtos'] == 'BEVIMais D+2'):
                dict_fields_card['produtos'] = 'POS - BEVIMais D+2'
                flex = 'Sim'

            elif (dict_fields_card['produtos'] == 'BEVIMais D+2 Black Friday'):
                dict_fields_card['produtos'] = 'POS - BEVIMais D+2'
                flex = 'Sim'

            new_pipe_attrs = [
                {'field_id': "rg", 'field_value': replaceMultiple(
                    str(dict_fields_card['rg'] if 'conta_bancaria' in dict_fields_card else request.POST.get('conta')),
                    ['.', ',', '-', ' '], '') if 'rg' in dict_fields_card else ""},
                {'field_id': "cadastro", 'field_value': dict_fields_card['cadastro']},
                {'field_id': "ficha_de_cadastro",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "contrato_social",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "cart_o_cnpj",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "comprovante_de_atividade_crm_crea_oab_alvar_da_prefeitura_outros",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh_de_um_dos_s_cios",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "documento_com_foto_rg_e_cpf_ou_cnh",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "comprovante_de_endere_o",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "comprovante_de_domic_lio_banc_rio",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "comprovante_de_pagamento_da_m_quina",
                 'field_value': 'COMPRA VIA E-COMMERCE ' + str(dict_fields_card["numero_pedido"])},
                {'field_id': "raz_o_social", 'field_value': dict_fields_card['razao_social']},
                {'field_id': "tipo_de_pagamento", 'field_value': dict_fields_card['tipo_pagamento']},
                {'field_id': "n_mero_do_comprovante_de_pagamento", 'field_value': dict_fields_card["numero_pedido"]},
                {'field_id': "haver_envio_de_equipamento", 'field_value': 'Sim'},
                {'field_id': "o_endere_o_para_entrega_da_m_quina_o_mesmo_endere_o_do_agente", 'field_value': 'Sim'},
                {'field_id': "empresa", 'field_value': dict_fields_card['nome_completo']},
                {'field_id': "cnpj", 'field_value': dict_fields_card['cnpj']},
                {'field_id': "cpf", 'field_value': dict_fields_card['CPF']},
                {'field_id': "data_de_nascimento", 'field_value': dict_fields_card['data_nascimento']},
                {'field_id': "telefone_de_contato_1", 'field_value': dict_fields_card['telefone1']},
                {'field_id': "telefone_de_contato_2", 'field_value': dict_fields_card['telefone2']},
                {'field_id': "email", 'field_value': dict_fields_card['email']},
                {'field_id': "rua_av", 'field_value': dict_fields_card['rua_av']},
                {'field_id': "n_mero", 'field_value': dict_fields_card['numero']},
                {'field_id': "complemento", 'field_value': dict_fields_card['complemento']},
                {'field_id': "bairro", 'field_value': dict_fields_card['bairro']},
                {'field_id': "cep", 'field_value': dict_fields_card['cep']},
                {'field_id': "cidade", 'field_value': dict_fields_card['cidade']},
                {'field_id': "estado", 'field_value': dict_fields_card['estado']},
                {'field_id': "domic_lio_banc_rio", 'field_value': dict_fields_card[
                    'domicilio_bancario'] if 'domicilio_bancario' in dict_fields_card else request.POST.get(
                    'domicilio_bancario')},
                {'field_id': "cod_tipo_de_conta", 'field_value': 'SELECIONE'},
                {'field_id': "ag_ncia",
                 'field_value': replaceMultiple(str(dict_fields_card[
                                                        'agencia_bancaria'] if 'agencia_bancaria' in dict_fields_card else request.POST.get(
                     'agencia')), ['.', ',', '-', ' '], '')},
                {'field_id': "conta",
                 'field_value': replaceMultiple(str(
                     dict_fields_card['conta_bancaria'] if 'conta_bancaria' in dict_fields_card else request.POST.get(
                         'conta')), ['.', ',', '-', ' '], '')},
                {'field_id': "d_gito_da_conta", 'field_value': replaceMultiple(str(
                    dict_fields_card['digito_conta'] if 'digito_conta' in dict_fields_card else request.POST.get(
                        'digito_conta')), ['.', ',', '-', ' '], '')},
                {'field_id': "tipo_de_conta", 'field_value': 'Conta corrente'},
                {'field_id': "aceita_antecipa_o_autom_tica", 'field_value': flex},
                {'field_id': "tipo_de_m_quina", 'field_value': dict_fields_card['produtos']},
                {'field_id': "quantidade_de_m_quinas", 'field_value': dict_fields_card['quantidade_maquina']},
                {'field_id': "operadora_de_prefer_ncia", 'field_value': request.POST.get('operadora')},
                {'field_id': "forma_de_pagamento", 'field_value': forma_pagamento},
                {'field_id': "prazo_do_boleto", 'field_value': dict_fields_card['prazo_boleto']},
                {'field_id': "entrega_rua_av", 'field_value': dict_fields_card['rua_av']},
                {'field_id': "entrega_n_mero", 'field_value': dict_fields_card['numero']},
                {'field_id': "entrega_complemento", 'field_value': dict_fields_card['complemento']},
                {'field_id': "entrega_bairro", 'field_value': dict_fields_card['bairro']},
                {'field_id': "entrega_cep", 'field_value': dict_fields_card['cep']},
                {'field_id': "entrega_cidade", 'field_value': dict_fields_card['cidade']},
                {'field_id': "entrega_estado", 'field_value': dict_fields_card['estado']},
                {'field_id': "cpf_do_vendedor", 'field_value': "016.797.427-01"},
                {'field_id': "email_solicitante", 'field_value': 'ecommerce@bevipag.com.br'},
                {'field_id': "tipo_de_cliente", 'field_value': 'Cliente comum (com envio de equipamento)'},
            ]

            pipe.createCard(476976, new_pipe_attrs, [str(dc['node']['id'])])
            move = pipe.moveCardToPhase(dc['node']['id'], 4230472)
            pprint(dict_fields_card)

    #pprint(dados_card)

@api_view(['POST'])
def calculo_valor_final(request):
    global dict_card
    dict_card = {}
    if request.method == 'POST':
        dados = request.data["data"]
        if dados["action"] == "card.create":
            card = dados["card"]
            if card["id"] != "":
                ccard = pipe.card(card['id'])
                for fields in ccard['fields']:
                    if fields['name'] == 'Tipo de máquina':
                        dict_card['tipo_maquina'] = fields['value']
                    if fields['name'] == 'Quantidade de máquinas':
                        dict_card['quantidade_maquinas'] = fields['value']
                    if fields['name'] == 'Forma de pagamento':
                        dict_card['forma_pagamento'] = fields['value']
                    if fields['name'] == 'Tipo de pagamento':
                        dict_card['tipo_pagamento'] = fields['value']
                    else:
                        dict_card['tipo_pagamento'] = ""
                    if fields['name'] == 'Prazo do boleto':
                        dict_card['prazo_boleto'] = card['value']
                    else:
                        dict_card['prazo_boleto'] = ""

                if dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card['forma_pagamento'] == 'Débito':
                    preco = 430
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card[
                    'forma_pagamento'] == 'Transferência bancária':
                    preco = 430
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] == 'À Vista':
                    preco = 430
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] == 'Á vista':
                    preco = 430
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                elif dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] != 'À Vista':
                    preco = 478.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] != 'Á vista':
                    preco = 478.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card['forma_pagamento'] == 'Débito':
                    preco = 780
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card[
                    'forma_pagamento'] == 'Transferência bancária':
                    preco = 780
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] == 'À Vista':
                    preco = 780
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] == 'Á vista':
                    preco = 780
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                elif dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] != 'À Vista':
                    preco = 838.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVITop' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] != 'Á vista':
                    preco = 838.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card['forma_pagamento'] == 'Débito':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card[
                    'forma_pagamento'] == 'Transferência bancária':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] == 'À Vista':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] == 'Á vista':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                elif dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card['forma_pagamento'] == 'Crédito' and \
                        dict_card['tipo_pagamento'] != 'À Vista':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

                if dict_card['tipo_maquina'] == 'POS - BEVIMais D+2' and dict_card['forma_pagamento'] == 'Boleto' and \
                        dict_card['prazo_boleto'] != 'Á vista':
                    preco = 238.8
                    preco_final = preco * float(dict_card['quantidade_maquinas'])
                    pipe.updateCardField(card['id'], 'valor_total_da_venda', repr(preco_final))

    return JsonResponse({'response': 'ok'})


@api_view(['POST'])
def adicional_maquina_add(request):
    global dict_card
    dict_card = {}
    if request.method == 'POST':
        dados = request.data["data"]
        if dados["action"] == "card.create":
            card = dados["card"]
            if card["id"] != "":
                ccard = pipe.card(card['id'])
                for fields in ccard['fields']:
                    if fields['name'] == 'Haverá envio de equipamento?':
                        dict_card['envio_equipamento'] = fields['value']
                    if fields['name'] == 'CEP da entrega':
                        dict_card['cep'] = fields['value']
                    if fields['name'] == 'Estado para entrega':
                        dict_card['estado'] = fields['value']
                    if fields['name'] == 'Município para entrega':
                        dict_card['municipio'] = fields['value']
                    if fields['name'] == 'Nome completo':
                        dict_card['nome_completo'] = fields['value']
                    if fields['name'] == 'Email do cliente':
                        dict_card['email_cliente'] = fields['value']
                    if fields['name'] == 'Número do comprovante de pagamento':
                        dict_card['numero_comprovante'] = fields['value']
                    if fields['name'] == 'Comprovante de pagamento da máquina':
                        dict_card['comprovante_pagamento'] = fields['value']
                    if fields['name'] == 'Tipo de pagamento':
                        dict_card['tipo_pagamento'] = fields['value']
                    if fields['name'] == 'Operadora de preferência':
                        dict_card['operadora'] = fields['value']
                    if fields['name'] == 'Forma de pagamento':
                        dict_card['forma_pagamento'] = fields['value']
                    if fields['name'] == 'Email solicitante':
                        dict_card['email_solicitante'] = fields['value']
                    if fields['name'] == 'Endereço para entrega':
                        dict_card['endereco_entrega'] = fields['value']
                    if fields['name'] == 'Quantidade de máquinas':
                        dict_card['quantidade_maquinas'] = fields['value']
                    if fields['name'] == 'Tipo de máquina':
                        dict_card['tipo_maquina'] = fields['value']
                    if fields['name'] == 'Termo aditivo':
                        dict_card['termo_aditivo'] = fields['value']

                    if fields['name'] == 'Cadastro':
                        dict_card['tipo_cadastro'] = fields['value']

                if dict_card['tipo_cadastro'] == 'Cliente Pessoa Física (CPF) - Adicional de máquina':
                    for fields in ccard['fields']:
                        if fields['name'] == 'CPF':
                            dict_card['valor_cadastro'] = fields['value']

                else:
                    for fields in ccard['fields']:
                        if fields['name'] == 'CNPJ':
                            dict_card['valor_cadastro'] = fields['value']

                cliente = Clientes.objects.using('bevi-txn').get(cnpj_cpf=dict_card['valor_cadastro'])
                ultimo_registro = Pedidos.objects.using('bevi-txn').filter().order_by('-pedido')[0]

                pedido = Pedidos()
                pprint(ultimo_registro.pedido)
                pedido.pedido = int(ultimo_registro.pedido) + 1
                pedido.comprovante_maquina = dict_card['comprovante_pagamento']
                pedido.emitir_boleto = None
                pedido.bairro = None
                pedido.cep = dict_card['cep']
                pedido.cidade = dict_card['municipio']
                pedido.complemento = None
                pedido.endereco = dict_card['endereco_entrega']
                pedido.estado = dict_card['estado']
                pedido.numero = None
                pedido.endereco_entrega_mesmo = 'Sim'
                pedido.estorno = None
                pedido.forma_pagamento = dict_card['forma_pagamento']
                pedido.maquina = dict_card['tipo_maquina']
                pedido.operadora = dict_card['operadora']
                pedido.proposta = None
                pedido.quantidade = dict_card['quantidade_maquinas']
                pedido.tipo_pagto = dict_card['tipo_pagamento']
                pedido.cliente = cliente
                pedido.tipo_frete = None
                pedido.email_vendedor = dict_card['email_solicitante']
                pedido.cod_nsu = dict_card['numero_comprovante']
                pedido.cod_prod = dict_card['tipo_maquina']
                pedido.qtde_prod = dict_card['quantidade_maquinas']
                pedido.tipo_pedido_venda = None
                pedido.dt_prev_entrega = None
                pedido.dt_entrega = None
                pedido.condicao_pagamento = None
                pedido.save(using='bevi-txn')
    return JsonResponse({'response': 'ok'})
