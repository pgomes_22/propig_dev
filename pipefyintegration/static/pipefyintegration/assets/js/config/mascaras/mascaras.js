$(document).ready(function() {
  $("#id_telefone").mask("(00) 00000-0000");
  $("#id_telefone_titular").mask("(00) 00000-0000");
  $("#id_whatsapp").mask("(00) 00000-0000");
  if ($("#id_tipo").val() === "01") $("#id_cpf").mask("000.000.000-00");
  else $("#id_cpf").mask("00.000.000/0000-00");

  $("#id_tipo").on("change", function() {
    if ($(this).val() == "01") {
        $("#id_cpf").mask("000.000.000-00");
    } else {
       $("#id_cpf").mask("00.000.000/0000-00");
    }
  });
});
