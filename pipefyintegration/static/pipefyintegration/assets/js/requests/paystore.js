/* Salvar Lojista */
$(document).ready(function() {
  var csrftoken = getCookie("csrftoken"); //static/assets/config/config.js
  $("#post-merchant").on("submit", function(event) {
    event.preventDefault();
    create_merchant();
  });

  function create_merchant() {
    $("#fieldsError").html("");
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        $("#modal-title-default").html("Enviando...");
        $("#btnOk").css("display", "none");
        $("#titleError").html('Enviando...');
        /* for (var i = 0; i < 100; i++) {
          (function (i) {
            setTimeout(function () {
              $("#titleError").html('<div class="progress">'+
              '<div class="progress-bar bg-default" role="progressbar" '+
              ' aria-valuenow="'+i+'" aria-valuemin="0" aria-valuemax="100" '+
              ' style="width: '+i+'%;"></div></div>');
              
            }, 100*i);
          })(i);
        }; */
        
        $("#modal-sucess").modal("show");
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });
    $.ajax({
      url: "/dashboard/add-merchant/", 
      type: "POST",
      data: {
        "owner": {
          "name": $("#id_nome").val(),
          "email": $("#id_email_titular").val(),
          "phone": $("#id_telefone_titular").val().replace(/\D/g, ""),
          "messenger_phone": $("#id_whatsapp").val(),
        },
        "additional_fields": {},
        "address_complement": $("#id_complemento").val(),
        "address_number": $("#id_numero").val(),
        "commercial_name": $("#id_nome_fantasia").val(),
        "company_name": $("#id_nome_fantasia").val(),
        "enterprise_email": $("#id_email").val(),
        "enterprise_phone": $("#id_telefone").val().replace(/\D/g, ""),
        "merchant_category_code": $("#id_categories_form").val(),
        "merchant_number": $("#id_inscricao_municipal").val(),
        "national_id": $("#id_cpf").val().replace(/\D/g, ""),
        "national_type": $("#id_tipo").val(),
        "postal_code": $("#id_cep").val().replace(/\D/g, ""),
        "street": $("#id_endereco").val()
      },
      
    }).done(function(json) {
      if (json.message) {
        $("#btnOk").css("display", "block");
        $("#btnOk").removeAttr("href");
        $("#btnOk").attr("data-dismiss", "modal");
        $("#titleError").html(json.message);
        $("#modal-title-default").html("Falha ao cadastrar o Lojista");
        var erros = "";
        for (const err in json.errors) {
          if (json.errors.hasOwnProperty(err)) {
            const element = json.errors[err];
            erros = erros + element + "<br>";
          }
          $("#fieldsError").html(erros);
        }
      }
      else {
        $("#btnOk").css("display", "block");
        $("#modal-title-default").html("Lojista cadastrado com sucesso!");
        $("#titleError").html("COD do Lojista: " + json.merchant);
      }
      $("#modal-sucess").modal("show");
    }).fail(function(xhr, errmsg, err) {
      console.log(err)
      $("#btnOk").removeAttr("href");
      $("#btnOk").css("display", "block");
      $("#modal-title-default").html("Falha ao cadastrar o Lojista");
      $("#titleError").empty();
      $("#titleError").html("Código do erro: " + xhr.status +" : "+ err);
      $("#modal-sucess").modal("show");
    });
  }
});
