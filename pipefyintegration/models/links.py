class LinkGenerator(object):
    def __init__(self):
        pass

    @staticmethod
    def mount(request, card):
        if request.is_secure():
            link = 'https://' + request.get_host() + "/pipefyintegration/completa-cadastro/" + card
        else:
            link = 'http://' + request.get_host() + "/pipefyintegration/completa-cadastro/" + card
        return link
