# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Clientes(models.Model):
    id = models.IntegerField(primary_key=True)
    flex = models.CharField(max_length=255, blank=True, null=True)
    cartao_cnpj = models.CharField(max_length=1024, blank=True, null=True)
    cnae = models.CharField(max_length=255, blank=True, null=True)
    comprovante_atividade = models.CharField(max_length=1024, blank=True, null=True)
    contrato_social = models.CharField(max_length=1024, blank=True, null=True)
    documento_foto = models.CharField(max_length=1024, blank=True, null=True)
    cnpj_cpf = models.CharField(unique=True, max_length=255, blank=True, null=True)
    ficha_cadastro = models.CharField(max_length=1024, blank=True, null=True)
    update = models.DateTimeField(blank=True, null=True)
    mcc = models.CharField(max_length=255, blank=True, null=True)
    nome = models.CharField(max_length=255, blank=True, null=True)
    ponto_venda = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=255, blank=True, null=True)
    status_documentacao = models.CharField(max_length=255, blank=True, null=True)
    tipo_pessoa = models.CharField(max_length=255, blank=True, null=True)
    created = models.DateTimeField(blank=True, null=True)
    inscricao_estadual = models.CharField(max_length=20, blank=True, null=True)
    tipo = models.CharField(max_length=20, blank=True, null=True)
    proposal_id = models.CharField(max_length=20, blank=True, null=True)
    rede_errors = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clientes'


class Domicilios(models.Model):
    id = models.IntegerField(primary_key=True)
    agencia = models.CharField(max_length=255, blank=True, null=True)
    comprovante = models.CharField(max_length=255, blank=True, null=True)
    conta = models.CharField(max_length=255, blank=True, null=True)
    digito = models.CharField(max_length=255, blank=True, null=True)
    bco = models.CharField(max_length=255, blank=True, null=True)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='cliente', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'domicilios'


class Enderecos(models.Model):
    id = models.IntegerField(primary_key=True)
    bairro = models.CharField(max_length=255, blank=True, null=True)
    cep = models.CharField(max_length=255, blank=True, null=True)
    cidade = models.CharField(max_length=255, blank=True, null=True)
    complemento = models.CharField(max_length=255, blank=True, null=True)
    comprovante = models.CharField(max_length=1024, blank=True, null=True)
    endereco = models.CharField(max_length=255, blank=True, null=True)
    estado = models.CharField(max_length=255, blank=True, null=True)
    numero = models.CharField(max_length=255, blank=True, null=True)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='cliente', blank=True, null=True)
    inscricao_estadual = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enderecos'


class Equipamentos(models.Model):
    numero_logico = models.CharField(primary_key=True, max_length=255)
    pedido = models.ForeignKey('Pedidos', models.DO_NOTHING, db_column='pedido', blank=True, null=True)
    numero_serie = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'equipamentos'


class Fases(models.Model):
    id = models.IntegerField(primary_key=True)
    fase = models.CharField(max_length=255, blank=True, null=True)
    fim = models.DateTimeField(blank=True, null=True)
    inicio = models.DateTimeField(blank=True, null=True)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='cliente', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fases'


class Pedidos(models.Model):
    pedido = models.IntegerField(primary_key=True)
    comprovante_maquina = models.CharField(max_length=1024, blank=True, null=True)
    emitir_boleto = models.CharField(max_length=255, blank=True, null=True)
    bairro = models.CharField(max_length=255, blank=True, null=True)
    cep = models.CharField(max_length=255, blank=True, null=True)
    cidade = models.CharField(max_length=255, blank=True, null=True)
    complemento = models.CharField(max_length=255, blank=True, null=True)
    endereco = models.CharField(max_length=255, blank=True, null=True)
    estado = models.CharField(max_length=255, blank=True, null=True)
    numero = models.CharField(max_length=255, blank=True, null=True)
    endereco_entrega_mesmo = models.CharField(max_length=255, blank=True, null=True)
    estorno = models.CharField(max_length=255, blank=True, null=True)
    forma_pagto = models.CharField(max_length=255, blank=True, null=True)
    maquina = models.CharField(max_length=255, blank=True, null=True)
    operadora = models.CharField(max_length=255, blank=True, null=True)
    proposta = models.CharField(max_length=255, blank=True, null=True)
    quantidade = models.IntegerField(blank=True, null=True)
    tipo_pagto = models.CharField(max_length=255, blank=True, null=True)
    cpf_vendedor = models.CharField(max_length=255, blank=True, null=True)
    email_vendedor = models.CharField(max_length=255, blank=True, null=True)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='cliente', blank=True, null=True)
    tipo_frete = models.CharField(max_length=3, blank=True, null=True)
    cod_nsu = models.CharField(max_length=255, blank=True, null=True)
    cod_prod = models.CharField(max_length=255, blank=True, null=True)
    qtde_prod = models.CharField(max_length=5, blank=True, null=True)
    tipo_pedido_venda = models.CharField(max_length=20, blank=True, null=True)
    dt_prev_entrega = models.DateTimeField(blank=True, null=True)
    dt_entrega = models.DateTimeField(blank=True, null=True)
    condicao_pagamento = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pedidos'


class Pessoas(models.Model):
    id = models.IntegerField(primary_key=True)
    cpf_cnpj = models.CharField(max_length=255, blank=True, null=True)
    data_nasc = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    rg = models.CharField(max_length=255, blank=True, null=True)
    tel1 = models.CharField(max_length=255, blank=True, null=True)
    tel2 = models.CharField(max_length=255, blank=True, null=True)
    cliente = models.ForeignKey(Clientes, models.DO_NOTHING, db_column='cliente', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pessoas'


class Users(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    token = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'users'
        unique_together = (('id', 'id', 'id'),)
