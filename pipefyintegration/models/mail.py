import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from django.conf import settings
from pprint import pprint
from email import encoders
from django.template import loader, Context


class Mail:
    @staticmethod
    def py_mail(SUBJECT, BODY, TO, FROM, cadastro, BLACKFRIDAY=False):
        # Create message container - the correct MIME type is multipart/alternative here!
        BCC = 'e-commerce@bevipag.com.br'
        MESSAGE = MIMEMultipart('alternative')
        MESSAGE['subject'] = SUBJECT
        MESSAGE['To'] = TO
        MESSAGE['Bcc'] = BCC
        MESSAGE['From'] = FROM
        MESSAGE.preamble = """
    Your mail reader does not support the report format."""

        # Record the MIME type text/html.
        HTML_BODY = MIMEText(BODY, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        MESSAGE.attach(HTML_BODY)

        filename = "formulario_credenciamento.pdf"

        # if BLACKFRIDAY is True and cadastro == 'Cliente Pessoa Física (CPF)':
        #     attachment = open(
        #         settings.BASE_DIR + '/media/PCAE credenciamento BEVIPAG v_ajust_contrato_REDE_tx_ajustada_ Bevimais D2_CPF.pdf',
        #         "rb")
        # elif BLACKFRIDAY is True and cadastro == 'Cliente Pessoa Jurídica (CNPJ)':
        #     attachment = open(
        #         settings.BASE_DIR + '/media/PCAE credenciamento BEVIPAG v_ajust_contrato_REDE_tx_ajustada_ Bevimais D2_CNPJ.pdf',
        #         "rb")
        # elif BLACKFRIDAY is False and cadastro == 'Cliente Pessoa Física (CPF)':
        #     attachment = open(
        #         settings.BASE_DIR + '/media/PCAE credenciamento BEVIPAG v_ajust_contrato_REDE_tx_ajustada_CPF.pdf',
        #         "rb")
        # elif BLACKFRIDAY is False and cadastro == 'Cliente Pessoa Jurídica (CNPJ)':
        #     attachment = open(
        #         settings.BASE_DIR + '/media/PCAE credenciamento BEVIPAG v_ajust_contrato_REDE_tx_ajustada_CNPJ.pdf',
        #         "rb")
        #
        # part = MIMEBase('application', 'octet-stream')
        # part.set_payload(attachment.read())
        # encoders.encode_base64(part)
        # part.add_header('Content-Disposition', "attachment; filename=" + filename)
        #
        # MESSAGE.attach(part)

        # The actual sending of the e-mail
        server = smtplib.SMTP('email-smtp.us-west-2.amazonaws.com:587')

        # Print debugging output when testing
        if __name__ == "__main__":
            server.set_debuglevel(1)

        # Credentials (if needed) for sending the mail
        password = "AqJf/MBKXpNqj+ON1H4BkBLHNqXK0X5AEYTJNAbYke0b"

        server.starttls()
        server.login('AKIAIFOZGG6XVC3XSB7Q', password)
        server.sendmail(FROM, [TO, BCC], MESSAGE.as_string())
        server.quit()

    @staticmethod
    def mountEmail(rota, cliente):
        template = loader.get_template('pipefyintegration/mail/complemento_compra_novo.html')
        content = template.render({
            'rota': rota,
            'cliente': cliente
        })

        return content

    @staticmethod
    def mountEmailRevendedor(rota):
        template = loader.get_template('pipefyintegration/mail/complemento_compra_revendedor.html')
        content = template.render({
            'rota': rota
        })

        return content

    @staticmethod
    def mountEmailInfoRevendedorCliente(revendedor, cliente):
        template = loader.get_template('pipefyintegration/mail/informacao_compra_revendedor_to_cliente.html')
        content = template.render({
            'revendedor': revendedor,
            'cliente': cliente
        })

        return content
