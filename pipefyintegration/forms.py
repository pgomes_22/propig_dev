from django import forms

estados = [('ESTADO', 'ESTADO'), ('AC', 'AC'), ('AL', 'AL'), ('AM', 'AM'), ('AP', 'AP'), ('BA', 'BA'), ('CE', 'CE'),
           ('DF', 'DF'), ('ES', 'ES'), ('GO', 'GO'), ('MA', 'MA'), ('MG', 'MG'), ('MS', 'MS'), ('MT', 'MT'),
           ('PA', 'PA'), ('PB', 'PB'), ('PE', 'PE'), ('PI', 'PI'), ('PR', 'PR'), ('RJ', 'RJ'), ('RN', 'RN'),
           ('RO', 'RO'),
           ('RR', 'RR'), ('RS', 'RS'), ('SC', 'SC'), ('SE', 'SE'), ('SP', 'SP'), ('TO', 'TO')]

operadoras = [
    ('Sem preferência', 'Sem preferência'),
    ('Vivo', 'Vivo'),
    ('Tim', 'Tim'),
    ('Oi', 'Oi'),
    ('Claro', 'Claro')
]

bancos = [
    ('001 - BANCO DO BRASIL', '001 - BANCO DO BRASIL'), ('003 - BANCO DA AMAZÔNIA', '003 - BANCO DA AMAZÔNIA'),
    ('004 - BANCO DO NORDESTE DO BRASIL', '004 - BANCO DO NORDESTE DO BRASIL'),
    ('021 - BANESTES - BANCO DO ESTADO DO ESPIRITO SANTO', '021 - BANESTES - BANCO DO ESTADO DO ESPIRITO SANTO'),
    ('025 - BANCO ALFA', '025 - BANCO ALFA'), ('033 - BANCO SANTANDER', '033 - BANCO SANTANDER'),
    ('037 - BANPARÁ', '037 - BANPARÁ'),
    ('041 - BANCO DO ESTADO DO RIO GRANDE DO SUL', '041 - BANCO DO ESTADO DO RIO GRANDE DO SUL'),
    ('047 - BANESE - BANCO DO ESTADO DE SERGIPE', '047 - BANESE - BANCO DO ESTADO DE SERGIPE'),
    ('070 - BRB - BANCO DE BRASILIA', '070 - BRB - BANCO DE BRASILIA'),
    ('077 - BANCO INTERMEDIUM', '077 - BANCO INTERMEDIUM'), ('082 - BANCO TOPÁZIO', '082 - BANCO TOPÁZIO'),
    ('084 - BANCO UNIPRIME NORTE DO PARANÁ', '084 - BANCO UNIPRIME NORTE DO PARANÁ'),
    ('085 - COOPERATIVA CENTRAL DE CRÉDITO - AILOS', '085 - COOPERATIVA CENTRAL DE CRÉDITO - AILOS'),
    ('089 - CREDISAN', '089 - CREDISAN'), ('104 - CAIXA ECONOMICA FEDERAL', '104 - CAIXA ECONOMICA FEDERAL'),
    ('107 - BANCO BOCOM BBM S.A.', '107 - BANCO BOCOM BBM S.A.'),
    ('130 - CARUANA S/A SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO',
     '130 - CARUANA S/A SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO'),
    ('136 - BANCO UNICRED DO BRASIL', '136 - BANCO UNICRED DO BRASIL'),
    ('213 - BANCO ARBI', '213 - BANCO ARBI'), ('224 - BANCO FIBRA', '224 - BANCO FIBRA'),
    ('237 - BANCO BRADESCO', '237 - BANCO BRADESCO'),
    ('243 - BANCO MÁXIMA S.A.', '243 - BANCO MÁXIMA S.A.'), ('246 - BANCO ABC BRASIL', '246 - BANCO ABC BRASIL'),
    ('341 - BANCO ITAÚ', '341 - BANCO ITAÚ'), ('376 - BANCO J.P. MORGAN S.A', '376 - BANCO J.P. MORGAN S.A'),
    ('389 - BANCO MERCANTIL DO BRASIL', '389 - BANCO MERCANTIL DO BRASIL'), ('422 - BANCO SAFRA', '422 - BANCO SAFRA'),
    ('604 - BANCO INDUSTRIAL DO BRASIL ', '604 - BANCO INDUSTRIAL DO BRASIL '),
    ('611 - BANCO PAULISTA', '611 - BANCO PAULISTA'), ('612 - BANCO GUANABARA', '612 - BANCO GUANABARA'),
    ('630 - BANCO INTERCAP S.A.', '630 - BANCO INTERCAP S.A.'), ('634 - BANCO TRIANGULO', '634 - BANCO TRIANGULO'),
    ('637 - BANCO SOFISA', '637 - BANCO SOFISA'), ('643 - BANCO PINE S.A.', '643 - BANCO PINE S.A.'),
    ('655 - BANCO VOTORANTIM', '655 - BANCO VOTORANTIM'), ('707 - DAYCOVAL', '707 - DAYCOVAL'),
    ('741 - BANCO RIBEIRAO PRETO', '741 - BANCO RIBEIRAO PRETO'), ('743 - BANCO SEMEAR', '743 - BANCO SEMEAR'),
    ('745 - BANCO CITIBANK', '745 - BANCO CITIBANK'),
    ('748 - BANCO COOPERATIVO SICREDI', '748 - BANCO COOPERATIVO SICREDI'),
    ('755 - BANCO MERRILL LYNCH', '755 - BANCO MERRILL LYNCH'),
    ('756 - BANCO COOPERATIVO DO BRASIL', '756 - BANCO COOPERATIVO DO BRASIL'),
]


class SalesForm(forms.Form):
    """cadastro = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Cadastro', 'readonly':'readonly'}))"""

    """nome_completo = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Nome Completo', 'readonly':'readonly'}))"""

    """razao_social = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Razão Social', 'readonly':'readonly'}))"""

    rg = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'RG'}))

    """cpf = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'CPF', 'readonly': 'readonly'}))"""

    """cnpj = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'CNPJ'}))"""

    """telefone1 = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Telefone 1', 'readonly':'readonly'}))"""

    """telefone2 = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Telefone 2'}))"""

    """email = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Email', 'readonly':'readonly'}))"""

    """rua_av = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'RUA/AV', 'readonly':'readonly'}))"""

    """numero = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Número', 'readonly':'readonly'}))"""

    """complemento = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Complemento', 'readonly':'readonly'}))"""

    """bairro = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Bairro', 'readonly':'readonly'}))"""

    """cidade = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Cidade', 'readonly':'readonly'}))"""

    """estado = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Estado', 'readonly':'readonly'}))"""

    domicilio_bancario = forms.CharField(required=False, widget=forms.Select(choices=bancos, attrs={'class': 'form-control'}))

    agencia = forms.CharField(max_length=15, required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Agência'}))

    conta = forms.CharField(max_length=15, required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Conta'}))

    digito_conta = forms.CharField(max_length=1, required=False, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Dígito da conta'}))

    tipo_conta = forms.CharField(
        widget=forms.Select(choices=[('TIPO DE CONTA', 'TIPO DE CONTA'), ('Conta corrente', 'Conta corrente')],
                            attrs={'class': 'form-control'}))

    # codigo_tipo_conta = forms.CharField(required=False,
    #                                     widget=forms.Select(choices=[
    #                                         ('1', 'Conta Corrente de Pessoa Física'),
    #                                         ('3', 'Conta Corrente de Pessoa Jurídica'),
    #                                         ('6', 'Entidades Públicas')],
    #                                         attrs={'class': 'form-control'}))

    cliente_flex = forms.CharField(required=False, widget=forms.Select(
        choices=[('ACEITA FLEX?', 'ACEITA FLEX?'), ('Sim', 'Sim'), ('Não', 'Não')], attrs={'class': 'form-control'}))

    ficha_cadastro = forms.FileField()

    # cpf_rg_cnh = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))

    comprovante_domicilio_bancario = forms.FileField()

    """tipo_maquina = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Tipo de máquina', 'readonly':'readonly'}))"""

    quantidade_maquina = forms.CharField(max_length=3, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Quantidade'}))

    operadora = forms.CharField(widget=forms.Select(choices=operadoras, attrs={'class': 'form-control'}))

    """forma_pagamento = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Forma de pagamento', 'readonly':'readonly'}))"""

    comprovante_pagamento_maquina = forms.FileField()

    cpf = forms.FileField()

    documento_foto = forms.FileField()

    cnh = forms.FileField()


class FormResendMail(forms.Form):
    pedidos = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control'}), required=True)
