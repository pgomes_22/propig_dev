from django.urls import path
from . import views

urlpatterns = [
    path('teste-email', views.test_mail, name='teste-email'),
    path('new-sale', views.new_sale_ecommerce, name='new-sale'),
    path('resend-mails', views.view_resend_mail, name='resend-mails'),
    path('adicional-maquinas', views.adicional_maquina_add, name='adicional-maquinas'),
    path('calculo-valor-final', views.calculo_valor_final, name='calculo-valor-final'),
    path('find-card-documetacao-ecommerce', views.find_card_agente_ecommerce, name='find-card-documetacao-ecommerce'),
    path('create-pipe-backoffice/<int:card_id>', views.create_pipe_manual, name='create-pipe-backoffice'),
    path('completa-cadastro/<str:token>/<int:card_id>/<str:record_id>', views.form_client_ecommerce,
         name='completa-cadastro'),
    path('migra-cards', views.migrate_cards, name='migra-cards'),
    path('download/<str:path>', views.download, name='download'),
]
