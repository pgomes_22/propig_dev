from django import forms


bancos = [
('001', '001 - BANCO DO BRASIL'), ('003', '003 - BANCO DA AMAZÔNIA'),
('004', '004 - BANCO DO NORDESTE DO BRASIL'), ('021', '021 - BANESTES - BANCO DO ESTADO DO ESPIRITO SANTO'),
('025', '025 - BANCO ALFA'), ('033', '033 - BANCO SANTANDER'),
('037', '037 - BANPARÁ'), ('041', '041 - BANCO DO ESTADO DO RIO GRANDE DO SUL'),
('047', '047 - BANESE - BANCO DO ESTADO DE SERGIPE'),
('070', '070 - BRB - BANCO DE BRASILIA'),
('077', '077 - BANCO INTERMEDIUM'), ('082', '082 - BANCO TOPÁZIO'),
('084', '084 - BANCO UNIPRIME NORTE DO PARANÁ'),
('085', '085 - COOPERATIVA CENTRAL DE CRÉDITO - AILOS'),
('089', '089 - CREDISAN'),  ('104', '104 - CAIXA ECONOMICA FEDERAL'),
('107', '107 - BANCO BOCOM BBM S.A.'),
('130', '130 - CARUANA S/A SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO'),
('136', '136 - BANCO UNICRED DO BRASIL'),
('213', '213 - BANCO ARBI'), ('224', '224 - BANCO FIBRA'), ('237', '237 - BANCO BRADESCO'),
('243', '243 - BANCO MÁXIMA S.A.'), ('246', '246 - BANCO ABC BRASIL'),
('341', '341 - BANCO ITAÚ'), ('376', '376 - BANCO J.P. MORGAN S.A'),
('389', '389 - BANCO MERCANTIL DO BRASIL'), ('422', '422 - BANCO SAFRA'),
('604', '604 - BANCO INDUSTRIAL DO BRASIL'),
('611', '611 - BANCO PAULISTA'), ('612', '612 - BANCO GUANABARA'),
('630', '630 - BANCO INTERCAP S.A.'), ('634', '634 - BANCO TRIANGULO'),
('637', '637 - BANCO SOFISA'), ('643', '643 - BANCO PINE S.A.'),
('655', '655 - BANCO VOTORANTIM'), ('707', '707 - DAYCOVAL'),
('741', '741 - BANCO RIBEIRAO PRETO'), ('743', '743 - BANCO SEMEAR'),
('745', '745 - BANCO CITIBANK'),  ('748', '748 - BANCO COOPERATIVO SICREDI'),
('755', '755 - BANCO MERRILL LYNCH'), ('756', '756 - BANCO COOPERATIVO DO BRASIL'),
]

parcelas = [
    ("1x", "1x A vista"),
    ("2x", "2x"),
    ("3x", "4x"),
    ("4x", "4x"),
    ("5x", "5x"),
    ("6x", "6x"),
    ("7x", "7x"),
    ("8x", "8x"),
    ("9x", "9x"),
    ("10x", "10x"),
    ("11x", "11x"),
    ("11x", "11x"),
]

class NewClientForm(forms.Form):
    # ENDEREÇO
    cep = forms.CharField(max_length=8, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CEP'}))
    uf = forms.CharField(max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'UF'}))
    cidade = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CIDADE'}))
    endereco = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'ENDEREÇO'}))
    bairro = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'BAIRRO'}))
    numero_endereco = forms.CharField(max_length=8, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NÚMERO'}))
    complemento = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPLEMENTO'}))
    pais = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PAÍS'}))

    tipo_endereco = forms.CharField(widget=forms.Select(
        choices=[("1", "PRINCIPAL"), ("2", "CORRESPONDÊNCIA"), ("3", "INSTALAÇÃO")], attrs={'class': 'form-control'}))

    # DADOS DA CONTA BANCÁRIA

    banco = forms.CharField(widget=forms.Select(
        choices=bancos, attrs={'class': 'form-control'}))

    agencia = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'AGÊNCIA'}))

    conta = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CONTA'}))

    digito_conta = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DÍGITO DA CONTA'}))

    tipo_conta = forms.CharField(widget=forms.Select(
        choices=[("Corrente", "CORRENTE"), ("Poupanca", "POUPANÇA")], attrs={'class': 'form-control'}))

    maquina_cessao = forms.CharField(widget=forms.Select(
        choices=[("", "ESCOLHA"), ("venda", "VENDA"), ("aluguel", "ALUGUEL"), ("cessao", "CESSÃO")], attrs={'class': 'form-control'}))

    foto_documento = forms.FileField()

    # pagamento
    tipo_pagamento = forms.CharField(widget=forms.Select(
        choices=[("cartao", "CARTÃO")], attrs={'class': 'form-control'}))

    parcelamento = forms.CharField(widget=forms.Select(
        choices=parcelas, attrs={'class': 'form-control'}))

    nsu = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPROVANTE DE PAGAMENTO'}))

