from django.urls import path
from . import views

urlpatterns = [
    path('', views.new_client, name='sale'),
    path('novo-cliente/', views.new_client, name='new-client'),
    path('cadastro-cliente/', views.new_insert, name='cadastro-cliente'),
    path('novo-pedido-recorrente/<int:id_cliente>/', views.tela_pedido, name='pedido-recorrente'),
    path('realiza-pedido-recorrente/', views.novo_pedido, name='realiza-pedido-recorrente'),
    path('meus-pedidos/', views.meus_pedidos, name='meus-pedidos'),
    path('meus-clientes/', views.meus_clientes, name='meus-clientes'),
    path('contato-responsavel/<int:lojista_id>', views.alteracao_contato, name='contato-responsavel'),
    path('taxas/<int:lojista_id>', views.alteracao_taxa, name='taxas'),
    path('endereco/<int:lojista_id>', views.alteracao_endereco, name='endereco'),
    path('dados-operadora', views.operadora_view, name='operadora'),
]
