from django.shortcuts import render
from dashboard.models import Lojista, IdWall, EnderecoLojista, DomicilioBancario, Cessao, \
    Pagamento, Pedido, Taxas, Responsavel, Transacoes, LojistaTerminal, Terminais, Planos, Distribuidores
from dashboard.forms import ConsultaDocForm, PagamentoForm, ResponsavelForm, TaxaForm, EnderecoForm
from .forms import NewClientForm
from datetime import datetime
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from pprint import pprint
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import secrets
import boto3
from django.db import IntegrityError
import json
from rest_framework.response import Response
from rest_framework import status


@login_required(login_url='/dashboard/login/')
def sale(request):
    lojistas = Lojista.objects.filter(resultado_idwall='VALID')
    pprint(request.GET.get('venda'))
    form = ConsultaDocForm(request.POST)
    return render(request, 'comercial/nova_venda.html', {
        'lojistas': lojistas,
        'logged_user': request.user,
        'form': form,
        'venda_sucesso': True if request.GET.get('venda') == 'success' else False
    })


@login_required(login_url='/dashboard/login/')
def new_client(request):
    form = ConsultaDocForm(request.POST)
    planos = Planos.objects.all()
    distribuidores = Distribuidores.objects.all()
    return render(request, 'comercial/novo_cliente.html', {
        'logged_user': request.user,
        'form': form,
        'planos': planos,
        'distribuidores': distribuidores
    })


@login_required(login_url='/dashboard/login/')
def new_insert(request):
    planos = Planos.objects.all()
    if request.method == 'POST':
        s3 = boto3.client(
            's3',
            aws_access_key_id='AKIAJ3VU2NH6RNPUXSUA',
            aws_secret_access_key='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'
        )
        bucket_name = 'doccessaolucree'

        form = ConsultaDocForm(request.POST)

        if form.is_valid():
            tipo = form.cleaned_data['tipo_doc']
            numero = form.cleaned_data['numero']  # cpf/cnpj
            result_consulta = IdWall.consulta_documento(tipo, numero)

            if result_consulta["result"]["numero"]:
                consulta_resultado = IdWall.consulta_resultado(result_consulta["result"]["numero"])
                if consulta_resultado["result"]:
                    try:
                        lojista = Lojista()
                        lojista.documento = numero
                        lojista.mensagem_idwall = consulta_resultado["result"]["mensagem"] \
                            if "message" in consulta_resultado["result"] else ""
                        lojista.matriz_idwall = consulta_resultado["result"]["nome"] \
                            if "nome" in consulta_resultado["result"] else ""
                        lojista.numero_idwall = consulta_resultado["result"]["numero"] \
                            if "numero" in consulta_resultado["result"] else ""
                        lojista.status_idwall = consulta_resultado["result"]["status"] \
                            if "status" in consulta_resultado["result"] else ""

                        lojista.operadora = form.cleaned_data["operadora"]
                        lojista.departamento = int(form.cleaned_data["departamento_empresa"])
                        lojista.email_vendedor_responsavel = request.user.email

                        plano = Planos.objects.get(id=int(form.cleaned_data['plano']))

                        if plano.tipo_contratacao == 'CESSAO':
                            distribuidor = Distribuidores.objects.get(id=int(form.cleaned_data['distribuidores']))
                            lojista.distribuidor = distribuidor

                        lojista.conta_favorecido = distribuidor.conta if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.digito_conta_favorecido = distribuidor.digito_conta if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.banco_favorecido = distribuidor.banco if plano.tipo_contratacao == 'CESSAO' else 0
                        lojista.agencia_favorecido = distribuidor.agencia if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.tipo_conta_favorecido = distribuidor.tipo_conta if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.cnpj_favorecido = distribuidor.cnpj if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.nome_razao_favorecido = distribuidor.razao_social if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.porcentagem_cliente = distribuidor.porcentagem_cliente if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.porcentagem_favorecido = distribuidor.porcentagem_favorecido if plano.tipo_contratacao == 'CESSAO' else ""
                        lojista.num_terminais = form.cleaned_data['num_maquinas']
                        lojista.plano = plano

                        pprint(lojista.__dict__)
                        lojista.save()

                        endereco = {
                            "logradouro": form.cleaned_data["endereco"],
                            "numero": form.cleaned_data["numero_endereco"],
                            "complemento": form.cleaned_data["complemento"],
                            "bairro": form.cleaned_data["bairro"],
                            "cidade": form.cleaned_data["cidade"],
                            "uf": form.cleaned_data["uf"],
                            "cep": form.cleaned_data["cep"],
                            "tipo": form.cleaned_data["tipo_endereco"],
                            "principal": True if form.cleaned_data["tipo_endereco"] == "PRINCIPAL" else False
                        }
                        save_endereco(lojista, endereco)

                        if form.cleaned_data["maquina_cessao"] == 'venda':
                            domicilio = {
                                "banco": form.cleaned_data["banco"],
                                "agencia": form.cleaned_data["agencia"],
                                "conta": form.cleaned_data["conta"],
                                "digito_conta": form.cleaned_data["digito_conta"],
                                "tipo_conta": form.cleaned_data["tipo_conta"]
                            }
                            save_domicilio_bancario(lojista, domicilio)

                            pagamento = {
                                "tipo_pagamento": form.cleaned_data["tipo_pagamento"],
                                "parcelas": form.cleaned_data["parcelamento"],
                                "nsu": form.cleaned_data["nsu"],
                                "data_pagamento": form.cleaned_data["data_pagamento"],
                            }

                            pagamento = save_pagamento(lojista, pagamento, False)

                            pprint(count_pedidos())
                            pedido = {
                                "numero_pedido": count_pedidos(),
                                "pagamento": pagamento,
                                "comercial": request.user
                            }
                            save_pedido(lojista, pedido, "")

                        elif form.cleaned_data["maquina_cessao"] == 'aluguel':
                            domicilio = {
                                "banco": form.cleaned_data["banco"],
                                "agencia": form.cleaned_data["agencia"],
                                "conta": form.cleaned_data["conta"],
                                "digito_conta": form.cleaned_data["digito_conta"],
                                "tipo_conta": form.cleaned_data["tipo_conta"]
                            }
                            save_domicilio_bancario(lojista, domicilio)

                        elif form.cleaned_data["maquina_cessao"] == 'cessao':
                            foto = upload(request.FILES['foto_documento'])
                            upload_s3(s3, bucket_name, foto)
                            url_foto_documento = "https://s3.amazonaws.com/{0}{1}".format('doccessaolucree',
                                                                                          foto)
                            cessao = {
                                "maquina_cessao": form.cleaned_data["maquina_cessao"],
                                "foto_documento": url_foto_documento
                            }
                            save_cessao(lojista, cessao)
                            remove(settings.BASE_DIR + foto)

                        taxas = {
                            "taxa_debito": str(plano.taxa_debito),
                            "taxa_credito_a_vista": str(plano.taxa_credito),
                            "taxa_credito_parcelado_emissor": str(plano.taxa_credito),
                            "parcelado_loja_dois_seis": str(plano.taxa_parcelado),
                            "parcelado_loja_sete_doze": str(plano.taxa_parcelado)
                        }
                        save_taxas(lojista, taxas)

                        responsavel = {
                            "nome": form.cleaned_data["nome_responsavel"],
                            "documento": form.cleaned_data["documento_responsavel"],
                            "tipo": form.cleaned_data["tipo_responsavel"],
                            "data_nascimento": form.cleaned_data["nascimento_responsavel"],
                            "sexo": form.cleaned_data["sexo_responsavel"],
                            "ddd": form.cleaned_data["ddd_telefone_responsavel"],
                            "telefone": form.cleaned_data["telefone_responsavel"],
                            "email": form.cleaned_data["email_responsavel"],
                            "nacionalidade": form.cleaned_data["nacionalidade_responsavel"],
                        }

                        save_responsavel(lojista, responsavel)

                        if request.user.profile.location != 'comercial':
                            return HttpResponseRedirect("/dashboard/merchants/")
                        else:
                            return HttpResponseRedirect("/comercial/?venda=success")

                    except IntegrityError as e:
                        pprint(e)
                        return render(request, 'comercial/novo_cliente.html', {
                            'logged_user': request.user,
                            'form': form,
                            'planos': planos,
                            'duplicated_user': True,

                        })
                else:
                    return JsonResponse({"resultado": "sem resultado"})
        else:
            return render(request, 'comercial/novo_cliente.html', {
                'logged_user': request.user,
                'planos': planos,
                'form': form,
                'form_errors': True
            })


@login_required(login_url='/dashboard/login/')
def tela_pedido(request, id_cliente):
    lojista = Lojista.objects.get(id=id_cliente)
    form = PagamentoForm(initial={'hidden_client_id': id_cliente})
    return render(request, 'comercial/novo_pedido_cliente_existente.html', {
        'logged_user': request.user,
        'form': form,
        'lojista': lojista
    })


@login_required(login_url='/dashboard/login/')
def novo_pedido(request):
    if request.method == 'POST':
        form = PagamentoForm(request.POST)
        lojista = Lojista.objects.get(id=request.POST.get("hidden_client_id"))
        if form.is_valid():
            pagamento = {
                "tipo_pagamento": form.cleaned_data["tipo_pagamento_simples"],
                "parcelas": form.cleaned_data["parcelamento_simples"],
                "nsu": form.cleaned_data["nsu_simples"],
                "data_pagamento": form.cleaned_data["data_pagamento_simples"],
            }
            pagamento = save_pagamento(lojista, pagamento, True)
            pedido = {
                "numero_pedido": count_pedidos(),
                "pagamento": pagamento
            }
            save_pedido(lojista, pedido, "autorizado")

            responsavel = {
                "nome": form.cleaned_data["nome_responsavel"],
                "documento": form.cleaned_data["documento_responsavel"],
                "tipo": form.cleaned_data["tipo_responsavel"],
                "data_nascimento": form.cleaned_data["nascimento_responsavel"],
                "sexo": form.cleaned_data["sexo_responsavel"],
                "ddd": form.cleaned_data["ddd_telefone_responsavel"],
                "telefone": form.cleaned_data["telefone_responsavel"],
                "email": form.cleaned_data["email_responsavel"],
                "nacionalidade": form.cleaned_data["nacionalidade_responsavel"],
            }

            save_responsavel(lojista, responsavel)

            return HttpResponseRedirect('/comercial/?venda=success')
        else:
            return render(request, 'comercial/novo_pedido_cliente_existente.html', {
                'logged_user': request.user,
                'form': form,
                'lojista': lojista,
                'erro_validacao': True,
                'erros': form.errors
            })


def file_extension(name):
    split = name.split(".")
    return split[1]


def upload(arquivo):
    fs = FileSystemStorage()
    filename = fs.save(secrets.token_hex(10) + "." + file_extension(arquivo.name), arquivo)
    return fs.url(filename)


def remove(filename):
    fs = FileSystemStorage()
    fs.delete(filename)


def upload_s3(s3, bucket_name, uploaded_file_url):
    s3.upload_file(settings.BASE_DIR + uploaded_file_url, bucket_name, uploaded_file_url.lstrip("/"),
                   {'ACL': 'public-read'})


def count_pedidos():
    return Pedido.objects.count() + 1


def save_responsavel(lojista, campos):
    responsavel = Responsavel()
    responsavel.nome = campos["nome"]
    responsavel.documento = campos["documento"]
    responsavel.tipo = campos["tipo"]
    responsavel.data_nascimento = campos["data_nascimento"]
    responsavel.sexo = campos["sexo"]
    responsavel.ddd = campos["ddd"]
    responsavel.telefone = campos["telefone"]
    responsavel.email = campos["email"]
    responsavel.nacionalidade = campos["nacionalidade"]
    responsavel.lojista = lojista
    responsavel.save()


def save_taxas(lojista, campos):
    taxa = Taxas()
    taxa.debito = campos["taxa_debito"]
    taxa.credito_a_vista = campos["taxa_credito_a_vista"]
    taxa.credito_parcelado_emissor = campos["taxa_credito_parcelado_emissor"]
    taxa.parcelado_loja_dois_seis = campos["parcelado_loja_dois_seis"]
    taxa.parcelado_loja_sete_doze = campos["parcelado_loja_sete_doze"]
    taxa.lojista = lojista
    taxa.data_cadastro = datetime.now()
    taxa.data_update = datetime.now()
    taxa.save()


def save_endereco(lojista, campos):
    endereco = EnderecoLojista()
    endereco.lojista = lojista
    endereco.logradouro = campos["logradouro"]
    endereco.numero = campos["numero"]
    endereco.complemento = campos["complemento"]
    endereco.bairro = campos["bairro"]
    endereco.cep = campos["cep"]
    endereco.bairro = campos["bairro"]
    endereco.cidade = campos["cidade"]
    endereco.estado = campos["uf"]
    endereco.tipo = campos["tipo"]
    endereco.principal = campos["principal"]
    endereco.save()


def save_domicilio_bancario(lojista, campos):
    domicilio = DomicilioBancario()
    domicilio.lojista = lojista
    domicilio.banco = campos["banco"]
    domicilio.agencia = campos["agencia"]
    domicilio.conta = campos["conta"]
    domicilio.digito_conta = campos["digito_conta"]
    domicilio.tipo_conta = campos["tipo_conta"]
    domicilio.data_cadastro = datetime.now()
    domicilio.data_update = datetime.now()
    domicilio.save()


def save_cessao(lojista, campos):
    cessao = Cessao()
    cessao.lojista = lojista
    cessao.maquina_cessao = campos["maquina_cessao"]
    cessao.foto_documento = campos["foto_documento"]
    cessao.data_cadastro = datetime.now()
    cessao.data_update = datetime.now()
    cessao.save()


def save_pagamento(lojista, campos, status_pagamento):
    pagamento = Pagamento()
    pagamento.lojista = lojista
    pagamento.tipo_pagamento = campos["tipo_pagamento"]
    pagamento.parcelas = campos["parcelas"]
    pagamento.pagamento_valido = status_pagamento
    pagamento.nsu = campos["nsu"]
    pagamento.data_pagamento = invertDate(campos["data_pagamento"])
    pagamento.data_cadastro = datetime.now()
    pagamento.data_update = datetime.now()
    pagamento.save()

    return pagamento


def invertDate(data):
    try:
        array_data = data.split("-")
        return array_data[2] + "-" + array_data[1] + "-" + array_data[0]
    except IndexError:
        array_data = data.split("/")
        return array_data[2] + "-" + array_data[1] + "-" + array_data[0]


def save_pedido(lojista, campos, status_pagamento):
    pedido = Pedido()
    pedido.lojista = lojista
    pedido.numero_pedido = campos["numero_pedido"]
    pedido.status_pedido = "processando" if status_pagamento == '' else status_pagamento
    pedido.pagamento = campos["pagamento"]
    pedido.comercial = campos["comercial"]
    pedido.data_cadastro = datetime.now()
    pedido.data_update = datetime.now()
    pedido.save()


@login_required(login_url='/dashboard/login/')
def meus_pedidos(request):
    pedidos = Pedido.objects.filter(comercial=request.user).select_related('lojista', 'pagamento')

    return render(request, 'comercial/meus_pedidos.html', {
        'pedidos': pedidos,
        'logged_user': request.user
    })


@login_required(login_url='/dashboard/login/')
def meus_clientes(request):
    clientes = Lojista.objects.filter(email_vendedor_responsavel=request.user.email)

    return render(request, 'comercial/meus_clientes.html', {
        'lojistas': clientes,
        'logged_user': request.user
    })


@login_required(login_url='/dashboard/login/')
def alteracao_contato(request, lojista_id):
    lojista = Lojista.objects.get(id=lojista_id)
    form = ResponsavelForm(request.POST)
    if request.method == 'POST':
        pprint('ENTRA NO POST!!')
        responsavel = Responsavel()
        responsavel.nome = request.POST.get('nome_responsavel')
        responsavel.documento = request.POST.get('documento_responsavel')
        responsavel.tipo = request.POST.get('tipo_responsavel')
        responsavel.data_nascimento = request.POST.get('nascimento_responsavel')
        responsavel.sexo = request.POST.get('sexo_responsavel')
        responsavel.ddd = request.POST.get('ddd_telefone_responsavel')
        responsavel.telefone = request.POST.get('telefone_responsavel')
        responsavel.email = request.POST.get('email_responsavel')
        responsavel.nacionalidade = request.POST.get('nacionalidade_responsavel')
        responsavel.lojista = lojista
        lojista.mensagem_integrador = ''
        lojista.save()
        responsavel.save()
        return render(request, 'comercial/contato_responsavel.html', {
            'lojista': lojista,
            'logged_user': request.user,
            'form': form,
            'success': True
        })

    return render(request, 'comercial/contato_responsavel.html', {
        'lojista': lojista,
        'logged_user': request.user,
        'form': form
    })


@login_required(login_url='/dashboard/login/')
def alteracao_taxa(request, lojista_id):
    lojista = Lojista.objects.get(id=lojista_id)
    form = TaxaForm(request.POST)
    if request.method == 'POST':
        if request.POST.get('plano') == 'A':
            taxas = {
                "taxa_debito": '1.99',
                "taxa_credito_a_vista": '2.99',
                "taxa_credito_parcelado_emissor": '2.99',
                "parcelado_loja_dois_seis": '3.78',
                "parcelado_loja_sete_doze": '3.78'
            }
            save_taxas(lojista, taxas)
        else:
            taxas = {
                "taxa_debito": '2.39',
                "taxa_credito_a_vista": '3.39',
                "taxa_credito_parcelado_emissor": '3.39',
                "parcelado_loja_dois_seis": '3.98',
                "parcelado_loja_sete_doze": '3.98'
            }
            save_taxas(lojista, taxas)
        lojista.mensagem_integrador = ''
        lojista.save()
        return render(request, 'comercial/taxas.html', {
            'lojista': lojista,
            'logged_user': request.user,
            'form': form,
            'success': True
        })
    return render(request, 'comercial/taxas.html', {
        'lojista': lojista,
        'logged_user': request.user,
        'form': form
    })


@login_required(login_url='/dashboard/login/')
def alteracao_endereco(request, lojista_id):
    lojista = Lojista.objects.get(id=lojista_id)
    form = EnderecoForm(request.POST)

    if request.method == 'POST':
        endereco = {
            "logradouro": request.POST.get("endereco"),
            "numero": request.POST.get("numero_endereco"),
            "complemento": request.POST.get("complemento"),
            "bairro": request.POST.get("bairro"),
            "cidade": request.POST.get("cidade"),
            "uf": request.POST.get("uf"),
            "cep": request.POST.get("cep"),
            "tipo": request.POST.get("tipo_endereco"),
            "principal": True if request.POST.get("tipo_endereco") == "PRINCIPAL" else False
        }
        save_endereco(lojista, endereco)
        lojista.mensagem_integrador = ''
        lojista.save()
        return render(request, 'comercial/endereco.html', {
            'lojista': lojista,
            'logged_user': request.user,
            'form': form,
            'success': True
        })
    return render(request, 'comercial/endereco.html', {
        'lojista': lojista,
        'logged_user': request.user,
        'form': form
    })


@login_required(login_url='/dashboard/login/')
def operadora_view(request):
    lojistas = Lojista.objects.filter(cnpj_favorecido=request.user.profile.bio)
    transacoes_list = []
    conta_transacao = 0
    for l in lojistas:
        terminais_lojista = LojistaTerminal.objects.select_related('terminal_id').filter(lojista_id=l)
        for tl in terminais_lojista:
            transacoes = Transacoes.objects.filter(tef_terminal=tl.terminal_id.terminal_id_physical, status=1)

            for tr in transacoes:
                dictfinal = {}
                a = '{:,.2f}'.format(float(tr.value))
                b = a.replace(',', 'v')
                c = b.replace('.', ',')
                conta_transacao = conta_transacao + float(tr.value)
                dictfinal['lojista_id'] = tl.lojista_id.id
                dictfinal['lojista_nome'] = tl.lojista_id.nome
                dictfinal['transacao_valor'] = c.replace('v', '.')
                dictfinal['transacao_bandeira'] = tr.brand
                dictfinal['transacao_acquirer_nsu'] = tr.acquirer_nsu
                dictfinal['tipo_pagamento'] = tr.product_name
                dictfinal['parcelas'] = tr.parcels
                dictfinal['data_pagamento'] = tr.payment_date
                transacoes_list.append(dictfinal)

    a = '{:,.2f}'.format(conta_transacao)
    b = a.replace(',', 'v')
    c = b.replace('.', ',')

    total = c.replace('v', '.')
    return render(request, 'comercial/operadora_view.html', {
        'transacoes': transacoes_list,
        'logged_user': request.user,
        'total_transacoes': total
    })
