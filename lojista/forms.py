from django import forms

class MachineForm(forms.Form):
    
    #DADOS COMERCIAIS
    comunication_profile = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'CÓDIGO DA MAQUININHA'}))
    merchant = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'CÓDIGO DO LOJISTA'}))
    terminal = forms.CharField(max_length=500, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'NÚMERO DO TERMINAL'}))
    