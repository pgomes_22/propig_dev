from django.apps import AppConfig


class LogistaConfig(AppConfig):
    name = 'lojista'
