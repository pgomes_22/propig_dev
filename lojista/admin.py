from django.contrib import admin

# Register your models here.
from .models import AtivacaoTerminal

class AtivacaoAdmin(admin.ModelAdmin):
    list_display = ('communication_profile', 'merchant', 'terminal')
    search_fields = ('communication_profile', 'merchant', 'terminal')

admin.site.register(AtivacaoTerminal, AtivacaoAdmin)