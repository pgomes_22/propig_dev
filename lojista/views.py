from django.http import HttpResponse
from django.shortcuts import render
from .models import PayStore, AtivacaoTerminal
import json
from django.http import JsonResponse
from .forms import MachineForm
from pprint import pprint


def ativacao(request):
    form = MachineForm()
    return render(request, 'lojista/ativacao.html', {'form': form})


def ativar(request):
    if (request.method == "POST"):
        newDict = {
            'communication_profile': request.POST.get('comunication_profile', ""),
            'merchant': request.POST.get('merchant', ""),
            'terminal':  request.POST.get('terminal', ""),
        }
        register = PayStore.saveTerminal(json.dumps(newDict))
        pprint(register)
    return JsonResponse(register)


