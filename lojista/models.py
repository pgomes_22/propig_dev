from django.db import models
import requests
from django.conf import settings
from pprint import pprint
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class AtivacaoTerminal(models.Model):
    communication_profile = models.CharField(max_length=200)
    merchant = models.CharField(max_length=10)
    terminal = models.CharField(max_length=15)

    def __str__(self):
        return 'ativacao_terminal'


class PayStore(models.Model):
    @classmethod
    def saveTerminal(self, terminal):
        end_point = settings.PAYSTORE_END_POINT + "terminal" #falta o endpoint
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer "+ settings.PAYSTORE_TOKEN,
        }
        results = requests.post(end_point, headers=headers, data=terminal)
        return results.json()