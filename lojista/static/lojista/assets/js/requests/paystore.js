/* Salvar Lojista */
$(document).ready(function() {
  var csrftoken = getCookie("csrftoken"); //static/assets/config/config.js
  $("#post-activation").on("submit", function(event) {
    event.preventDefault();
    activate_machine();
  });

  function activate_machine() {
    $("#fieldsError").html("");
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        $("#modal-title-default").html("Enviando...");
        $("#btnOk").css("display", "none");
        $("#titleError").html('Enviando...');
        /* for (var i = 0; i < 100; i++) {
          (function (i) {
            setTimeout(function () {
              $("#titleError").html('<div class="progress">'+
              '<div class="progress-bar bg-default" role="progressbar" '+
              ' aria-valuenow="'+i+'" aria-valuemin="0" aria-valuemax="100" '+
              ' style="width: '+i+'%;"></div></div>');
              
            }, 100*i);
          })(i);
        }; */
        
        $("#modal-sucess").modal("show");
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
      }
    });
    $.ajax({
      url: "/lojista/ativar/",
      type: "POST",
      data: {
        comunication_profile: $("#id_comunication_profile").val(),
        merchant: $("#id_merchant").val(),
        terminal: $("#id_terminal").val()
      },
      success: function(json) {
        $("#btnOk").removeAttr("href");
        if (json.message) {
          $("#btnOk").attr("data-dismiss", "modal");
          $("#titleError").html(json.message);
          $("#modal-title-default").html("Falha ao ativar a maquininha");
          var erros = "";
          for (const err in json.errors) {
            if (json.errors.hasOwnProperty(err)) {
              const element = json.errors[err];
              erros = erros + element + "<br>";
            }
            $("#fieldsError").html(erros);
          }
        } else {
          $("#modal-title-default").html("Maquininha ativada com sucesso!");
          var textToken =
            "Terminal ativado com sucesso, este é o seu token de ativação: " +
            json.token +
            ", insira-o em sua maquininha para ativa-la.";
          $("#titleError").html(textToken);
        }
        $("#modal-sucess").modal("show");
      },
      error: function(xhr, errmsg, err) {
        console.log(xhr.status + ": " + xhr.responseText);
      }
    });
  }
});
