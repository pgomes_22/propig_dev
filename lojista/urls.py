from django.urls import path

from . import views

urlpatterns = [
    #path('', views.index, name='index'),
    path('ativacao/', views.ativacao, name='ativacao'),
    path('ativar/', views.ativar, name='ativar'),
]