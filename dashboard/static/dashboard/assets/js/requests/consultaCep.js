$(document).ready(function() {
  function limpa_formulário_cep() {
    $("#id_endereco").val("");
    $("#id_bairro").val("");
    $("#id_cidade").val("");
    $("#id_uf").val("");
  }

  $("#id_cep").blur(function() {
    var cep = $(this)
      .val()
      .replace(/\D/g, "");
    if (cep != "") {
      var validacep = /^[0-9]{8}$/;
      if (validacep.test(cep)) {
        $("#id_endereco").val("...");
        $("#id_bairro").val("...");
        $("#id_cidade").val("...");
        $("#id_uf").val("...");

        $.getJSON(
          "https://viacep.com.br/ws/" + cep + "/json/?callback=?",
          function(dados) {
            if (!("erro" in dados)) {
              $("#id_endereco").val(dados.logradouro);
              $("#id_bairro").val(dados.bairro);
              $("#id_cidade").val(dados.localidade);
              $("#id_uf").val(dados.uf);
            } else {
              limpa_formulário_cep();
              alert("CEP não encontrado.");
            }
          }
        );
      } else {
        limpa_formulário_cep();
        alert("Formato de CEP inválido.");
      }
    } else {
      limpa_formulário_cep();
    }
  });
});
