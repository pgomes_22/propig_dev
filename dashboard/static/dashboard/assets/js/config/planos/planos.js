$(document).ready(function() {

    $("#btn_planoa").click(function(e) {
        e.preventDefault()

        $("#planoa").addClass("popular")
        $("#planob").removeClass("popular")
        $("#id_plano").val("A")
   });


   $("#btn_planob").click(function(e) {
        e.preventDefault()
        $("#planob").addClass("popular")
        $("#planoa").removeClass("popular")
        $("#id_plano").val("B")
   });

   $(".selecionar_plano").click(function(e) {
        e.preventDefault()
        var selecionar_planos = $('.selecionar_plano')

        var id = $(this).data('plano')
        for (var key in selecionar_planos) {
            if (!selecionar_planos.hasOwnProperty(key)) continue;
            var obj = selecionar_planos[key];
            console.log($(obj).data('plano'))
            if ($(obj).data('plano') != id) {
                $(obj).text('Selecione')
                $(obj).removeClass('btn-success')
                $(obj).addClass('btn-primary')
            }
        }
        $('#id_plano').val(id)
        var cessao = $(this).data('cessao')
        if (cessao == 'CESSAO'){
            id_tr = '#'+id+"_favorecido"
            $('.distribuidor').fadeIn("slow")
            $('.venda').fadeOut( "slow" );
            $('#id_maquina_cessao').val('cessao')
        }
        else if (cessao == 'ALUGUEL') {
            $('.venda').fadeOut( "slow" );
            $('.distribuidor').fadeOut("slow")
            $('#id_maquina_cessao').val('aluguel')
        }
        else {
            $('#id_maquina_cessao').val('venda')
            $('.distribuidor').fadeOut("slow")
            $('.venda').fadeIn("slow")
        }
        $(this).text('Selecionado')
        $(this).removeClass('btn-primary')
        $(this).addClass('btn-success')
        $(this).attr('disabled', true)
        $("#id_plano").val(id)
   })

});
