$(document).ready(function() {
  $("#visao_geral").css("display", "block");
  $("#certidoes").css("display", "none");
  $("#dados_pessoa").css("display", "none");


  $('#divida_ativa').css("display", "none");
  $('#protestos').css("display", "none");
  $('#emails').css("display", "none");
  $('#enderecos').css("display", "none");
  $('#pessoas_relacionadas').css("display", "none");
  $("#telefones").css("display", "none");
  $("#lista_restricoes").css("display", "none");
  $("#sintegra").css("display", "none");
  $("#cessao").css("display", "none");

  $("#visao_geral_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "block");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").addClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#certidoes_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "block");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").addClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#pessoa_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "block");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").addClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#divida_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "block");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').addClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#protesto_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "block");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').addClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#email_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "block");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').addClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#endereco_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "block");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");



    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').addClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#relacoes_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "block");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').addClass("active");
    $("#telefones").removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#telefones_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "block");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");



    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones_link").addClass("active");
    $("#restricoes_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#restricoes_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "block");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#restricoes_link").addClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#sintegra_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "block");
    $("#cessao").css("display", "none");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#restricoes_link").removeClass("active");
    $("#sintegra_link").addClass("active");
    $("#cessao_link").removeClass("active");
  })

  $("#cessao_link").click(function(e) {
    e.preventDefault()
    $("#visao_geral").css("display", "none");
    $("#certidoes").css("display", "none");
    $("#dados_pessoa").css("display", "none");
    $('#divida_ativa').css("display", "none");
    $('#protestos').css("display", "none");
    $('#emails').css("display", "none");
    $('#enderecos').css("display", "none");
    $('#pessoas_relacionadas').css("display", "none");
    $("#telefones").css("display", "none");
    $("#lista_restricoes").css("display", "none");
    $("#sintegra").css("display", "none");
    $("#cessao").css("display", "block");


    $("#visao_geral_link").removeClass("active");
    $("#certidoes_link").removeClass("active");
    $("#pessoa_link").removeClass("active");
    $('#divida_link').removeClass("active");
    $('#protesto_link').removeClass("active");
    $('#email_link').removeClass("active");
    $('#endereco_link').removeClass("active");
    $('#relacoes_link').removeClass("active");
    $("#telefones_link").removeClass("active");
    $("#restricoes_link").removeClass("active");
    $("#sintegra_link").removeClass("active");
    $("#cessao_link").addClass("active");
  })

});
