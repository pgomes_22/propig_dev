$(function() {
    var table = $('#example').DataTable( {
        "scrollY":        "200px",
        "responsive": true,
        "scrollCollapse": true,
        "paging":         false,
            "language": {
        "processing":     "Processando...",
        "search":         "Pesquisar Cliente:",
        "info":           "Mostrando de _START_ a _END_ de _TOTAL_ clientes",
        "infoEmpty":      "Mostrando de  0 a 0 de 0 clientes",
        "infoFiltered":   "(_MAX_ clientes no total)",
        "infoPostFix":    "",
        "loadingRecords": "Carrengando lista de clientes...",
        "zeroRecords":    "Nenhum cliente encontrado",
        "emptyTable":     "Nenhum cliente cadastrado",
        "paginate": {
            "first":      "Primeiro",
            "previous":   "Anterior",
            "next":       "Próximo",
            "last":       "Ultimo"
        }

    }
    });


    var oTable = $('#distribuidora').DataTable({
        "scrollY":        "200px",
        "responsive": true,
        "scrollCollapse": true,
        "paging":         true,
            "language": {
        "processing":     "Processando...",
        "search":         "Pesquisar Transação:",
        "info":           "Mostrando de _START_ a _END_ de _TOTAL_ transações",
        "infoEmpty":      "Mostrando de  0 a 0 de 0 transações",
        "infoFiltered":   "(_MAX_ transações no total)",
        "infoPostFix":    "",
        "loadingRecords": "Carrengando lista de transações...",
        "zeroRecords":    "Nenhuma transação encontrado",
        "emptyTable":     "Nenhuma transação cadastrado",
        "paginate": {
            "first":      "Primeiro",
            "previous":   "Anterior",
            "next":       "Próximo",
            "last":       "Ultimo"
        },
        "iDisplayLength": -1,
        "sPaginationType": "full_numbers",
    }
    });


    $("#datepicker_from").datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
      }).on('changeDate', function(e) {
          minDateFilter = e.date.getTime();
          console.log('minDateFilter', minDateFilter)
          oTable.draw();
    })

    $("#datepicker_to").datepicker({
		format: 'dd/mm/yyyy',
		autoclose: true
      }).on('changeDate', function(e) {
          maxDateFilter = e.date.getTime();
          console.log('maxDateFilter', maxDateFilter)
          oTable.draw();
    })

    $("#limpar_datas").click(function(e) {
        $("#datepicker_to").val('');
        $("#datepicker_from").val('');
        minDateFilter = "";
        maxDateFilter = "";


        oTable.draw();
    });

});


function getMoney( str )
{
        return parseInt( str.replace(/[\D]+/g,'') );
}
function formatReal( int )
{
        var tmp = int+'';
        tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
        if( tmp.length > 6 )
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

        return tmp;
}


minDateFilter = "";
maxDateFilter = "";

$.fn.dataTableExt.afnFiltering.push(
	function( oSettings, aData, iDataIndex ) {
	    novaDataList = aData[4].split('/')
        novaData = novaDataList[2]+'-'+novaDataList[1]+'-'+novaDataList[0]
		if (typeof aData._date == 'undefined') {
            aData._date = new Date(novaData).getTime();
        }

        if (minDateFilter && !isNaN(minDateFilter)) {
          if (aData._date < minDateFilter) {
            return false;
          }
        }

        if (maxDateFilter && !isNaN(maxDateFilter)) {
          if (aData._date > maxDateFilter) {
            return false;
          }
        }

        return true;
    }
);

$.fn.datepicker.dates['en'] = {
    days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabado"],
    daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
    daysMin: ["D", "S", "T", "Q", "Q", "S", "S"],
    months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
    monthsShort: ["Jan", "Fev", "Mar", "Apr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
    today: "Hoje",
    clear: "Limpar",
    format: "dd/mm/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};