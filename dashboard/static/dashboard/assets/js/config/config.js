function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$('#modal-exclusao-prod').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var url = button.data('url')
  console.log(url)
  var modal = $(this)
  modal.find('.btn-certeza-prod').attr('href', url)
//  modal.find('.modal-body input').val(recipient)
})

$('#modal-exclusao-plan').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var url = button.data('url')
  console.log(url)
  var modal = $(this)
  modal.find('.btn-certeza-plan').attr('href', url)
//  modal.find('.modal-body input').val(recipient)
})