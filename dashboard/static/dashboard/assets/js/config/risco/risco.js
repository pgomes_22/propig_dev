$(document).ready(function() {
   $(".aceitavel").click(function() {
        $('.form-obs').css("display", "block")
        $(this).attr("disabled", true)
        $('.alto-risco').removeAttr("disabled")
        $('#id_avaliacao').val("aceitavel")
   })

   $(".alto-risco").click(function() {
        $('.form-obs').css("display", "block")
        $(this).attr("disabled", true)
        $('.aceitavel').removeAttr("disabled")
        $('#id_avaliacao').val("alto")
   })
});
