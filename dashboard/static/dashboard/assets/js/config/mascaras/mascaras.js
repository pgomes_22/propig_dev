$(document).ready(function() {
  $("#id_telefone").mask("(00) 00000-0000");
  $("#id_telefone_titular").mask("(00) 00000-0000");
  $("#id_whatsapp").mask("(00) 00000-0000");
  if ($("#id_tipo").val() === "01") $("#id_cpf").mask("000.000.000-00");
  else $("#id_cpf").mask("00.000.000/0000-00");

  $("#id_tipo").on("change", function() {
    if ($(this).val() == "01") {
        $("#id_cpf").mask("000.000.000-00");
    } else {
       $("#id_cpf").mask("00.000.000/0000-00");
    }
  });

  $('#id_taxa_debito').mask('000.00', {reverse: true});
  $('#id_taxa_credito').mask('000.00', {reverse: true});
  $('#id_taxa_parcelado').mask('000.00', {reverse: true});
  $('#id_valor_adesao_plano').mask('000.00', {reverse: true});
  $('#id_taxa_credito_a_vista').mask('000.00', {reverse: true});
  $('#id_taxa_credito_parcelado_emissor').mask('000.00', {reverse: true});
  $('#id_parcelado_loja_dois_seis').mask('000.00', {reverse: true});
  $('#id_parcelado_loja_sete_doze').mask('000.00', {reverse: true});
  $("#id_data_pagamento").mask("00/00/0000");
  $("#id_data_pagamento_simples").mask("00/00/0000");
  $("#id_nascimento_responsavel").mask("00/00/0000");
  $("#id_cep").mask("00000-000");
  $("#id_ddd_telefone_responsavel").mask("00");
  $("#id_telefone_responsavel").mask("000000000");
  $("#id_documento_responsavel").mask("00000000000");
  $("#id_porcentagem_cliente").mask("000");
  $("#id_porcentagem_favorecido").mask("000");
});
