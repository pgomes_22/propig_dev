from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import DocType, Text, Date, Integer, Boolean

connections.create_connection(hosts=['elasticsearch:9200'])


class LojistaIndex(DocType):
    nome = Text()
    documento = Text()
    branch_id = Text()
    merchant_id = Text()
    affiliation_key = Text()
    mensagem_idwall = Text()
    matriz_idwall = Text()
    numero_idwall = Text()
    resultado_idwall = Text()
    status_idwall = Text()
    pep_idwall = Text()
    fase = Text()
    com_protesto = Text()
    operadora = Text()
    departamento = Integer()
    aprovacao_manual = Boolean()
    email_vendedor_responsavel = Text()
    relatorio_a_partir_pj = Boolean()
    banco_favorecido = Integer()
    agencia_favorecido = Text()
    conta_favorecido = Text()
    digito_conta_favorecido = Text()
    tipo_conta_favorecido = Text()
    porcentagem_favorecido = Text()
    porcentagem_cliente = Text()
    mensagem_integrador = Text()
    num_terminais = Integer()
    observacao_liberacao_risco = Text()
    nome_razao_favorecido = Text()
    cnpj_favorecido = Text()
    data_cadastro = Date()
    data_update = Date()

    class Meta:
        index = 'lojista-index'

from elasticsearch.helpers import bulk
from elasticsearch import Elasticsearch
from . import models

def bulk_indexing():
    LojistaIndex.init()
    es = Elasticsearch()
    bulk(client=es, actions=(b.indexing() for b in models.Lojista.objects.all().iterator()))