import json
from datetime import datetime
from pprint import pprint
from django.urls import resolve
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.db.models import Q
from django.utils.formats import localize
import pandas as pd
from django.conf import settings
import os

from django.contrib.auth.models import User

from .forms import LoginForm, MerchantForm, ConsultaDocForm, RiscForm, SenhaForm, ResetSenhaForm, PlanoForm, \
    ProdutosForm, DistribuidorForm
from .models import PayStore, Retorno4ward, \
    Lojista, IdWall, \
    Cpf, PessoasRelacionadas, \
    EnderecoLojista, EmailsLojista, \
    ResumoConsultas, NomeFonteConsulta, \
    TentativasResumoConsulta, Cnpj, CertidoesNegativasItens, \
    AtividadesSecundaria, GrafiasCnpj, QsaCnpj, SintegraItem, \
    SintegraItemAtividadeSecundaria, Telefones, ParticipacaoEmpresasItens, Pagamento, Pedido, \
    Validacao, ItemValidacoes, DividaAtiva, Protesto, Responsavel, Taxas, DomicilioBancario, Cessao, \
    Transacoes, Slack, \
    Skipper, Terminais, Departamentos, LojistaTerminal, Registration, Planos, Produtos, Distribuidores

from .serializers import Retorno4wardSerializer, \
    LojistaSerializer, TerminaisSerializer, \
    LojistaTerminalSerializer


def login(request):
    global next
    next = request.GET.get('next')
    merchants = Lojista.objects.all().order_by('-id')[:10]
    merchants_analise = Lojista.objects.filter(resultado_idwall='', fase='Em análise na Id Wall',
                                               relatorio_a_partir_pj=False).count()
    merchants_approved = Lojista.objects.filter(resultado_idwall='VALID', fase='Liberado',
                                                relatorio_a_partir_pj=False).count()
    merchants_risc = Lojista.objects.filter(resultado_idwall='INVALID', fase='Risco',
                                            relatorio_a_partir_pj=False).count()
    merchants_denied = Lojista.objects.filter(resultado_idwall='INVALID', fase='Negado',
                                              relatorio_a_partir_pj=False).count()
    pedidos = Pedido.objects.filter(status_pedido="autorizado").count()
    count_terminais = Terminais.objects.all().count()
    lojista_terminal = LojistaTerminal.objects.all().select_related('lojista_id', 'terminal_id')[:10]
    if request.user.is_authenticated:
        if next is not None:
            return HttpResponseRedirect(next)

        if request.user.profile.location != 'comercial':
            if request.user.profile.location == 'operadora':
                return HttpResponseRedirect('/comercial/dados-operadora')
            if 'status' in merchants:
                return render(request, 'dashboard/panel.html',
                              {
                                  'logged_user': request.user,
                                  'merchants': merchants,
                                  'terminals': lojista_terminal,
                                  'error': merchants['message'],
                                  'merchants_analise': merchants_analise,
                                  'merchants_approved': merchants_approved,
                                  'merchants_risc': merchants_risc,
                                  'merchants_denied': merchants_denied,
                                  'pedidos': pedidos,
                                  'count_terminais': count_terminais
                              })
            return render(request, 'dashboard/panel.html',
                          {
                              'logged_user': request.user,
                              'merchants': merchants,
                              'terminals': lojista_terminal,
                              'merchants_analise': merchants_analise,
                              'merchants_approved': merchants_approved,
                              'merchants_risc': merchants_risc,
                              'merchants_denied': merchants_denied,
                              'pedidos': pedidos,
                              'count_terminais': count_terminais
                          })
        else:
            return HttpResponseRedirect('/comercial/')
    else:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():

                next_page = request.POST.get('next_page')
                pprint(next_page)

                username = form.cleaned_data['username']
                senha = form.cleaned_data['senha']
                user = authenticate(request, username=username, password=senha)
                if user is not None:
                    auth_login(request, user)
                    if next_page is not None:
                        return HttpResponseRedirect(next_page)
                    if user.profile.location != 'comercial':
                        if request.user.profile.location == 'operadora':
                            return HttpResponseRedirect('/comercial/dados-operadora')
                        if 'status' in merchants:
                            return render(request, 'dashboard/panel.html',
                                          {
                                              'logged_user': request.user,
                                              'merchants': merchants,
                                              'terminals': lojista_terminal,
                                              'error': merchants['message'],
                                              'merchants_analise': merchants_analise,
                                              'merchants_approved': merchants_approved,
                                              'merchants_risc': merchants_risc,
                                              'merchants_denied': merchants_denied,
                                              'pedidos': pedidos,
                                              'count_terminais': count_terminais
                                          })

                        return render(request, 'dashboard/panel.html',
                                      {
                                          'logged_user': user,
                                          'merchants': merchants,
                                          'terminals': lojista_terminal,
                                          'merchants_analise': merchants_analise,
                                          'merchants_approved': merchants_approved,
                                          'merchants_risc': merchants_risc,
                                          'merchants_denied': merchants_denied,
                                          'pedidos': pedidos,
                                          'count_terminais': count_terminais
                                      })
                    else:
                        return HttpResponseRedirect('/comercial/')
                else:
                    error_message = 'Este usuário não existe, tente novamente!'
                    return render(request, 'dashboard/login.html',
                                  {
                                      'form': form,
                                      'none_user': error_message
                                  })
            else:
                return render(request, 'dashboard/login.html', {'form': form})
        else:
            form = LoginForm(initial={'next_page': request.GET.get('next')})
        return render(request, 'dashboard/login.html', {'form': form})


@login_required(login_url='/dashboard/login/')
def index(request):
    merchants = Lojista.objects.all().order_by('-id')[:10]
    merchants_analise = Lojista.objects.filter(resultado_idwall='', fase='Em análise na Id Wall',
                                               relatorio_a_partir_pj=False).count()
    merchants_approved = Lojista.objects.filter(fase='Liberado', relatorio_a_partir_pj=False).count()
    merchants_risc = Lojista.objects.filter(resultado_idwall='INVALID', fase='Risco',
                                            relatorio_a_partir_pj=False).count()
    merchants_denied = Lojista.objects.filter(resultado_idwall='INVALID', fase='Negado',
                                              relatorio_a_partir_pj=False).count()
    pedidos = Pedido.objects.filter(status_pedido="autorizado").count()
    count_terminais = Terminais.objects.all().count()
    lojista_terminal = LojistaTerminal.objects.all().select_related('lojista_id', 'terminal_id')[:10]
    if 'status' in merchants:
        return render(request, 'dashboard/panel.html', {
            'logged_user': request.user,
            'merchants': merchants,
            'terminals': lojista_terminal,
            'error': merchants['message'],
            'merchants_analise': merchants_analise,
            'merchants_approved': merchants_approved,
            'merchants_risc': merchants_risc,
            'merchants_denied': merchants_denied,
            'pedidos': pedidos,
            'count_terminais': count_terminais
        }
                      )
    return render(request, 'dashboard/panel.html', {
        'logged_user': request.user,
        'merchants': merchants,
        'terminals': lojista_terminal,
        'merchants_analise': merchants_analise,
        'merchants_approved': merchants_approved,
        'merchants_risc': merchants_risc,
        'merchants_denied': merchants_denied,
        'pedidos': pedidos,
        'count_terminais': count_terminais
    }
                  )


@login_required(login_url='/dashboard/login/')
def merchants(request):
    merchants = Lojista.objects.filter(resultado_idwall='', fase='Em análise na Id Wall', relatorio_a_partir_pj=False)
    if 'status' in merchants:
        return render(request, 'dashboard/merchants.html', {
            'logged_user': request.user,
            'merchants': merchants,
            'error': merchants['message']
        }
                      )
    return render(request, 'dashboard/merchants.html', {
        'logged_user': request.user,
        'merchants': merchants
    }
                  )


@login_required(login_url='/dashboard/login/')
def merchants_approved(request):
    merchants = Lojista.objects.filter(resultado_idwall='VALID', relatorio_a_partir_pj=False)
    if 'status' in merchants:
        return render(request, 'dashboard/merchants_approved.html', {
            'logged_user': request.user,
            'merchants': merchants,
            'error': merchants['message']
        }
                      )
    return render(request, 'dashboard/merchants_approved.html', {
        'logged_user': request.user,
        'merchants': merchants
    }
                  )


@login_required(login_url='/dashboard/login/')
def alterar_senha(request):
    if request.method == 'POST':
        form = SenhaForm(request.POST)
        user = User.objects.get(username=request.user.username)
        if user.check_password(request.POST.get('senha_atual')):
            user.set_password(request.POST.get('nova_senha'))
            user.save()
            falha = False
            sucesso = True
            msg_falha = ''
            msg_sucesso = 'Senha atualizada com sucesso!'
        else:
            falha = True
            msg_falha = 'Senha atual não confere'

        return render(request, 'dashboard/nova_senha.html', {
            'logged_user': request.user,
            'form': form,
            'falha': falha,
            'sucesso': sucesso,
            'msg_falha': msg_falha,
            'msg_sucesso': msg_sucesso,
            'operadora': True if request.user.profile.location == 'operadora' else False
        })
    else:
        form = SenhaForm()
        return render(request, 'dashboard/nova_senha.html', {
            'logged_user': request.user,
            'form': form,
            'falha': False,
            'sucesso': False,
            'msg_falha': '',
            'msg_sucesso': '',
            'operadora': True if request.user.profile.location == 'operadora' else False
        })


def reset_senha(request):
    if request.method == 'POST':
        form = ResetSenhaForm(request.POST)
        user = User.objects.get(email=request.POST.get('email'))
        passwd = User.objects.make_random_password()
        user.set_password(passwd)
        user.save()
        PayStore.py_mail(
            'Renovação de senha',
            Registration.mountEmailNovaSenha(passwd, user.username),
            request.POST.get('email'),
            'noreply@grupoe2f.com.br'
        )
        return render(request, 'dashboard/reset_senha.html', {'form': form, 'sucesso': True})

    else:
        form = ResetSenhaForm()
        return render(request, 'dashboard/reset_senha.html', {'form': form})


def skipper_integration(cliente_id):
    global retorno
    lojista = Lojista.objects.get(id=cliente_id)
    if lojista.matriz_idwall == 'lucree_QSA_pj':
        retorno = create_skipper_skeleton(lojista, 'pj')
    else:
        retorno = create_skipper_skeleton(lojista, 'pf')

    if 'fail' in retorno:
        return JsonResponse(retorno)
    else:
        skipper_retorno = Skipper.credenciamento(retorno)
        lojista.mensagem_integrador = 'Credenciamento enviado'
        pprint(skipper_retorno)
        if skipper_retorno['retornos'][0]['status'] == 3:
            lojista.mensagem_integrador = 'Credenciamento enviado'
            lojista.save()
        else:
            lojista.mensagem_integrador = 'Falha ao enviar o credenciamento (4ward)'
            lojista.save()

        return JsonResponse(skipper_retorno)


def skipper_integration_get(request, cliente_id):
    global retorno
    lojista = Lojista.objects.get(id=cliente_id)
    if lojista.matriz_idwall == 'lucree_QSA_pj':
        retorno = create_skipper_skeleton(lojista, 'pj')
    else:
        retorno = create_skipper_skeleton(lojista, 'pf')

    if 'fail' in retorno:
        pprint(retorno)
        clientes = Lojista.objects.filter(
            email_vendedor_responsavel=request.user.email)  # request.user.email)
        return render(request, 'comercial/meus_clientes.html', {
            'lojistas': clientes,
            'logged_user': request.user,
            'fail': True
        })
    else:
        skipper_retorno = Skipper.credenciamento(retorno)
        pprint('sucesso')
        pprint(skipper_retorno)
        lojista.mensagem_integrador = 'Credenciamento enviado'
        pprint(skipper_retorno)
        if skipper_retorno['retornos'][0]['status'] == 3:
            lojista.mensagem_integrador = 'Credenciamento enviado'
            lojista.save()
        else:
            lojista.mensagem_integrador = 'Falha ao enviar o credenciamento (4ward)'
            lojista.save()

        clientes = Lojista.objects.filter(
            email_vendedor_responsavel=request.user.email)  # request.user.email)
        return render(request, 'comercial/meus_clientes.html', {
            'lojistas': clientes,
            'logged_user': request.user,
            'fail': False,
            'enviado': True
        })


def create_skipper_skeleton(lojista, type):
    global id, razao_social, nome_fantasia, \
        contatos, data_fundacao, taxas, banco, endereco, \
        rede_comercial, codigo_adquirente, departamentos, \
        payment_type, company_formation_code, id_canal, url_retorno, credenciamentos

    if type == 'pj':
        cnpj = Cnpj.objects.get(lojista=lojista)
        id = {
            'type': 1,
            'number': cnpj.cnpj
        }
        razao_social = cnpj.nome_empresarial
        nome_fantasia = cnpj.nome_empresarial if cnpj.nome_fantasia == "" else cnpj.nome_fantasia



        try:
            responsavel = Responsavel.objects.get(lojista=lojista)
            contatos = [
                {
                    "name": responsavel.nome,
                    "documentNumber": responsavel.documento,
                    "responsibleType": responsavel.tipo,
                    "birthday": responsavel.data_nascimento,
                    "gender": responsavel.sexo,
                    "phones": [
                        {
                            "type": 1,
                            "country": 55,
                            "area": responsavel.ddd,
                            "number": responsavel.telefone
                        },
                        {
                            "type": 2,
                            "country": 55,
                            "area": responsavel.ddd,
                            "number": responsavel.telefone
                        }
                    ],
                    "email": responsavel.email,
                    "nacionalidade": responsavel.nacionalidade
                }
            ]
        except Responsavel.DoesNotExist:
            lojista.mensagem_integrador = 'Contato responsável pelo estabelecimento não foi cadastrado'
            lojista.save()
            return {'fail': True, 'problema encontrado': lojista.mensagem_integrador}

        data_fundacao = dateToString(cnpj.data_abertura)

        try:
            taxas_lojista = Taxas.objects.get(lojista=lojista)
            taxas = [
                {
                    "productTrans": 1,
                    "rateValue": taxas_lojista.debito
                },
                {
                    "productTrans": 2,
                    "rateValue": taxas_lojista.credito_a_vista
                },
                {
                    "productTrans": 3,
                    "rateValue": taxas_lojista.credito_parcelado_emissor
                },
                {
                    "productTrans": 4,
                    "rateValue": taxas_lojista.parcelado_loja_dois_seis
                }

            ]
        except Taxas.DoesNotExist:
            lojista.mensagem_integrador = 'Taxas não encontradas'
            lojista.save()
            return {'fail': True, 'problema encontrado': lojista.mensagem_integrador}

        if lojista.banco_favorecido != 0:

            # try:
            #     cessao = Cessao.objects.get(lojista=lojista)
            # except Cessao.DoesNotExist:
            #     cessao = None

            banco = [
                {
                    "bankCode": int(lojista.banco_favorecido),
                    "branchNumber": int(lojista.agencia_favorecido),
                    "branchDig": "",
                    "accountNumber": int(lojista.conta_favorecido),
                    "accountDig": int(lojista.digito_conta_favorecido),
                    "accountType": lojista.tipo_conta_favorecido,
                    "favoredType": "Outro-PJ",
                    "favoredCnpjCpf": lojista.cnpj_favorecido,
                    "favoredName": lojista.nome_razao_favorecido
                }
            ]

        else:
            domicilio_bancario = DomicilioBancario.objects.get(lojista=lojista)
            banco = [
                {
                    "bankCode": int(domicilio_bancario.banco),
                    "branchNumber": int(domicilio_bancario.agencia),
                    "branchDig": "",
                    "accountNumber": int(domicilio_bancario.conta),
                    "accountDig": int(domicilio_bancario.digito_conta),
                    "accountType": domicilio_bancario.tipo_conta,
                    "favoredType": "Cliente",
                    "favoredCnpjCpf": "",
                    "favoredName": ""
                }

            ]

        try:
            enderec = EnderecoLojista.objects.get(lojista=lojista, tipo="1")
            endereco = [
                {
                    "type": 1,
                    "street": enderec.logradouro,
                    "number": enderec.numero,
                    "complement": enderec.complemento,
                    "neighborhood": enderec.bairro,
                    "city": enderec.cidade,
                    "state": enderec.estado,
                    "country": "Brasil",
                    "zip": enderec.cep
                },

            ]
        except EnderecoLojista.DoesNotExist:
            lojista.mensagem_integrador = 'Endereço não encontrado'
            lojista.save()
            return {'fail': True, 'problema encontrado': lojista.mensagem_integrador}

        rede_comercial = {
            "matriz": True,
            "tipoEstabelecimento": "EC Matriz",
            "mid": 0
        }

        plano = Planos.objects.get(id=lojista.plano_id)
        if plano.adquirencia == 'ADIQ':
            codigo_adquirente = 8
        elif plano.adquirencia == 'REDE':
            codigo_adquirente = 9

        departamentos = [
            {
                "codigo": lojista.departamento,
                "terminais": [
                    {
                        "posTerminal": {
                            "tecnologyCode": 3,
                            "operator": lojista.operadora
                        },
                        "qt": lojista.num_terminais,
                        "rentValue": plano.valor_contratacao if plano.tipo_contratacao == 'ALUGUEL' else 0,
                        "installValue": plano.valor_contratacao if plano.tipo_contratacao == 'VENDA' else 0,
                    }
                ]
            }
        ]

        payment_type = 1
        if cnpj.natureza_juridica == "2305 - EMPRESA INDIVIDUAL DE RESP.LIMITADA (DE NATUREZA EMPRESARIA)" or cnpj.natureza_juridica == '230-5 - Empresa Individual de Responsabilidade Limitada (de Natureza Empresári':
            company_formation_code = "06"
        if cnpj.natureza_juridica == "2062 - SOCIEDADE EMPRESARIA LIMITADA" or cnpj.natureza_juridica == '206-2 - Sociedade Empres�ria Limitada' or cnpj.natureza_juridica == '206-2 - Sociedade Empresária Limitada':
            company_formation_code = "02"
        if cnpj.natureza_juridica == "2135 - EMPRESARIO (INDIVIDUAL)" or cnpj.natureza_juridica == '213-5 - Empres�rio (Individual)' or cnpj.natureza_juridica == '213-5 - Empresário (Individual)':
            company_formation_code = "03"
        pprint(cnpj.natureza_juridica)
        credenciamentos = [
            {
                "id": id,
                "razaoSocial": razao_social,
                "nomeFantasia": nome_fantasia,
                "contatos": contatos,
                "dataFundacao": data_fundacao,
                "taxas": taxas,
                "domBanks": banco,
                "enderecos": endereco,
                "redeComercial": rede_comercial,
                "codigoAdquirente": codigo_adquirente,
                "departamentos": departamentos,
                "paymentType": payment_type,
                "companyFormationCode": company_formation_code
            }
        ]
        id_canal = 0

        url_retorno = "http://ec2-34-222-98-38.us-west-2.compute.amazonaws.com/dashboard/retorno4ward/"

        dict_adiq = {
            "credenciamentos": credenciamentos,
            "idCanal": id_canal,
            "urlRetorno": url_retorno
        }

        pprint(dict_adiq)

        return dict_adiq
    else:
        try:
            cpf = Cpf.objects.get(lojista=lojista)
        except Cpf.DoesNotExists:
            lojista.mensagem_integrador = 'Cpf não foi cadastrado'
            lojista.save()
            return {'fail': True, 'problema encontrado': lojista.mensagem_integrador}

        id = {
            'type': 2,
            'number': lojista.documento
        }

        nome_fantasia = lojista.nome
        try:
            responsavel = Responsavel.objects.get(lojista=lojista)
            contatos = [
                {
                    "name": responsavel.nome,
                    "documentNumber": responsavel.documento,
                    "responsibleType": responsavel.tipo,
                    "birthday": responsavel.data_nascimento,
                    "gender": responsavel.sexo,
                    "phones": [
                        {
                            "type": 1,
                            "country": 55,
                            "area": responsavel.ddd,
                            "number": responsavel.telefone
                        },
                        {
                            "type": 2,
                            "country": 55,
                            "area": responsavel.ddd,
                            "number": responsavel.telefone
                        }
                    ],
                    "email": responsavel.email,
                    "nacionalidade": responsavel.nacionalidade
                }
            ]
        except Responsavel.DoesNotExist:
            lojista.mensagem_integrador = 'Contato responsável pelo estabelecimento não foi cadastrado'
            lojista.save()
            return {'fail': True, 'problema encontrado': lojista.mensagem_integrador}

        data_fundacao = dateToString(cpf.data_de_nascimento)

        taxas_lojista = Taxas.objects.get(lojista=lojista)
        taxas = [
            {
                "productTrans": 1,
                "rateValue": taxas_lojista.debito
            },
            {
                "productTrans": 2,
                "rateValue": taxas_lojista.credito_a_vista
            },
            {
                "productTrans": 3,
                "rateValue": taxas_lojista.credito_parcelado_emissor
            },
            {
                "productTrans": 4,
                "rateValue": taxas_lojista.parcelado_loja_dois_seis
            }

        ]

        domicilio_bancario = DomicilioBancario.objects.get(lojista=lojista)
        try:
            cessao = Cessao.objects.get(lojista=lojista)
        except Cessao.DoesNotExist:
            cessao = None
        banco = [
            {
                "bankCode": int(domicilio_bancario.banco),
                "branchNumber": int(domicilio_bancario.agencia),
                "branchDig": "",
                "accountNumber": int(domicilio_bancario.conta),
                "accountDig": int(domicilio_bancario.digito_conta),
                "accountType": domicilio_bancario.tipo_conta,
                "favoredType": "Outro-PJ" if cessao else "Cliente",
                "favoredCnpjCpf": "09104373430" if cessao else "",
                "favoredName": "MOURA" if cessao else ""
            }
        ]
        enderec = EnderecoLojista.objects.get(lojista=lojista, tipo="1")
        endereco = [
            {
                "type": 1,
                "street": enderec.logradouro,
                "number": enderec.numero,
                "complement": enderec.complemento,
                "neighborhood": enderec.bairro,
                "city": enderec.cidade,
                "state": enderec.estado,
                "country": "Brasil",
                "zip": enderec.cep
            },
        ]

        rede_comercial = {
            "matriz": True,
            "tipoEstabelecimento": "EC Matriz",
            "mid": 0
        }

        plano = Planos.objects.get(id=lojista.plano_id)
        if plano.adquirencia == 'ADIQ':
            codigo_adquirente = 8
        elif plano.adquirencia == 'REDE':
            codigo_adquirente = 9

        departamentos = [
            {
                "codigo": lojista.departamento,
                "terminais": [
                    {
                        "posTerminal": {
                            "tecnologyCode": 3,
                            "operator": lojista.operadora,
                        },
                        "qt": lojista.num_terminais,
                        "rentValue": plano.valor_contratacao if plano.tipo_contratacao == 'ALUGUEL' else 0,
                        "installValue": plano.valor_contratacao if plano.tipo_contratacao == 'VENDA' else 0
                    }
                ]
            }
        ]

        payment_type = 1
        company_formation_code = "00"

        credenciamentos = [
            {
                "id": id,
                "nomeFantasia": nome_fantasia,
                "contatos": contatos,
                "dataFundacao": data_fundacao,
                "taxas": taxas,
                "domBanks": banco,
                "enderecos": endereco,
                "redeComercial": rede_comercial,
                "ativPF": "5620102",
                "codigoAdquirente": codigo_adquirente,
                "departamentos": departamentos,
                "paymentType": payment_type,
                "companyFormationCode": company_formation_code
            }
        ]
        id_canal = 0

        url_retorno = "https://webhook.site/e1ac82ee-ca89-4558-a8fb-4117b7ab6203"

        dict_adiq = {
            "credenciamentos": credenciamentos,
            "idCanal": id_canal,
            "urlRetorno": url_retorno
        }

        pprint(dict_adiq)

        return dict_adiq


def dateToString(data):
    strData = str(data);
    arrayData = strData.split("-")
    finalStr = arrayData[2] + "/" + arrayData[1] + "/" + arrayData[0]
    return finalStr


def formatCpfToSend(cpf):
    first_replace = cpf.replace(".", '')
    second_replace = first_replace.replace("-", '')
    return second_replace


@login_required(login_url='/dashboard/login/')
def merchants_risc(request):
    merchants = Lojista.objects.filter(resultado_idwall='INVALID', fase='Risco', relatorio_a_partir_pj=False)
    if 'status' in merchants:
        return render(request, 'dashboard/merchants_risc.html', {
            'logged_user': request.user,
            'merchants': merchants,
            'error': merchants['message']
        }
                      )
    return render(request, 'dashboard/merchants_risc.html', {
        'logged_user': request.user,
        'merchants': merchants
    }
                  )


@login_required(login_url='/dashboard/login/')
def risc_details(request, cliente_id):
    try:
        form = RiscForm()
        lojista = Lojista.objects.get(id=cliente_id)

        try:
            validacao = Validacao.objects.get(lojista=lojista)

            itens_validacoes = ItemValidacoes.objects.filter(validacao=validacao).values('regra', 'nome', 'descricao',
                                                                                         'resultado', 'mensagem',
                                                                                         'validacao_id').distinct()
        except:
            validacao = None
            itens_validacoes = None

        # print(itens_validacoes.query)
        certidoes = CertidoesNegativasItens.objects.filter(lojista=lojista)

        divida_ativa = DividaAtiva.objects.filter(lojista=lojista)
        protestos = Protesto.objects.filter(lojista=lojista)
        emails = EmailsLojista.objects.filter(lojista=lojista)
        enderecos = EnderecoLojista.objects.filter(lojista=lojista)
        pessoas_relacionadas = PessoasRelacionadas.objects.filter(lojista=lojista)
        telefones = Telefones.objects.filter(lojista=lojista)

        try:
            cessao = Cessao.objects.get(lojista=lojista)
        except Cessao.DoesNotExist:
            cessao = None

        try:
            sintegra = SintegraItem.objects.get(lojista=lojista)
            sintegra_atividades_secundarias = SintegraItemAtividadeSecundaria.objects.filter(sintegra_item=sintegra)
        except:
            sintegra = None
            sintegra_atividades_secundarias = None

        if lojista.matriz_idwall == 'lucree_QSA_pj':

            cnpj = Cnpj.objects.get(lojista=lojista)

            grafias = GrafiasCnpj.objects.filter(cnpj=cnpj)
            socios = PessoasRelacionadas.objects.filter(Q(cargo='SOCIO') | Q(cargo='SOCIO-ADMINISTRADOR'),
                                                        lojista=lojista)

            atividades_secundarias = AtividadesSecundaria.objects.filter(cnpj=cnpj)

            if cnpj.capital_social is not None:
                capital_social = localize(int(cnpj.capital_social))
            else:
                capital_social = None

            itens_validacoes_sintegra = ItemValidacoes.objects.filter(validacao=validacao,
                                                                      regra='idwall.matrix.regras.sintegras.RegraSintegras').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_cnpj = ItemValidacoes.objects.filter(validacao=validacao,
                                                                           regra='idwall.matrix.regras.cnpj.RegraSituacaoCnpj').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_big_boost = ItemValidacoes.objects.filter(validacao=validacao,
                                                                                regra='idwall.matrix.regras.RegraBigBoost').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_trtsp = ItemValidacoes.objects.filter(validacao=validacao,
                                                                            regra='idwall.matrix.regras.certidoes.RegraTRTSP').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_regrabasica = ItemValidacoes.objects.filter(validacao=validacao,
                                                                                  regra='idwall.matrix.regras.RegraBasica').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_protestos = ItemValidacoes.objects.filter(validacao=validacao,
                                                                                regra='idwall.matrix.regras.protestos.RegraProtestos').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            itens_validacoes_situacao_divida = ItemValidacoes.objects.filter(validacao=validacao,
                                                                             regra='idwall.matrix.regras.blacklists.RegraDividaAtiva').values(
                'regra', 'nome', 'descricao', 'resultado', 'mensagem', 'validacao_id').distinct()

            try:
                dict_regras = {
                    'sintegra': itens_validacoes_sintegra[0]['resultado'],
                    'cnpj': itens_validacoes_situacao_cnpj[0]['resultado'],
                    'big_boost': itens_validacoes_situacao_big_boost[0]['resultado'],
                    'trtsp': itens_validacoes_situacao_trtsp[0]['resultado'],
                    'regrabasica': itens_validacoes_situacao_regrabasica[0]['resultado'],
                    'protestos': itens_validacoes_situacao_protestos[0]['resultado'],
                    'divida': itens_validacoes_situacao_divida[0]['resultado'],
                }
            except:
                dict_regras = None;

            return render(request, 'dashboard/risc_details.html', {
                'lojista': lojista,
                'certidoes': certidoes,
                'validacao': validacao,
                'itens_validacoes': itens_validacoes,
                'cnpj': cnpj if cnpj else None,
                'grafias': grafias if grafias else None,
                'socios': socios if socios else None,
                'atividades_secundarias': atividades_secundarias if atividades_secundarias else None,
                'capital_social': capital_social if capital_social else None,
                'divida_ativa': divida_ativa if divida_ativa else None,
                'emails': emails if emails else None,
                'enderecos': enderecos if enderecos else None,
                'pessoas_relacionadas': pessoas_relacionadas if pessoas_relacionadas else None,
                'telefones': telefones if telefones else None,
                'protestos': protestos if protestos else None,
                'dict_regras': dict_regras if dict_regras else None,
                'sintegra': sintegra if sintegra else None,
                'cessao': cessao.foto_documento if cessao else None,
                'sintegra_atividades_secundarias': sintegra_atividades_secundarias if sintegra_atividades_secundarias else None,
                'logged_user': request.user,
                'form': form
            })

        else:
            cpf = Cpf.objects.get(lojista=lojista)

            pprint(ItemValidacoes.objects.get(validacao=validacao,
                                              regra='idwall.matrix.regras.cpf.RegraSituacaoReceitaFederal').__dict__)
            itens_validacoes_situacao_cpf = ItemValidacoes.objects.get(validacao=validacao,
                                                                       regra='idwall.matrix.regras.cpf.RegraSituacaoReceitaFederal')

            pprint('oi1')

            itens_validacoes_situacao_big_boost = ItemValidacoes.objects.get(validacao=validacao,
                                                                             regra='idwall.matrix.regras.RegraBigBoost')
            pprint('oi2')
            itens_validacoes_situacao_protestos = ItemValidacoes.objects.get(validacao=validacao,
                                                                             regra='idwall.matrix.regras.protestos.RegraProtestos')
            pprint('oi3')
            itens_validacoes_situacao_divida = ItemValidacoes.objects.get(validacao=validacao,
                                                                          regra='idwall.matrix.regras.blacklists.RegraDividaAtiva')
            pprint('oi4')
            dict_regras = {
                'cpf': itens_validacoes_situacao_cpf.resultado,
                'big_boost': itens_validacoes_situacao_big_boost.resultado,
                'protestos': itens_validacoes_situacao_protestos.resultado,
                'divida': itens_validacoes_situacao_divida.resultado,
            }
            return render(request, 'dashboard/risc_details.html', {
                'lojista': lojista,
                'certidoes': certidoes,
                'validacao': validacao,
                'itens_validacoes': itens_validacoes,
                'cnpj': None,
                'cpf': cpf if cpf else None,
                'grafias': None,
                'socios': None,
                'atividades_secundarias': None,
                'capital_social': None,
                'divida_ativa': divida_ativa if divida_ativa else None,
                'emails': emails if emails else None,
                'enderecos': enderecos if enderecos else None,
                'pessoas_relacionadas': pessoas_relacionadas if pessoas_relacionadas else None,
                'telefones': telefones if telefones else None,
                'protestos': protestos if protestos else None,
                'dict_regras': dict_regras if dict_regras else None,
                'cessao': cessao.foto_documento if cessao else None,
                'sintegra': None,
                'sintegra_atividades_secundarias': None,
                'logged_user': request.user,
                'form': form
            })
    except Exception as e:
        return render(request, 'dashboard/risc_view_fail.html', {
            'error': str(e)
        })


@login_required(login_url='/dashboard/login/')
def change_client(request, cliente_id):
    if request.method == 'POST':
        lojista = Lojista.objects.get(id=cliente_id)

        if request.POST.get('avaliacao') == 'aceitavel':
            lojista.fase = 'Liberado'
            lojista.resultado_idwall = 'VALID'
            lojista.aprovacao_manual = True
            lojista.observacao_liberacao_risco = request.POST.get('observacao')
            lojista.save()
            skipper_integration_get(request, cliente_id)
            return HttpResponseRedirect('/dashboard/merchants-approved/')
        elif request.POST.get('avaliacao') == 'alto':
            lojista.fase = 'Negado'
            lojista.resultado_idwall = 'INVALID'
            lojista.observacao_liberacao_risco = request.POST.get('observacao')
            lojista.save()
            responsavel = Responsavel.objects.get(lojista=lojista)
            PayStore.py_mail("Cliente com Alto Risco",
                             PayStore.mountEmailRiscoVendedor(lojista.documento, lojista.nome),
                             lojista.email_vendedor_responsavel, 'mail@bevipag.com.br')
            PayStore.py_mail("Cadastro com Alto Risco",
                             PayStore.mountEmailRiscoCliente(lojista.documento, lojista.nome),
                             responsavel.email, 'mail@beivpag.com.br')
            return HttpResponseRedirect('/dashboard/merchants-denied/')
    else:
        HttpResponseRedirect('dashboard/risc-details/' + str(cliente_id) + '/')


@login_required(login_url='/dashboard/login/')
def merchants_denied(request):
    merchants = Lojista.objects.filter(resultado_idwall='INVALID', fase='Negado', relatorio_a_partir_pj=False)
    if 'status' in merchants:
        return render(request, 'dashboard/merchants_denied.html', {
            'logged_user': request.user,
            'merchants': merchants,
            'error': merchants['message']
        }
                      )
    return render(request, 'dashboard/merchants_denied.html', {
        'logged_user': request.user,
        'merchants': merchants
    }
                  )


@login_required(login_url='/dashboard/login/')
def register_merchants(request):
    form = MerchantForm()
    return render(request, 'dashboard/merchant_register.html', {
        'logged_user': request.user,
        'form': form
    })


@login_required(login_url='/dashboard/login')
def terminals(request):
    terminals = PayStore.allTerminals()
    lojista_terminal = LojistaTerminal.objects.all().select_related('lojista_id', 'terminal_id')

    return render(request, 'dashboard/terminals.html', {
        'logged_user': request.user,
        'terminals': lojista_terminal
    }
                  )


@api_view(['POST'])
def service_accreditation(request):
    lojistas = Lojista.objects.filter(merchant_id="", status_idwall="CONCLUIDO", resultado_idwall="VALID",
                                      mensagem_integrador="Credenciamento enviado")

    lista = []
    lista_merchants = []
    for lojista in lojistas:
        tipo = 0
        newDict = {}
        pprint(lojista.nome)
        if lojista.matriz_idwall == 'lucree_QSA_pj':
            tipo = 1
        else:
            tipo = 2

        newDict = {
            "id": {
                "type": tipo,
                "number": lojista.documento
            },
            "codigoAdquirente": 8
        }

        lista.append(newDict)

    retorno = Skipper.consulta_credenciamento(lista)

    for r in retorno:
        if r['status'] == 1:
            merchant = {}
            lojista = Lojista.objects.get(documento=r['id']['number'])
            lojista.merchant_id = r['merchantBranchId']['terminals'][0]['terminalIDLogical']
            for terminal in r['merchantBranchId']['terminals']:
                if terminal['terminalIDPhysical'] != "":
                    existe_t = Terminais.objects.filter(terminal_id_physical=terminal['terminalIDPhysical']).count()
                    if existe_t == 0:
                        t = Terminais()
                        t.terminal_serial_number = terminal['terminalSerialNumber']
                        t.terminal_id_physical = terminal['terminalIDPhysical']
                        t.terminal_id_logical = terminal['terminalIDLogical']
                        t.save()
                        lojista_terminal = LojistaTerminal()
                        lojista_terminal.lojista_id = lojista
                        lojista_terminal.terminal_id = t
                        lojista_terminal.save()
                    else:
                        pprint("EXISTE")

            lojista.merchant_id = r['merchantBranchId']['terminals'][0]['terminalIDLogical']
            pprint(lojista.merchant_id)
            lojista.save()
            merchant = new_merchant(lojista)

    return JsonResponse({"consulta": retorno})


def new_merchant(lojista):
    responsavel = Responsavel.objects.get(lojista=lojista)
    enderec = EnderecoLojista.objects.get(lojista=lojista, tipo="1")
    departamento = Departamentos.objects.get(codigo_departamento=lojista.departamento)

    if lojista.matriz_idwall == "lucree_QSA_pj":
        cnpj = Cnpj.objects.get(lojista=lojista)
        if cnpj.nome_fantasia == "":
            nome_empresa = cnpj.nome_empresarial[0:35]
        else:
            nome_empresa = cnpj.nome_fantasia[0:35]
    else:
        nome_empresa = lojista.nome[0:35]

    newDict = {
        "owner": {
            "name": responsavel.nome,
            "email": responsavel.email,
            "phone": responsavel.ddd + responsavel.telefone,
            "messenger_phone": responsavel.ddd + responsavel.telefone
        },
        "national_type": "01" if lojista.matriz_idwall == "lucree_QSA_pf" else "02",
        "national_id": lojista.documento,
        "company_name": nome_empresa,
        "commercial_name": nome_empresa,
        "merchant_category_code": departamento.id_mcc,
        "postal_code": str(enderec.cep).replace("-", ""),
        "address_number": enderec.numero,
        "enterprise_email": responsavel.email,
        "enterprise_phone": responsavel.ddd + responsavel.telefone,
        "enterprise_messenger_phone": responsavel.ddd + responsavel.telefone,
    }

    register = PayStore.saveMerchant(newDict)
    pprint(register)

    if lojista.merchant_id != "":
        newDictTefConfig = {
            "tef_acquirer": 3,
            "merchant": register['merchant'],
            "merchant_acquirer_id": lojista.merchant_id,
            "enabled": True,
            "priority": 1
        }
        PayStore.tefConfigMerchant(newDictTefConfig)

    if 'errors' not in register:
        count = 0
        string_ativacao = ""
        lojista_terminal = LojistaTerminal.objects.filter(lojista_id=lojista)
        for lt in lojista_terminal:
            count = count + 1
            name_of_terminal = 'M' + register['merchant'] + str(count)
            newDict = {
                'communication_profile': 'VX685_VX690_LYRA_GPRS',
                'merchant': register['merchant'],
                'terminal': name_of_terminal,
            }
            returnterminal = PayStore.saveTerminal(newDict)
            pprint('TOKEN')
            pprint(returnterminal)
            string_ativacao = string_ativacao + ' [' + name_of_terminal + ': ' + returnterminal['token'] + ']'
            terminal = Terminais.objects.get(id=lt.terminal_id_id)
            newDictTerminal = {
                "tef_acquirer": 3,
                "merchant": register['merchant'],
                "terminal_acquirer_id": terminal.terminal_id_physical,
                "terminal": name_of_terminal
            }
            pprint(newDictTerminal)
            PayStore.tefConfigTerminal(newDictTerminal)

            # PayStore.py_mail(
            #     'ATIVE O(S) SEU(S) TERMINAL(AIS)',
            #     PayStore.mountEmailAtivacao(
            #         string_ativacao
            #     ),
            #     responsavel.email,
            #     'daniel.lima.nascimento@gmail.com'
            # )
        return JsonResponse(register)

    return JsonResponse(newDict)


def teste_email(request):
    PayStore.py_mail("Nova análise de risco",
                     PayStore.mountEmailRisco(),
                     "daniel.nascimento@bevipag.com.br",
                     "mail@bevipag.com.br"
                     )
    return render(request, 'dashboard/mail/alerta_risco_email.html', {
        'documento': '1323123123123',
        'nome_razao_social': 'Nome do cliente',
        'rota': 'rotaaaa'
    })


@api_view(['GET', 'POST'])
def retornoapi(request):
    if request.method == 'GET':
        retorno = Retorno4ward.objects.all()
        serializer = Retorno4wardSerializer(retorno, many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        for value in request.data['retornos']:
            lojista = Lojista.objects.get(documento=value['id']['number'])
            mid = value['merchantBranchId']['merchantID']
            branch = value['merchantBranchId']['branchID']
            lojista.merchant_id = mid
            lojista.branch_id = branch
            lojista.save()
            lojista_id = lojista.id

            for terminais in value['merchantBranchId']['terminais']:
                terminal = Terminais()
                terminal.terminal_id_logical = terminais['terminalIDLogical']
                terminal.terminal_id_physical = terminais['terminalIDPhysical']
                terminal.terminal_serial_number = terminais['terminalSerialNumber']
                terminal.tecnology_code = terminais['tecnologyCode']

                terminal_id = terminal.save()
                lojista_terminal = LojistaTerminal()
                lojista_terminal.lojista_id = lojista_id
                lojista_terminal.terminal_id = terminal_id
                lojista_terminal.save()

                return Response({'mid': lojista.lojista_id, 'tid': terminal.terminal_id_logical},
                                status=status.HTTP_201_CREATED)

        else:
            return Response({'não há retornos'}, status=status.HTTP_400_BAD_REQUEST)


@login_required(login_url='/dashboard/login/')
def consulta_doc(request):
    form = ConsultaDocForm()

    return render(request, 'dashboard/consulta_documentacao.html', {'form': form})


@login_required(login_url='/dashboard/login/')
def consulta_idwall(request):
    if request.method == 'POST':
        form = ConsultaDocForm(request.POST)
        if form.is_valid():
            tipo = form.cleaned_data['tipo_doc']
            numero = form.cleaned_data['numero']
            result_consulta = IdWall.consulta_documento(tipo, numero)
            if result_consulta["result"]["numero"]:
                consulta_resultado = IdWall.consulta_resultado(result_consulta["result"]["numero"])
                if consulta_resultado["result"]:
                    try:
                        pesquisa_lojista = Lojista.objects.get(documento=numero)
                        pesquisa_lojista.mensagem_idwall = consulta_resultado["result"]["mensagem"] \
                            if "message" in consulta_resultado["result"] else ""
                        pesquisa_lojista.matriz_idwall = consulta_resultado["result"]["nome"] \
                            if "nome" in consulta_resultado["result"] else ""
                        pesquisa_lojista.numero_idwall = consulta_resultado["result"]["numero"] \
                            if "numero" in consulta_resultado["result"] else ""
                        pesquisa_lojista.status_idwall = consulta_resultado["result"]["status"] \
                            if "status" in consulta_resultado["result"] else ""
                        pesquisa_lojista.data_update = datetime.now()
                        pesquisa_lojista.save()

                    except Lojista.DoesNotExist:
                        lojista = Lojista()
                        lojista.documento = numero
                        lojista.mensagem_idwall = consulta_resultado["result"]["mensagem"] \
                            if "message" in consulta_resultado["result"] else ""
                        lojista.matriz_idwall = consulta_resultado["result"]["nome"] \
                            if "nome" in consulta_resultado["result"] else ""
                        lojista.numero_idwall = consulta_resultado["result"]["numero"] \
                            if "numero" in consulta_resultado["result"] else ""
                        lojista.status_idwall = consulta_resultado["result"]["status"] \
                            if "status" in consulta_resultado["result"] else ""
                        lojista.save()

                    return HttpResponseRedirect("/dashboard/merchants/")

                else:
                    return JsonResponse({"resultado": "sem resultado"})


@api_view(['POST'])
def retornoidwall(request):
    if request.method == "POST":
        if request.data['tipo'] == "protocolo_status":
            if request.data['dados']['protocolo'] and (request.data['dados']['status'] == "CONCLUIDO" \
                                                       or request.data['dados']['status'] == "EM ANALISE"):

                consulta_resultado = IdWall.consulta_resultado(request.data['dados']['protocolo'])
                pprint(consulta_resultado)
                if "result" in consulta_resultado:
                    if consulta_resultado["result"]["nome"] == "lucree_QSA_pf":
                        inicia_processo_pf(request, consulta_resultado)
                    elif consulta_resultado["result"]["nome"] == "lucree_QSA_pj":
                        inicia_processo_pj(request, consulta_resultado)

                    return JsonResponse(request.data['dados'])
                return JsonResponse({'resultado': 'Protocolo inexistente'})


@api_view(['POST'])
def capturatransacao(request):
    if request.method == "POST":
        exist = Transacoes.objects.filter(nsu=request.data["nsu"]).count()
        if exist < 1:
            transacoes = Transacoes()
            transacoes.nsu = request.data["nsu"] if "nsu" in request.data else ""
            transacoes.acquirer_nsu = request.data["acquirer_nsu"] if "acquirer_nsu" in request.data else ""
            transacoes.value = float(int(request.data["value"]) / 100) if "value" in request.data else ""
            transacoes.status = request.data["status"] if "status" in request.data else ""
            transacoes.parcels = request.data["parcels"] if "parcels" in request.data else ""
            transacoes.brand = request.data["brand"] if "brand" in request.data else ""
            transacoes.start_date = request.data["start_date"] if "start_date" in request.data else ""
            transacoes.finish_date = request.data["finish_date"] if "finish_date" in request.data else ""
            transacoes.confirmation_date = request.data[
                "confirmation_date"] if "confirmation_date" in request.data else ""
            transacoes.payment_date = request.data["payment_date"] if "payment_date" in request.data else ""
            transacoes.response_code = request.data["response_code"] if "response_code" in request.data else ""
            transacoes.response_message = request.data["response_message"] if "response_message" in request.data else ""
            transacoes.authorization_number = request.data[
                "authorization_number"] if "authorization_number" in request.data else ""
            transacoes.terminal = request.data["terminal"] if "terminal" in request.data else ""
            transacoes.tef_terminal = request.data["tef_terminal"] if "tef_terminal" in request.data else ""
            transacoes.terminal_serial_number = request.data[
                "terminal_serial_number"] if "terminal_serial_number" in request.data else ""
            transacoes.terminal_manufacturer = request.data[
                "terminal_manufacturer"] if "terminal_manufacturer" in request.data else ""
            transacoes.terminal_model = request.data["terminal_model"] if "terminal_model" in request.data else ""
            transacoes.terminal_type = request.data["terminal_type"] if "terminal_type" in request.data else ""
            transacoes.acquirer = request.data["acquirer"] if "acquirer" in request.data else ""
            transacoes.merchant = request.data["merchant"] if "merchant" in request.data else ""
            transacoes.tef_merchant = request.data["tef_merchant"] if "tef_merchant" in request.data else ""
            transacoes.merchant_category_code = request.data[
                "merchant_category_code"] if "merchant_category_code" in request.data else ""
            transacoes.merchant_national_type = request.data[
                "merchant_national_type"] if "merchant_national_type" in request.data else ""
            transacoes.merchant_national_id = request.data[
                "merchant_national_id"] if "merchant_national_id" in request.data else ""
            transacoes.product_name = request.data["product_name"] if "product_name" in request.data else ""
            transacoes.product_id = request.data["product_id"] if "product_id" in request.data else ""
            transacoes.card_input_method = request.data[
                "card_input_method"] if "card_input_method" in request.data else ""
            transacoes.requested_password = request.data[
                "requested_password"] if "requested_password" in request.data else ""
            transacoes.fallback = request.data["fallback"] if "fallback" in request.data else ""
            transacoes.origin = request.data["origin"] if "origin" in request.data else ""
            transacoes.authorization_time = request.data[
                "authorization_time"] if "authorization_time" in request.data else ""
            transacoes.client_version = request.data["client_version"] if "client_version" in request.data else ""
            transacoes.server_version = request.data["server_version"] if "server_version" in request.data else ""
            transacoes.card_holder = request.data["card_holder"] if "card_holder" in request.data else ""
            transacoes.save()
            return JsonResponse({"transacoes": "ok"})
        else:
            return JsonResponse({"transacoes": "ja existe"})


def inicia_processo_pj(requisicao, consulta_resultado):
    pesquisa_lojista = Lojista.objects.get(numero_idwall=requisicao.data['dados']['protocolo'])
    relatorio_consulta = IdWall.relatorio_consulta(requisicao.data['dados']['protocolo'])
    validacoes_consulta = IdWall.consulta_validacoes(requisicao.data['dados']['protocolo'])
    # processamento_cnpj(pesquisa_lojista, consulta_resultado)
    cnpj = atualiza_cnpj_lojista(pesquisa_lojista, consulta_resultado)
    atualiza_certidoes_negativas(pesquisa_lojista, consulta_resultado)
    atualiza_atividades_secundarias(consulta_resultado, cnpj)
    atualiza_grafias(consulta_resultado, cnpj)
    atualiza_qsa(consulta_resultado, cnpj)
    # atualiza_pessoas_relacionadas(pesquisa_lojista, consulta_resultado)
    sintegra = atualiza_sintegra(pesquisa_lojista, consulta_resultado)
    atualiza_sintegra_atividades(consulta_resultado, sintegra)
    atualiza_telefones(consulta_resultado, pesquisa_lojista)
    atualiza_participacao_empresas(consulta_resultado, pesquisa_lojista)
    atualiza_enderecos(pesquisa_lojista, consulta_resultado)
    processa_resultado_relatorio(relatorio_consulta, pesquisa_lojista)
    atualiza_divida_ativa(pesquisa_lojista, consulta_resultado)
    atualiza_protestos(pesquisa_lojista, consulta_resultado)
    atualiza_email(pesquisa_lojista, consulta_resultado)
    processamento_cnpj(pesquisa_lojista, consulta_resultado)

    processa_resultado_validacoes(validacoes_consulta, pesquisa_lojista)


def inicia_processo_pf(requisicao, consulta_resultado):
    pesquisa_lojista = Lojista.objects.get(numero_idwall=requisicao.data['dados']['protocolo'])
    # if pesquisa_lojista.mensagem_idwall == '':
    relatorio_consulta = IdWall.relatorio_consulta(requisicao.data['dados']['protocolo'])
    validacoes_consulta = IdWall.consulta_validacoes(requisicao.data['dados']['protocolo'])

    pprint('PASSANDO NO PROCESSAMENTOOOOO')
    atualiza_cpf_lojista(pesquisa_lojista, consulta_resultado)
    # atualiza_pessoas_relacionadas(pesquisa_lojista, consulta_resultado)
    atualiza_enderecos(pesquisa_lojista, consulta_resultado)
    atualiza_email(pesquisa_lojista, consulta_resultado)
    processa_resultado_relatorio(relatorio_consulta, pesquisa_lojista)
    atualiza_divida_ativa(pesquisa_lojista, consulta_resultado)
    atualiza_protestos(pesquisa_lojista, consulta_resultado)
    processamento_cpf(pesquisa_lojista, consulta_resultado)
    processa_resultado_validacoes(validacoes_consulta, pesquisa_lojista)

    return JsonResponse(consulta_resultado["result"])


def inicia_processo_pf_relacoes(consulta_resultado, protocolo):
    pesquisa_lojista = Lojista.objects.get(numero_idwall=protocolo)
    # if pesquisa_lojista.mensagem_idwall == '':
    relatorio_consulta = IdWall.relatorio_consulta(protocolo)
    validacoes_consulta = IdWall.consulta_validacoes(protocolo)
    processamento_cpf(pesquisa_lojista, consulta_resultado)
    atualiza_pessoas_relacionadas(pesquisa_lojista, consulta_resultado)
    atualiza_enderecos(pesquisa_lojista, consulta_resultado)
    atualiza_email(pesquisa_lojista, consulta_resultado)
    processa_resultado_relatorio(relatorio_consulta, pesquisa_lojista)
    atualiza_divida_ativa(pesquisa_lojista, consulta_resultado)
    atualiza_protestos(pesquisa_lojista, consulta_resultado)
    atualiza_cpf_lojista(pesquisa_lojista, consulta_resultado)
    processa_resultado_validacoes(validacoes_consulta, pesquisa_lojista)

    return JsonResponse(consulta_resultado["result"])


def processa_resultado_validacoes(validacoes_consulta, pesquisa_lojista):
    try:
        validacao = Validacao.objects.get(lojista=pesquisa_lojista)
        validacao.mensagem = validacoes_consulta['result']['mensagem']
        validacao.nome = validacoes_consulta['result']['nome']
        validacao.resultado = validacoes_consulta['result']['resultado']
        validacao.status = validacoes_consulta['result']['status']
        validacao.lojista = pesquisa_lojista
        validacao.data_update = datetime.now()
        validacao.save()
        processa_item_validacao(validacao, validacoes_consulta['result']['validacoes'])
    except Validacao.DoesNotExist:
        validacao = Validacao()
        validacao.mensagem = validacoes_consulta['result']['mensagem']
        validacao.nome = validacoes_consulta['result']['nome']
        validacao.resultado = validacoes_consulta['result']['resultado']
        validacao.status = validacoes_consulta['result']['status']
        validacao.lojista = pesquisa_lojista
        validacao.data_update = datetime.now()
        validacao.save()
        processa_item_validacao(validacao, validacoes_consulta['result']['validacoes'])


def processa_item_validacao(validacao, validacoes):
    for item in validacoes:
        vali = ItemValidacoes()
        vali.regra = item["regra"] if "regra" in item else None
        vali.nome = item["nome"] if "nome" in item else None
        vali.descricao = item["descricao"] if "descricao" in item else None
        vali.resultado = item["resultado"] if "resultado" in item else None
        vali.mensagem = item["mensagem"] if "mensagem" in item else None
        vali.validacao = validacao
        vali.data_cadastro = datetime.now()
        vali.data_update = datetime.now()
        vali.save()


def processa_resultado_relatorio(relatorio_consulta, pesquisa_lojista):
    try:
        resumo = ResumoConsultas.objects.get(lojista=pesquisa_lojista)
        resumo.nome_matriz = relatorio_consulta['result']['nome_matriz']
        resumo.status_protocolo = relatorio_consulta['result']['status_protocolo']
        resumo.lojista = pesquisa_lojista
        resumo.data_update = datetime.now()
        resumo.save()
        processa_nome_fonte_consulta(resumo, relatorio_consulta)
    except ResumoConsultas.DoesNotExist:
        resumo = ResumoConsultas()
        resumo.nome_matriz = relatorio_consulta['result']['nome_matriz']
        resumo.status_protocolo = relatorio_consulta['result']['status_protocolo']
        resumo.lojista = pesquisa_lojista
        resumo.data_cadastro = datetime.now()
        resumo.data_update = datetime.now()
        resumo.save()
        processa_nome_fonte_consulta(resumo, relatorio_consulta)


def processa_nome_fonte_consulta(resumo, relatorio_consulta):
    consultas = relatorio_consulta['result']['consultas']
    for consulta in consultas:
        try:
            if "nome" in consulta:
                nome_fonte = NomeFonteConsulta.objects.get(nome=consulta['nome'], resumo_consulta=resumo)
                nome_fonte.nome = consulta["nome"] if "nome" in consulta else ""
                nome_fonte.data_update = datetime.now()
                nome_fonte.save()
                for tentativas in consulta['tentativas']:
                    dict_tentativa = {
                        "duracao_tentativa": tentativas["duracao_tentativa"],
                        "hora_fim_tentativa": tentativas["hora_fim_tentativa"],
                        "hora_inicio_tentativa": tentativas["hora_inicio_tentativa"],
                        "msg_erro_tentativa": tentativas["msg_erro_tentativa"],
                        "nome_fonte": tentativas["nome_fonte"],
                        "status_fonte": tentativas["status_fonte"],
                        "status_tentativa": tentativas["status_tentativa"],
                        "tipo_erro_tentativa": tentativas["tipo_erro_tentativa"],
                    }
                    pprint(dict_tentativa)
                    altera_tentativas_consulta(nome_fonte, dict_tentativa)

        except NomeFonteConsulta.DoesNotExist:
            nome_fonte = NomeFonteConsulta()
            nome_fonte.nome = consulta['nome']
            nome_fonte.resumo_consulta = resumo
            nome_fonte.data_cadastro = datetime.now()
            nome_fonte.data_update = datetime.now()
            nome_fonte.save()
            for tentativas in consulta['tentativas']:
                dict_tentativa = {
                    "duracao_tentativa": tentativas["duracao_tentativa"],
                    "hora_fim_tentativa": tentativas["hora_fim_tentativa"],
                    "hora_inicio_tentativa": tentativas["hora_inicio_tentativa"],
                    "msg_erro_tentativa": tentativas["msg_erro_tentativa"],
                    "nome_fonte": tentativas["nome_fonte"],
                    "status_fonte": tentativas["status_fonte"],
                    "status_tentativa": tentativas["status_tentativa"],
                    "tipo_erro_tentativa": tentativas["tipo_erro_tentativa"],
                }
                cadastra_tentativas_consulta(nome_fonte, dict_tentativa)


def altera_tentativas_consulta(nome_fonte, dados_tentativa):
    pprint(nome_fonte.id)
    try:
        tentativa = TentativasResumoConsulta.objects.get(fonte=nome_fonte)
        tentativa.duracao_tentativa = dados_tentativa["duracao_tentativa"]
        tentativa.hora_fim_tentativa = dados_tentativa["hora_fim_tentativa"]
        tentativa.hora_inicio_tentativa = dados_tentativa["hora_inicio_tentativa"]
        tentativa.msg_erro_tentativa = dados_tentativa["msg_erro_tentativa"]
        tentativa.fonte = nome_fonte
        tentativa.status_fonte = dados_tentativa["status_fonte"]
        tentativa.status_tentativa = dados_tentativa["status_tentativa"]
        tentativa.tipo_erro_tentativa = dados_tentativa["tipo_erro_tentativa"]

        tentativa.save()
    except TentativasResumoConsulta.DoesNotExist:
        cadastra_tentativas_consulta(nome_fonte, dados_tentativa)


def cadastra_tentativas_consulta(nome_fonte, dados_tentativa):
    tentativa = TentativasResumoConsulta()
    tentativa.duracao_tentativa = dados_tentativa["duracao_tentativa"]
    tentativa.hora_fim_tentativa = dados_tentativa["hora_fim_tentativa"]
    tentativa.hora_inicio_tentativa = dados_tentativa["hora_inicio_tentativa"]
    tentativa.msg_erro_tentativa = dados_tentativa["msg_erro_tentativa"]
    tentativa.fonte = nome_fonte
    tentativa.status_fonte = dados_tentativa["status_fonte"]
    tentativa.status_tentativa = dados_tentativa["status_tentativa"]
    tentativa.tipo_erro_tentativa = dados_tentativa["tipo_erro_tentativa"]

    tentativa.save()
    return tentativa


def processamento_cpf(pesquisa_lojista, consulta_resultado):
    try:
        pesquisa_lojista.mensagem_idwall = consulta_resultado["result"]["mensagem"]
        pesquisa_lojista.matriz_idwall = consulta_resultado["result"]["nome"]
        pesquisa_lojista.numero_idwall = consulta_resultado["result"]["numero"]
        pesquisa_lojista.status_idwall = consulta_resultado["result"]["status"]
        pesquisa_lojista.resultado_idwall = consulta_resultado["result"]["resultado"] if "resultado" in \
                                                                                         consulta_resultado[
                                                                                             "result"] else ""
        pesquisa_lojista.pep_idwall = consulta_resultado["result"]["pep"]["pep"] if "pep" in \
                                                                                    consulta_resultado["result"][
                                                                                        "pep"] else False
        pesquisa_lojista.nome = consulta_resultado["result"]["cpf"]["nome"]
        if "resultado" in consulta_resultado["result"]:
            pesquisa_lojista.fase = "Liberado" if consulta_resultado["result"]["resultado"] == "VALID" else "Risco"
            if pesquisa_lojista.fase == 'Risco':
                pesquisa_lojista.mensagem_integrador = 'Aguarde o setor de risco analisar este cliente'
            atualiza_status_pedido(pesquisa_lojista, "Liberado")
        else:
            pesquisa_lojista.fase = "Em análise"
        pesquisa_lojista.com_protesto = True if "estado_com_protestos" in consulta_resultado["result"][
            "protestos"] else False
        pesquisa_lojista.data_update = datetime.now()
        pesquisa_lojista.save()
        if pesquisa_lojista.resultado_idwall == 'VALID':
            # pagamento = processa_pagamento(pesquisa_lojista)
            # if pagamento == 'pago':
            skipper_integration(pesquisa_lojista.id)
        elif pesquisa_lojista.resultado_idwall == 'INVALID':
            PayStore.py_mail("Nova análise de risco",
                             PayStore.mountEmailRisco(pesquisa_lojista.documento, pesquisa_lojista.nome),
                             "daniel.nascimento@bevipag.com.br",
                             "mail@bevipag.com.br"
                             )
            msgDict = {
                "attachments": [
                    {
                        "fallback": "Nova avaliação de risco.",
                        "color": "#FF8C00",
                        "pretext": "Nova avaliação de risco",
                        "author_name": "Integrador Lucree",
                        "author_link": "http://integrador.lucree.com.br/dashboard/risc-details/1/",
                        "author_icon": "http://flickr.com/icons/bobby.jpg",
                        "title": pesquisa_lojista.nome + " - " + pesquisa_lojista.documento,
                        "title_link": "http://integrador.lucree.com.br/dashboard/risc-details/" + str(
                            pesquisa_lojista.id) + "/",
                        "text": "Clique no link acima avaliar o novo cliente",
                        "fields": [
                            {
                                "title": "Prioridade",
                                "value": "Alta",
                                "short": False
                            },
                            {
                                "title": "Matriz",
                                "value": "Pessoa Física" if pesquisa_lojista.matriz_idwall == 'lucree_QSA_pf' else "Pessoa Jurídica",
                                "short": False
                            }
                        ],
                        "image_url": "http://my-website.com/path/to/image.jpg",
                        "thumb_url": "http://example.com/path/to/thumb.png",
                        "footer": "Análise Id Wall",
                        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
                        "ts": 123456789
                    }
                ]
            }

            Slack.alerta_risco(msgDict)
    except Lojista.DoesNotExist:
        return JsonResponse({"usuario": "Usuario não existe"})


def processa_pagamento(pesquisa_lojista):
    pagamentos_lojista = Pagamento.objects.filter(lojista=pesquisa_lojista)

    for pagamento in pagamentos_lojista:
        data = pagamento.data_pagamento
        pprint(data)
        checagem_transacao = PayStore.getTransaction(
            'acquirer_nsu=' + pagamento.nsu + '&date=' + str(data) + '&init_time=00:00:00&finish_time=23:59:59')
        if len(checagem_transacao['content']) > 0:
            pagamento.pagamento_valido = True
            pagamento.save()

            pedido_relacionado = Pedido.objects.get(pagamento=pagamento)
            pedido_relacionado.status_pedido = 'autorizado'
            pedido_relacionado.save()
            return 'pago'


def processamento_cnpj(pesquisa_lojista, consulta_resultado):
    try:
        pesquisa_lojista.mensagem_idwall = consulta_resultado["result"]["mensagem"]
        pesquisa_lojista.matriz_idwall = consulta_resultado["result"]["nome"]
        pesquisa_lojista.numero_idwall = consulta_resultado["result"]["numero"]
        pesquisa_lojista.status_idwall = consulta_resultado["result"]["status"]
        pesquisa_lojista.resultado_idwall = consulta_resultado["result"]["resultado"] if "resultado" in \
                                                                                         consulta_resultado[
                                                                                             "result"] else ""

        pesquisa_lojista.pep_idwall = consulta_resultado["result"]["pep"]["pep"] if "pep" in consulta_resultado[
            "result"] else False
        pesquisa_lojista.nome = consulta_resultado["result"]["cnpj"]["nome_empresarial"]
        if "resultado" in consulta_resultado["result"]:
            pesquisa_lojista.fase = "Liberado" if consulta_resultado["result"]["resultado"] == "VALID" else "Risco"
            if pesquisa_lojista.fase == 'Risco':
                pesquisa_lojista.mensagem_integrador = 'Aguarde o setor de risco analisar este cliente'
            atualiza_status_pedido(pesquisa_lojista, "Liberado")
        else:
            pesquisa_lojista.fase = "Em análise"
        pesquisa_lojista.com_protesto = True if "estados_com_protestos" in consulta_resultado["result"][
            "protestos"] else False
        pesquisa_lojista.data_update = datetime.now()
        pesquisa_lojista.save()
        if pesquisa_lojista.resultado_idwall == 'VALID':
            # pagamento = processa_pagamento(pesquisa_lojista)
            # if pagamento == 'pago':
            skipper_integration(pesquisa_lojista.id)
        else:
            PayStore.py_mail("Nova análise de risco",
                             PayStore.mountEmailRisco(),
                             "daniel.nascimento@bevipag.com.br",
                             "mail@bevipag.com.br"
                             )
            msgDict = {
                "attachments": [
                    {
                        "fallback": "Nova avaliação de risco.",
                        "color": "#FF8C00",
                        "pretext": "Nova avaliação de risco",
                        "author_name": "Integrador Lucree",
                        "author_link": "http://192.168.99.100:8000/dashboard/risc-details/1/",
                        "author_icon": "http://flickr.com/icons/bobby.jpg",
                        "title": pesquisa_lojista.nome + " - " + pesquisa_lojista.documento,
                        "title_link": "http://192.168.99.100:8000/dashboard/risc-details/" + str(
                            pesquisa_lojista.id) + "/",
                        "text": "Clique no link acima avaliar o novo cliente",
                        "fields": [
                            {
                                "title": "Prioridade",
                                "value": "Alta",
                                "short": False
                            },
                            {
                                "title": "Matriz",
                                "value": "Pessoa Física" if pesquisa_lojista.matriz_idwall == 'lucree_QSA_pf' else "Pessoa Jurídica",
                                "short": False
                            }
                        ],
                        "image_url": "http://my-website.com/path/to/image.jpg",
                        "thumb_url": "http://example.com/path/to/thumb.png",
                        "footer": "Análise Id Wall",
                        "footer_icon": "https://platform.slack-edge.com/img/default_application_icon.png",
                        "ts": 123456789
                    }
                ]
            }

            Slack.alerta_risco(msgDict)

    except Lojista.DoesNotExist:
        return JsonResponse({"usuario": "Usuario não existe"})
    pass


def atualiza_status_pedido(cliente, fase_cliente):
    if fase_cliente == 'Liberado':
        pedidos = Pedido.objects.filter(lojista=cliente)
        for pedido in pedidos:
            pedido.status_pedido = 'válido'
            pedido.save()


def atualiza_protestos(pesquisa_lojista, consulta_resultado):
    if "estados_com_protestos" in consulta_resultado["result"]["protestos"]:
        for protesto in consulta_resultado["result"]["protestos"]["estados_com_protestos"]:
            prot = Protesto()
            prot.estado_protesto = protesto
            prot.lojista = pesquisa_lojista
            prot.save()


def invertDate(data):
    try:
        array_data = data.split("-")
        return array_data[2] + "-" + array_data[1] + "-" + array_data[0]
    except IndexError:
        array_data = data.split("/")
        return array_data[2] + "-" + array_data[1] + "-" + array_data[0]


def atualiza_cpf_lojista(pesquisa_lojista, consulta_resultado):
    pprint('PASSANDO NO PROCESSAMENTOOOOO 2')
    try:
        cpf = Cpf.objects.get(lojista=pesquisa_lojista)
        cpf.sexo = consulta_resultado["result"]["cpf"]["sexo"] if "sexo" in consulta_resultado["result"]["cpf"] else ""
        cpf.numero = consulta_resultado["result"]["cpf"]["numero"] if "numero" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.data_de_nascimento = invertDate(
            consulta_resultado["result"]["cpf"]["data_de_nascimento"]) if "data_de_nascimento" in \
                                                                          consulta_resultado["result"][
                                                                              "cpf"] else "1999-09-09"
        cpf.nome = consulta_resultado["result"]["cpf"]["nome"] if "nome" in consulta_resultado["result"]["cpf"] else ""
        cpf.renda = consulta_resultado["result"]["cpf"]["renda"] if "renda" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.pep = consulta_resultado["result"]["cpf"]["pep"] if "pep" in consulta_resultado["result"]["cpf"] else ""
        cpf.situacao_imposto_de_renda = consulta_resultado["result"]["cpf"][
            "situacao_imposto_de_renda"] if "situacao_imposto_de_renda" in consulta_resultado["result"]["cpf"] else ""
        cpf.cpf_situacao_cadastral = consulta_resultado["result"]["cpf"][
            "cpf_situacao_cadastral"] if "cpf_situacao_cadastral" in consulta_resultado["result"]["cpf"] else ""

        cpf.cpf_data_de_inscricao = invertDate(
            consulta_resultado["result"]["cpf"]["cpf_data_de_inscricao"]) if "cpf_data_de_inscricao" in \
                                                                             consulta_resultado["result"][
                                                                                 "cpf"] else "1999-09-09"

        cpf.cpf_digito_verificador = consulta_resultado["result"]["cpf"][
            "cpf_digito_verificador"] if "cpf_digito_verificador" in consulta_resultado["result"]["cpf"] else ""
        cpf.cpf_anterior_1990 = consulta_resultado["result"]["cpf"]["cpf_anterior_1990"] if "cpf_anterior_1990" in \
                                                                                            consulta_resultado[
                                                                                                "result"]["cpf"] else ""
        cpf.ano_obito = consulta_resultado["result"]["cpf"]["ano_obito"] if "ano_obito" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.grafia = consulta_resultado["result"]["cpf"]["grafias"][0] if "grafias" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.lojista = pesquisa_lojista
        cpf.data_cadastro = datetime.now()
        cpf.data_update = datetime.now()
        cpf.save()
        return JsonResponse({"resultado": "cpf atualizado"})

    except Cpf.DoesNotExist:
        cpf = Cpf()
        cpf.sexo = consulta_resultado["result"]["cpf"]["sexo"] if "sexo" in consulta_resultado["result"]["cpf"] else ""
        cpf.numero = consulta_resultado["result"]["cpf"]["numero"] if "numero" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.data_de_nascimento = invertDate(
            consulta_resultado["result"]["cpf"]["data_de_nascimento"]) if "data_de_nascimento" in \
                                                                          consulta_resultado["result"][
                                                                              "cpf"] else "1999-09-09"
        cpf.nome = consulta_resultado["result"]["cpf"]["nome"] if "nome" in consulta_resultado["result"]["cpf"] else ""
        cpf.renda = consulta_resultado["result"]["cpf"]["renda"] if "renda" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.pep = consulta_resultado["result"]["cpf"]["pep"] if "pep" in consulta_resultado["result"]["cpf"] else ""
        cpf.situacao_imposto_de_renda = consulta_resultado["result"]["cpf"][
            "situacao_imposto_de_renda"] if "situacao_imposto_de_renda" in consulta_resultado["result"]["cpf"] else ""
        cpf.cpf_situacao_cadastral = consulta_resultado["result"]["cpf"][
            "cpf_situacao_cadastral"] if "cpf_situacao_cadastral" in consulta_resultado["result"]["cpf"] else ""
        cpf.cpf_data_de_inscricao = invertDate(
            consulta_resultado["result"]["cpf"]["cpf_data_de_inscricao"]) if "cpf_data_de_inscricao" in \
                                                                             consulta_resultado["result"][
                                                                                 "cpf"] else "1999-09-09"
        cpf.cpf_digito_verificador = consulta_resultado["result"]["cpf"][
            "cpf_digito_verificador"] if "cpf_digito_verificador" in consulta_resultado["result"]["cpf"] else ""
        cpf.cpf_anterior_1990 = consulta_resultado["result"]["cpf"]["cpf_anterior_1990"] if "cpf_anterior_1990" in \
                                                                                            consulta_resultado[
                                                                                                "result"]["cpf"] else ""
        cpf.ano_obito = consulta_resultado["result"]["cpf"]["ano_obito"] if "ano_obito" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.grafia = consulta_resultado["result"]["cpf"]["grafias"][0] if "grafias" in consulta_resultado["result"][
            "cpf"] else ""
        cpf.lojista = pesquisa_lojista
        cpf.data_cadastro = datetime.now()
        cpf.data_update = datetime.now()
        cpf.save()
        return JsonResponse({"resultado": "novo cpf"})


def atualiza_cnpj_lojista(pesquisa_lojista, consulta_resultado):
    try:
        pprint('inicia_processo_pj')
        pprint(pesquisa_lojista)
        cnpj = Cnpj.objects.get(lojista=pesquisa_lojista)
        cnpj.cnpj = consulta_resultado["result"]["cnpj"]["cnpj"] if "cnpj" in consulta_resultado["result"]["cnpj"] else \
            consulta_resultado["result"]["cnpj"]["numero"]
        cnpj.nome_fantasia = consulta_resultado["result"]["cnpj"]["nome_fantasia"]
        cnpj.nome_empresarial = consulta_resultado["result"]["cnpj"]["nome_empresarial"]
        cnpj.data_abertura = invertDate(consulta_resultado["result"]["cnpj"]["data_abertura"]) if \
            consulta_resultado["result"]["cnpj"]["data_abertura"] else None
        cnpj.fonte = consulta_resultado["result"]["cnpj"]["fonte"] if "fonte" in consulta_resultado["result"][
            "cnpj"] else ""
        cnpj.atividade_principal = consulta_resultado["result"]["cnpj"]["atividade_principal"]
        cnpj.faturamento_presumido = consulta_resultado["result"]["cnpj"][
            "faturamento_presumido"] if "faturamento_presumido" in consulta_resultado["result"]["cnpj"] else ""
        cnpj.numero_funcionarios = consulta_resultado["result"]["cnpj"][
            "numero_funcionarios"] if "numero_funcionarios" in consulta_resultado["result"]["cnpj"] else ""
        cnpj.numero_funcionarios_filiais = consulta_resultado["result"]["cnpj"][
            "numero_funcionarios_filiais"] if "numero_funcionarios_filiais" in consulta_resultado["result"][
            "cnpj"] else ""
        cnpj.regime_tributario = consulta_resultado["result"]["cnpj"]["regime_tributario"] if "regime_tributario" in \
                                                                                              consulta_resultado[
                                                                                                  "result"][
                                                                                                  "cnpj"] else ""
        cnpj.irs_status = consulta_resultado["result"]["cnpj"]["irs_status"] if "irs_status" in \
                                                                                consulta_resultado["result"][
                                                                                    "cnpj"] else ""
        cnpj.capital_social = consulta_resultado["result"]["cnpj"]["capital_social"]
        cnpj.data_situacao_cadastral = invertDate(consulta_resultado["result"]["cnpj"]["data_situacao_cadastral"]) if \
            consulta_resultado["result"]["cnpj"]["data_situacao_cadastral"] else None
        cnpj.data_situacao_especial = invertDate(consulta_resultado["result"]["cnpj"]["data_situacao_especial"]) if \
            consulta_resultado["result"]["cnpj"]["data_situacao_especial"] else None
        cnpj.email = consulta_resultado["result"]["cnpj"]["email"]
        cnpj.ente_federativo_responsavel = consulta_resultado["result"]["cnpj"]["ente_federativo_responsavel"]
        cnpj.motivo_situacao_cadastral = consulta_resultado["result"]["cnpj"]["motivo_situacao_cadastral"]
        cnpj.natureza_juridica = consulta_resultado["result"]["cnpj"]["natureza_juridica"]
        cnpj.numero = consulta_resultado["result"]["cnpj"]["numero"]
        cnpj.situacao_cadastral = consulta_resultado["result"]["cnpj"]["situacao_cadastral"]
        cnpj.situacao_especial = consulta_resultado["result"]["cnpj"]["situacao_especial"]
        cnpj.telefone = consulta_resultado["result"]["cnpj"]["telefone"]
        cnpj.tipo = consulta_resultado["result"]["cnpj"]["tipo"]
        cnpj.localizacao_bairro = consulta_resultado["result"]["cnpj"]["localizacao"]["bairro"]
        cnpj.localizacao_cep = consulta_resultado["result"]["cnpj"]["localizacao"]["cep"]
        cnpj.localizacao_cidade = consulta_resultado["result"]["cnpj"]["localizacao"]["cidade"]
        cnpj.localizacao_complemento = consulta_resultado["result"]["cnpj"]["localizacao"]["complemento"]
        cnpj.localizacao_estado = consulta_resultado["result"]["cnpj"]["localizacao"]["estado"]
        cnpj.localizacao_logradouro = consulta_resultado["result"]["cnpj"]["localizacao"]["logradouro"]
        cnpj.localizacao_numero = consulta_resultado["result"]["cnpj"]["localizacao"]["numero"]
        cnpj.lojista = pesquisa_lojista
        cnpj.data_cadastro = datetime.now()
        cnpj.data_update = datetime.now()

        cnpj.save()
        return cnpj
    except Cnpj.DoesNotExist:
        pprint('inicia_processo_pj except')
        cnpj = Cnpj()
        cnpj.cnpj = consulta_resultado["result"]["cnpj"]["cnpj"] if "cnpj" in consulta_resultado["result"]["cnpj"] else \
            consulta_resultado["result"]["cnpj"]["numero"]
        cnpj.nome_fantasia = consulta_resultado["result"]["cnpj"]["nome_fantasia"]
        cnpj.nome_empresarial = consulta_resultado["result"]["cnpj"]["nome_empresarial"]
        cnpj.data_abertura = invertDate(consulta_resultado["result"]["cnpj"]["data_abertura"]) if \
            consulta_resultado["result"]["cnpj"]["data_abertura"] else None
        cnpj.fonte = consulta_resultado["result"]["cnpj"]["fonte"] if "fonte" in consulta_resultado["result"][
            "cnpj"] else ""
        cnpj.atividade_principal = consulta_resultado["result"]["cnpj"]["atividade_principal"]
        cnpj.faturamento_presumido = consulta_resultado["result"]["cnpj"][
            "faturamento_presumido"] if "faturamento_presumido" in consulta_resultado["result"]["cnpj"] else ""
        cnpj.numero_funcionarios = consulta_resultado["result"]["cnpj"][
            "numero_funcionarios"] if "numero_funcionarios" in consulta_resultado["result"]["cnpj"] else ""
        cnpj.numero_funcionarios_filiais = consulta_resultado["result"]["cnpj"][
            "numero_funcionarios_filiais"] if "numero_funcionarios_filiais" in consulta_resultado["result"][
            "cnpj"] else ""
        cnpj.regime_tributario = consulta_resultado["result"]["cnpj"]["regime_tributario"] if 'regime_tributario' in \
                                                                                              consulta_resultado[
                                                                                                  "result"][
                                                                                                  "cnpj"] else "",
        cnpj.irs_status = consulta_resultado["result"]["cnpj"]["irs_status"] if "irs_status" in \
                                                                                consulta_resultado["result"][
                                                                                    "cnpj"] else ""
        cnpj.capital_social = consulta_resultado["result"]["cnpj"]["capital_social"]
        cnpj.data_situacao_cadastral = invertDate(consulta_resultado["result"]["cnpj"]["data_situacao_cadastral"]) if \
            consulta_resultado["result"]["cnpj"]["data_situacao_cadastral"] else None
        cnpj.data_situacao_especial = invertDate(consulta_resultado["result"]["cnpj"]["data_situacao_especial"]) if \
            consulta_resultado["result"]["cnpj"]["data_situacao_especial"] else None
        cnpj.email = consulta_resultado["result"]["cnpj"]["email"]
        cnpj.ente_federativo_responsavel = consulta_resultado["result"]["cnpj"]["ente_federativo_responsavel"]
        cnpj.motivo_situacao_cadastral = consulta_resultado["result"]["cnpj"]["motivo_situacao_cadastral"]
        cnpj.natureza_juridica = consulta_resultado["result"]["cnpj"]["natureza_juridica"]
        cnpj.numero = consulta_resultado["result"]["cnpj"]["numero"]
        cnpj.situacao_cadastral = consulta_resultado["result"]["cnpj"]["situacao_cadastral"]
        cnpj.situacao_especial = consulta_resultado["result"]["cnpj"]["situacao_especial"]
        cnpj.telefone = consulta_resultado["result"]["cnpj"]["telefone"]
        cnpj.tipo = consulta_resultado["result"]["cnpj"]["tipo"]
        cnpj.localizacao_bairro = consulta_resultado["result"]["cnpj"]["localizacao"]["bairro"]
        cnpj.localizacao_cep = consulta_resultado["result"]["cnpj"]["localizacao"]["cep"]
        cnpj.localizacao_cidade = consulta_resultado["result"]["cnpj"]["localizacao"]["cidade"]
        cnpj.localizacao_complemento = consulta_resultado["result"]["cnpj"]["localizacao"]["complemento"]
        cnpj.localizacao_estado = consulta_resultado["result"]["cnpj"]["localizacao"]["estado"]
        cnpj.localizacao_logradouro = consulta_resultado["result"]["cnpj"]["localizacao"]["logradouro"]
        cnpj.localizacao_numero = consulta_resultado["result"]["cnpj"]["localizacao"]["numero"]
        cnpj.lojista = pesquisa_lojista
        cnpj.data_cadastro = datetime.now()
        cnpj.data_update = datetime.now()

        cnpj.save()
        return cnpj


def atualiza_certidoes_negativas(pesquisa_lojista, consulta_resultado):
    certidoes = consulta_resultado["result"]["certidoes_negativas"]["itens"]

    for certidao in certidoes:
        try:

            itens = CertidoesNegativasItens.objects.get(protocolo=certidao["protocolo"], lojista=pesquisa_lojista)
            itens.emitido_em = certidao["emitido_em"]
            itens.fonte = certidao["fonte"]
            itens.protocolo = certidao["protocolo"]
            itens.cnpj = certidao["url"]
            itens.nada_consta = certidao["nada_consta"]
            itens.lojista = pesquisa_lojista
            itens.data_update = datetime.now()
            itens.save()
            return itens

        except CertidoesNegativasItens.DoesNotExist:
            itens = CertidoesNegativasItens()
            itens.emitido_em = certidao["emitido_em"]
            itens.fonte = certidao["fonte"]
            itens.protocolo = certidao["protocolo"]
            itens.cnpj = certidao["url"]
            itens.nada_consta = certidao["nada_consta"]
            itens.lojista = pesquisa_lojista
            itens.data_criacao = datetime.now()
            itens.data_update = datetime.now()
            itens.save()
            return itens


def atualiza_atividades_secundarias(consulta_resultado, cnpj):
    atividades = consulta_resultado["result"]["cnpj"]["atividades_secundarias"]
    for atividade in atividades:
        at = AtividadesSecundaria()
        at.atividade = atividade
        at.cnpj = cnpj
        at.data_update = datetime.now()
        at.data_cadastro = datetime.now()
        at.save()


def atualiza_grafias(consulta_resultado, cnpj):
    grafias = consulta_resultado["result"]["cnpj"]["grafias"]
    for grafia in grafias:
        at = GrafiasCnpj()
        at.grafia = grafia
        at.cnpj = cnpj
        at.data_update = datetime.now()
        at.data_cadastro = datetime.now()
        at.save()


def atualiza_qsa(consulta_resultado, cnpj):
    qsas = consulta_resultado["result"]["cnpj"]["qsa"]
    for qsa in qsas:
        try:
            at = QsaCnpj.objects.get(cnpj=cnpj)
            at.nome = qsa['nome'] if qsa['nome'] else ""
            at.nome_representante_legal = qsa['nome_representante_legal']
            at.pais_origem = qsa['pais_origem']
            at.qualificacao = qsa['qualificacao'] if qsa['qualificacao'] else ""
            at.cnpj = cnpj
            at.qualificacao_representante_legal = qsa['qualificacao_representante_legal'] if qsa[
                'qualificacao_representante_legal'] else ""
            at.data_update = datetime.now()
            at.save()
        except QsaCnpj.DoesNotExist:
            at = QsaCnpj()
            at.nome = qsa['nome'] if qsa['nome'] else ""
            at.nome_representante_legal = qsa['nome_representante_legal']
            at.pais_origem = qsa['pais_origem']
            at.qualificacao = qsa['qualificacao'] if qsa['qualificacao'] else ""
            at.cnpj = cnpj
            at.qualificacao_representante_legal = qsa['qualificacao_representante_legal'] if qsa[
                'qualificacao_representante_legal'] else ""
            at.data_update = datetime.now()
            at.data_cadastro = datetime.now()
            at.save()


def atualiza_sintegra(pesquisa_lojista, consulta_resultado):
    sintegra_itens = consulta_resultado["result"]["sintegra"]["itens"]
    for item in sintegra_itens:
        try:
            sintegra = SintegraItem.objects.get(lojista=pesquisa_lojista)
            sintegra.cnpj = item['cnpj']
            sintegra.inscricao_estadual = item['inscricao_estadual']
            sintegra.razao_social = item['razao_social']
            sintegra.cnae_principal = item['cnae_principal']
            sintegra.situacao_cadastral_vigente = item['situacao_cadastral_vigente']
            sintegra.data_situacao_cadastral = invertDate(item['data_situacao_cadastral']) if item[
                'data_situacao_cadastral'] else None
            sintegra.regime_apuracao = item['regime_apuracao']
            sintegra.data_credenciamento_nfe = invertDate(item['data_credenciamento_nfe']) if item[
                'data_credenciamento_nfe'] else None
            sintegra.indicador_obrigatoriedade_nfe = item['indicador_obrigatoriedade_nfe']
            sintegra.data_obrigatoriedade_nfe = item['data_obrigatoriedade_nfe'] if item[
                'data_obrigatoriedade_nfe'] else None
            sintegra.protocolo = item['protocolo']
            sintegra.tipo = item['tipo']
            sintegra.uf = item['uf'] if item['uf'] else consulta_resultado["result"]["cnpj"]["localizacao"]["estado"]
            sintegra.cad_icms = item['cad_icms']
            sintegra.cae = item['cae']
            sintegra.nome_fantasia = item['nome_fantasia']
            sintegra.data_inicio_atividade = invertDate(item['data_inicio_atividade']) if item[
                'data_inicio_atividade'] else None
            sintegra.natureza_juridica = item['natureza_juridica']
            sintegra.natureza_estabelecimento = item['natureza_estabelecimento']
            sintegra.data_abertura = invertDate(item['data_abertura']) if item['data_abertura'] else None
            sintegra.data_baixa = invertDate(item['data_baixa']) if item['data_baixa'] else None
            sintegra.delegacia_fazendaria = item['delegacia_fazendaria']
            sintegra.enquadramento_empresa = item['enquadramento_empresa']
            sintegra.observacao = item['observacao']
            sintegra.data_inscricao = invertDate(item['data_inscricao']) if item[
                'data_inscricao'] else None
            sintegra.ped = item['ped']
            sintegra.data_inicio_ped = invertDate(item['data_inicio_ped']) if item['data_inicio_ped'] else None
            sintegra.ultrapassou_limite_estadual = item['ultrapassou_limite_estadual']
            sintegra.data_inicio_simples_nacional = invertDate(item['data_inicio_simples_nacional']) if item[
                'data_inicio_ped'] else None
            sintegra.email = item['email']
            sintegra.indicador_obrigatoriedade_efd = item['indicador_obrigatoriedade_efd']
            sintegra.data_obrigatoriedade_efd = invertDate(item['data_obrigatoriedade_efd']) if item[
                'data_obrigatoriedade_efd'] else None
            sintegra.opcao_simples = item['opcao_simples']
            sintegra.condicao = item['condicao']
            sintegra.indicador_obrigatoriedade_cte = item['indicador_obrigatoriedade_cte']
            sintegra.data_obrigatoriedade_cte = invertDate(item['data_obrigatoriedade_cte']) if item[
                'data_obrigatoriedade_cte'] else None
            sintegra.situacao_sintegra = item['situacao_sintegra']
            sintegra.tipo_unidade_auxiliar = item['tipo_unidade_auxiliar']
            sintegra.regime_pagamento = item['regime_pagamento']
            sintegra.situacao_contribuinte = item['situacao_contribuinte']
            sintegra.lojista = pesquisa_lojista
            sintegra.data_update = datetime.now()
            sintegra.save()
        except SintegraItem.DoesNotExist:
            sintegra = SintegraItem()
            sintegra.cnpj = item['cnpj']
            sintegra.inscricao_estadual = item['inscricao_estadual']
            sintegra.razao_social = item['razao_social']
            sintegra.cnae_principal = item['cnae_principal']
            sintegra.situacao_cadastral_vigente = item['situacao_cadastral_vigente']
            sintegra.data_situacao_cadastral = invertDate(item['data_situacao_cadastral']) if item[
                'data_situacao_cadastral'] else None
            sintegra.regime_apuracao = item['regime_apuracao']
            sintegra.data_credenciamento_nfe = invertDate(item['data_credenciamento_nfe']) if item[
                'data_credenciamento_nfe'] else None
            sintegra.indicador_obrigatoriedade_nfe = item['indicador_obrigatoriedade_nfe']
            sintegra.data_obrigatoriedade_nfe = item['data_obrigatoriedade_nfe'] if item[
                'data_obrigatoriedade_nfe'] else None
            sintegra.protocolo = item['protocolo']
            sintegra.tipo = item['tipo']
            sintegra.uf = item['uf'] if item['uf'] else consulta_resultado["result"]["cnpj"]["localizacao"]["estado"]
            sintegra.cad_icms = item['cad_icms']
            sintegra.cae = item['cae']
            sintegra.nome_fantasia = item['nome_fantasia']
            sintegra.data_inicio_atividade = invertDate(item['data_inicio_atividade']) if item['data_baixa'] else None
            sintegra.natureza_juridica = item['natureza_juridica']
            sintegra.natureza_estabelecimento = item['natureza_estabelecimento']
            sintegra.data_abertura = invertDate(item['data_abertura']) if item['data_abertura'] else None
            sintegra.data_baixa = invertDate(item['data_baixa']) if item['data_baixa'] else None
            sintegra.delegacia_fazendaria = item['delegacia_fazendaria']
            sintegra.enquadramento_empresa = item['enquadramento_empresa']
            sintegra.observacao = item['observacao']
            sintegra.data_inscricao = invertDate(item['data_inscricao']) if item['data_inscricao'] else None
            sintegra.ped = item['ped']
            sintegra.data_inicio_ped = invertDate(item['data_inicio_ped']) if item['data_inicio_ped'] else None
            sintegra.ultrapassou_limite_estadual = item['ultrapassou_limite_estadual']
            sintegra.data_inicio_simples_nacional = invertDate(item['data_inicio_simples_nacional']) if item[
                'data_inicio_simples_nacional'] else None
            sintegra.email = item['email']
            sintegra.indicador_obrigatoriedade_efd = item['indicador_obrigatoriedade_efd']
            sintegra.data_obrigatoriedade_efd = invertDate(item['data_obrigatoriedade_efd']) if item[
                'data_obrigatoriedade_efd'] else None
            sintegra.opcao_simples = item['opcao_simples']
            sintegra.condicao = item['condicao']
            sintegra.indicador_obrigatoriedade_cte = item['indicador_obrigatoriedade_cte']
            sintegra.data_obrigatoriedade_cte = invertDate(item['data_obrigatoriedade_cte']) if item[
                'data_obrigatoriedade_cte'] else None
            sintegra.situacao_sintegra = item['situacao_sintegra']
            sintegra.tipo_unidade_auxiliar = item['tipo_unidade_auxiliar']
            sintegra.regime_pagamento = item['regime_pagamento']
            sintegra.situacao_contribuinte = item['situacao_contribuinte']
            sintegra.lojista = pesquisa_lojista
            sintegra.data_update = datetime.now()
            sintegra.data_cadastro = datetime.now()
            sintegra.save()
        return sintegra


def atualiza_sintegra_atividades(consulta_resultado, sintegra):
    if len(consulta_resultado["result"]["sintegra"]["itens"]):
        sintegra_atividades = consulta_resultado["result"]["sintegra"]["itens"]
        pprint(sintegra_atividades)
        for atividade in sintegra_atividades:
            if atividade["atividades_secundarias"]:
                for atividades_secundarias in atividade["atividades_secundarias"]:
                    try:
                        at = SintegraItemAtividadeSecundaria.objects.get(sintegra_item=sintegra,
                                                                         codigo=atividades_secundarias['codigo'])
                        at.codigo = atividades_secundarias['codigo']
                        at.descricao = atividades_secundarias['descricao']
                        at.sintegra_item = sintegra
                        at.data_update = datetime.now()
                        at.save()
                    except SintegraItemAtividadeSecundaria.DoesNotExist:
                        at = SintegraItemAtividadeSecundaria()
                        at.codigo = atividades_secundarias['codigo']
                        at.descricao = atividades_secundarias['descricao']
                        at.sintegra_item = sintegra
                        at.data_update = datetime.now()
                        at.data_cadastro = datetime.now()
                        at.save()


def atualiza_telefones(consulta_resultado, pesquisa_lojista):
    if "telefones" in consulta_resultado["result"]:
        telefones = consulta_resultado["result"]["telefones"]
        for telefone in telefones:
            tel = Telefones()
            pprint('DDD')
            pprint(telefone['fone_ddd'])
            if len(telefone['fone_ddd']) > 2:
                tel.fone_ddd = "00"
            else:
                tel.fone_ddd = telefone['fone_ddd']
            tel.fone_numero = telefone['fone_numero']
            tel.tipo_assinante = telefone['tipo_assinante']
            tel.pessoas_com_o_mesmo_telefone = telefone[
                'pessoas_com_o_mesmo_telefone'] if 'pessoas_com_o_mesmo_telefone' in telefone else 0
            tel.lojista = pesquisa_lojista
            tel.data_cadastro = datetime.now()
            tel.data_update = datetime.now()
            tel.save()


def atualiza_participacao_empresas(consulta_resultado, pesquisa_lojista):
    if "participacao_empresas" in consulta_resultado["result"]:
        participacoes = consulta_resultado["result"]["participacao_empresas"]["itens"]
        for participacao in participacoes:
            try:
                part = ParticipacaoEmpresasItens.objects.get(lojista=pesquisa_lojista)
                part.cnpj = participacao['cnpj']
                part.nome_empresarial = participacao['nome_empresarial']
                part.tipo_relacionamento = participacao[
                    'tipo_relacionamento'] if 'tipo_relacionamento' in participacao else None
                part.cargo = participacao['cargo'] if 'cargo' in participacao else None
                part.lojista = pesquisa_lojista
                part.data_update = datetime.now()
                part.save()
            except ParticipacaoEmpresasItens.DoesNotExist:
                part = ParticipacaoEmpresasItens()
                part.cnpj = participacao['cnpj']
                part.nome_empresarial = participacao['nome_empresarial']
                part.tipo_relacionamento = participacao[
                    'tipo_relacionamento'] if 'tipo_relacionamento' in participacao else None
                part.cargo = participacao['cargo'] if 'cargo' in participacao else None
                part.lojista = pesquisa_lojista
                part.data_cadastro = datetime.now()
                part.data_update = datetime.now()
                part.save()


def atualiza_divida_ativa(pesquisa_lojista, consulta_resultado):
    if "divida_ativa" in consulta_resultado["result"]:
        divida_ativa = consulta_resultado["result"]["divida_ativa"]
        if "nome" in divida_ativa:
            pprint(divida_ativa['nome'])
            div = DividaAtiva()
            div.nome = divida_ativa['nome']
            div.valor_devido = divida_ativa['valor_devido']
            div.lojista = pesquisa_lojista
            div.data_cadastro = datetime.now()
            div.data_update = datetime.now()
            div.save()


def atualiza_pessoas_relacionadas(pesquisa_lojista, consulta_resultado):
    if "pessoas_relacionadas" in consulta_resultado["result"]:
        lista_pessoas = consulta_resultado["result"]["pessoas_relacionadas"]
        for pessoa in lista_pessoas:
            pessoas = PessoasRelacionadas()
            pessoas.cpf = pessoa["cpf"] if "cpf" in pessoa else ""
            pessoas.nome = pessoa["nome"] if "nome" in pessoa else ""
            pessoas.tipo = pessoa["tipo"] if "tipo" in pessoa else ""
            pessoas.cargo = pessoa["cargo"] if "cargo" in pessoa else ""
            pessoas.cnpj = pessoa["cnpj"] if "cnpj" in pessoa else ""
            pessoas.lojista = pesquisa_lojista
            pessoas.data_cadastro = datetime.now()
            pessoas.data_update = datetime.now()
            pessoas.save()

            try:
                existente = Lojista.objects.get(documento=pessoa["cpf"] if "cpf" in pessoa else pessoa["cnpj"])
                existente.matriz_idwall = "lucree_QSA_pf"
                existente.documento = pessoa["cpf"] if "cpf" in pessoa else pessoa["cnpj"]
                existente.numero_idwall = pessoa["protocolo"] if "protocolo" in pessoa else ""
                existente.relatorio_a_partir_pj = True
                existente.departamento = 0
                existente.save()
            except Lojista.DoesNotExist:
                lojista = Lojista()
                lojista.matriz_idwall = "lucree_QSA_pf"
                lojista.documento = pessoa["cpf"] if "cpf" in pessoa else pessoa["cnpj"]
                lojista.numero_idwall = pessoa["protocolo"] if "protocolo" in pessoa else ""
                lojista.relatorio_a_partir_pj = True
                lojista.departamento = 0
                lojista.save()

            if "protocolo" in pessoa:
                consulta_resultado = IdWall.consulta_resultado(pessoa["protocolo"])
                inicia_processo_pf_relacoes(consulta_resultado, pessoa["protocolo"])

    return JsonResponse({"resultado": "cpf atualizado"})


def atualiza_enderecos(pesquisa_lojista, consulta_resultado):
    if "enderecos" in consulta_resultado["result"]:
        lista_enderecos = consulta_resultado["result"]["enderecos"]
        for endereco in lista_enderecos:
            enderecos = EnderecoLojista()
            enderecos.principal = endereco["principal"] if "principal" in endereco else ""
            enderecos.cidade = endereco["cidade"] if "cidade" in endereco else ""
            enderecos.estado = endereco["estado"] if "estado" in endereco else ""
            enderecos.numero = endereco["numero"] if "numero" in endereco else ""
            enderecos.complemento = endereco["complemento"] if "complemento" in endereco else ""
            enderecos.logradouro = endereco["logradouro"] if "logradouro" in endereco else ""
            enderecos.bairro = endereco["bairro"] if "bairro" in endereco else ""
            enderecos.tipo = endereco["tipo"] if "tipo" in endereco else ""
            enderecos.cep = endereco["cep"] if "cep" in endereco else ""
            enderecos.pessoas_endereco = endereco["pessoas_no_endereco"] if "pessoas_no_endereco" in endereco else ""
            enderecos.lojista = pesquisa_lojista
            enderecos.data_cadastro = datetime.now()
            enderecos.data_update = datetime.now()
            enderecos.save()

    return JsonResponse({"resultado": "cpf atualizado"})


def atualiza_email(pesquisa_lojista, consulta_resultado):
    if "emails" in consulta_resultado["result"]:
        lista_emails = consulta_resultado["result"]["emails"]
        for email in lista_emails:
            try:
                if "endereco" in email:
                    mails = EmailsLojista.objects.get(endereco=email["endereco"], lojista=pesquisa_lojista)
                    mails.data_update = datetime.now()
                    mails.save()

            except EmailsLojista.DoesNotExist:
                mails = EmailsLojista()
                mails.endereco = email["endereco"]
                mails.lojista = pesquisa_lojista
                mails.data_cadastro = datetime.now()
                mails.data_update = datetime.now()
                mails.save()

        return JsonResponse({"resultado": "cpf atualizado"})


def pedidos_aprovados(request):
    pedidos = Pedido.objects.filter(status_pedido="autorizado").select_related('lojista', 'pagamento')

    return render(request, 'dashboard/pedidos_autorizados.html', {
        'pedidos': pedidos,
        'logged_user': request.user
    })


def exit(request):
    logout(request)
    return HttpResponseRedirect('/dashboard/login/')


def split_pagamento(request):
    lojistas = Lojista.objects.filter().exclude(porcentagem_favorecido='')

    cnpj_conta_origem = []

    cnpj_cpf_conta_destino_1 = []
    nome_conta_destino_1 = []
    percentual_conta_destino_1 = []
    agencia_destino_1 = []
    conta_destino_1 = []

    cnpj_conta_destino_2 = []
    nome_conta_destino_2 = []
    percentual_conta_destino_2 = []
    agencia_destino_2 = []
    conta_destino_2 = []

    for lojista in lojistas:
        cnpj_conta_origem.append('13203354000185')
        if lojista.porcentagem_favorecido != '100' and lojista.porcentagem_favorecido != '':
            pprint(lojista.nome)
            domicilio_bancario = DomicilioBancario.objects.get(lojista=lojista)
            cnpj_cpf_conta_destino_1.append(lojista.documento)
            nome_conta_destino_1.append(lojista.nome)
            percentual_conta_destino_1.append(lojista.porcentagem_cliente)
            agencia_destino_1.append(domicilio_bancario.agencia)
            conta_destino_1.append(domicilio_bancario.conta + "-" + domicilio_bancario.digito_conta)
        else:
            cnpj_cpf_conta_destino_1.append(0)
            nome_conta_destino_1.append(0)
            percentual_conta_destino_1.append(0)
            agencia_destino_1.append(0)
            conta_destino_1.append(0)

        cnpj_conta_destino_2.append(lojista.cnpj_favorecido)
        nome_conta_destino_2.append(lojista.nome_razao_favorecido)
        percentual_conta_destino_2.append(lojista.porcentagem_favorecido)
        agencia_destino_2.append(lojista.agencia_favorecido)
        conta_destino_2.append(lojista.conta_favorecido + '-' + lojista.digito_conta_favorecido)

    dict_final = {
        'cnpj_conta_origem': cnpj_conta_origem,
        'cnpj_cpf_conta_destino_1': cnpj_cpf_conta_destino_1,
        'nome_conta_destino_1': nome_conta_destino_1,
        'percentual_conta_destino_1': percentual_conta_destino_1,
        'agencia_destino_1': agencia_destino_1,
        'conta_destino_1': conta_destino_1,
        'cnpj_conta_destino_2': cnpj_conta_destino_2,
        'nome_conta_destino_2': nome_conta_destino_2,
        'percentual_conta_destino_2': percentual_conta_destino_2,
        'agencia_destino_2': agencia_destino_2,
        'conta_destino_2': conta_destino_2
    }

    df = pd.DataFrame(dict_final,
                      columns=['cnpj_conta_origem', 'cnpj_cpf_conta_destino_1', 'nome_conta_destino_1',
                               'percentual_conta_destino_1', 'agencia_destino_1', 'conta_destino_1',
                               'cnpj_conta_destino_2', 'nome_conta_destino_2',
                               'percentual_conta_destino_2', 'agencia_destino_2', 'conta_destino_2'])

    df.to_csv(settings.BASE_DIR + '/media/split.csv', encoding='utf-8-sig', index=False)
    file_path = os.path.join(settings.BASE_DIR + '/media/', 'split.csv')
    if os.path.exists(file_path):
        with open(file_path, 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/vnd.ms-excel")
            response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
            return response
    raise Http404


def planos(request):
    if request.GET.get('apagado') == 'true':

        apagado = True
    elif request.GET.get('apagado') == 'false':
        apagado = False
    else:
        apagado = request.GET.get('apagado')

    planos = Planos.objects.select_related('produto').all()
    return render(request, 'dashboard/planos.html', {'planos': planos, 'apagado': apagado, 'logged_user': request.user})


def insert_planos(request):
    if request.method != 'POST':
        form = PlanoForm()
        return render(request, 'dashboard/insert_planos.html', {'form': form, 'success': None, 'logged_user': request.user})
    else:
        pprint('entrou no else')
        form = PlanoForm(request.POST)
        if form.is_valid():
            plano = Planos()
            plano.nome = request.POST.get('nome_plano')
            plano.valor_contratacao = request.POST.get('valor_adesao_plano')
            plano.taxa_debito = request.POST.get('taxa_debito')
            plano.taxa_credito = request.POST.get('taxa_credito')
            plano.taxa_parcelado = request.POST.get('taxa_parcelado')
            plano.tipo_contratacao = request.POST.get('tipo_plano')
            plano.adquirencia = request.POST.get('adquirente_plano')
            produto = Produtos.objects.get(id=request.POST.get('prod'))
            plano.produto = produto
            plano.save()
            pprint('form valido')
            success = True
            msg = "Plano cadastrado com sucesso!"
        else:

            pprint('form invalido')
            success = False
            msg = form.errors

        return render(request, 'dashboard/insert_planos.html',
                      {'form': form, 'success': success, 'msg': msg, 'logged_user': request.user})


def edit_planos(request, id):
    plano = Planos.objects.get(id=id)
    if request.method != 'POST':
        form = PlanoForm(initial={
            'tipo_plano': plano.tipo_contratacao,
            'adquirente_plano': plano.adquirencia,
            'nome_plano': plano.nome,
            'valor_adesao_plano': plano.valor_contratacao,
            'taxa_debito': plano.taxa_debito,
            'taxa_credito': plano.taxa_credito,
            'taxa_parcelado': plano.taxa_parcelado,
            'prod': str(plano.produto.id)
        })
        return render(request, 'dashboard/edit_planos.html',
                      {'plano': plano, 'form': form, 'success': None, 'idplan': id, 'logged_user': request.user})
    else:
        pprint('entrou no else')
        form = PlanoForm(request.POST)
        if form.is_valid():
            plano.nome = request.POST.get('nome_plano')
            plano.valor_contratacao = request.POST.get('valor_adesao_plano')
            plano.taxa_debito = request.POST.get('taxa_debito')
            plano.taxa_credito = request.POST.get('taxa_credito')
            plano.taxa_parcelado = request.POST.get('taxa_parcelado')
            plano.tipo_contratacao = request.POST.get('tipo_plano')
            plano.adquirencia = request.POST.get('adquirente_plano')
            produto = Produtos.objects.get(id=request.POST.get('prod'))
            plano.produto = produto
            plano.save()
            pprint('form valido')
            success = True
            msg = "Plano cadastrado com sucesso!"
        else:
            success = False
            msg = form.errors

        return render(request, 'dashboard/edit_planos.html',
                      {'form': form, 'success': success, 'msg': msg, 'logged_user': request.user})


def delete_plano(request, id):
    try:
        plano = Planos.objects.get(id=id)
        plano.delete()
        return HttpResponseRedirect('/dashboard/planos?apagado=true')
    except:
        return HttpResponseRedirect('/dashboard/planos?apagado=false')


def produtos(request):
    produtos = Produtos.objects.all()

    if request.GET.get('apagado') == 'true':

        apagado = True
    elif request.GET.get('apagado') == 'false':
        apagado = False
    else:
        apagado = request.GET.get('apagado')

    return render(request, 'dashboard/produtos.html', {'produtos': produtos, 'apagado': apagado, 'logged_user': request.user})


def novo_produto(request):
    if request.method != 'POST':
        form = ProdutosForm()
        return render(request, 'dashboard/insert_produto.html', {'form': form, 'success': None, 'logged_user': request.user})
    else:
        form = ProdutosForm(request.POST)
        if form.is_valid():
            produto = Produtos()
            produto.descricao = request.POST.get('descricao')
            produto.identificador = request.POST.get('identificador')
            produto.gprs = True if request.POST.get('gprs') == 'on' else False
            produto.wifi = True if request.POST.get('wifi') == 'on' else False
            produto.save()
            success = True
            msg = "Produto cadastrado com sucesso!"
        else:
            success = False
            msg = form.errors

        return render(request, 'dashboard/insert_produto.html',
                      {'form': form, 'success': success, 'msg': msg, 'logged_user': request.user})


def edit_produto(request, id):
    produto = Produtos.objects.get(id=id)
    if request.method != 'POST':
        form = ProdutosForm(initial={
            'descricao': produto.descricao,
            'identificador': produto.identificador,
            'gprs': produto.gprs,
            'wifi': produto.wifi,
            'idprod': id
        })
        return render(request, 'dashboard/edit_produto.html',
                      {'produto': produto, 'form': form, 'success': None, 'idprod': id, 'logged_user': request.user})
    else:
        form = ProdutosForm(request.POST)

        if form.is_valid():
            produto.descricao = request.POST.get('descricao')
            produto.identificador = request.POST.get('identificador')
            produto.gprs = True if request.POST.get('gprs') == 'on' else False
            produto.wifi = True if request.POST.get('wifi') == 'on' else False
            produto.save()
            success = True
            msg = 'Produto alterado com sucesso!'
        else:
            success = False
            msg = form.errors

        return render(request, 'dashboard/edit_produto.html',
                      {'form': form, 'success': success, 'msg': msg, 'logged_user': request.user})


def delete_produto(request, id):
    try:
        produto = Produtos.objects.get(id=id)
        produto.delete()
        return HttpResponseRedirect('/dashboard/produtos?apagado=true')
    except:
        return HttpResponseRedirect('/dashboard/produtos?apagado=false')


def distribuidores(request):
    distribuidores = Distribuidores.objects.all()

    if request.GET.get('apagado') == 'true':
        apagado = True
    elif request.GET.get('apagado') == 'false':
        apagado = False
    else:
        apagado = request.GET.get('apagado')

    return render(request, 'dashboard/distribuidores.html', {'distribuidores': distribuidores, 'apagado': apagado, 'logged_user': request.user})


def novo_distribuidor(request):
    if request.method != 'POST':
        form = DistribuidorForm()
        return render(request, 'dashboard/insert_distribuidor.html', {'form': form, 'success': None, 'logged_user': request.user})
    else:
        form = DistribuidorForm(request.POST)
        if form.is_valid():
            distribuidor = Distribuidores()
            distribuidor.razao_social = request.POST.get('razao_social')
            distribuidor.cnpj = request.POST.get('cnpj')
            distribuidor.tipo_conta = request.POST.get('tipo')
            distribuidor.banco = request.POST.get('banco')
            distribuidor.digito_conta = request.POST.get('digito_conta')
            distribuidor.conta = request.POST.get('conta')
            distribuidor.agencia = request.POST.get('agencia')

            distribuidor.save()
            success = True
            msg = "Distribuidor cadastrado com sucesso!"
        else:
            success = False
            msg = form.errors

        return render(request, 'dashboard/insert_distribuidor.html',
                      {'form': form, 'success': success, 'msg': msg, 'logged_user': request.user})


def edit_distribuidor(request, id):
    distribuidor = Distribuidores.objects.get(id=id)
    if request.method != 'POST':
        form = DistribuidorForm(initial={
            'agencia': distribuidor.agencia,
            'conta': distribuidor.conta,
            'digito_conta': distribuidor.digito_conta,
            'cnpj': distribuidor.cnpj,
            'banco': distribuidor.banco,
            'tipo': distribuidor.tipo_conta,
            'razao_social': distribuidor.razao_social
        })
        return render(request, 'dashboard/edit_distribuidor.html',
                      {'distribuidor': distribuidor, 'form': form, 'success': None, 'iddistribuidor': id, 'logged_user': request.user})
    else:
        form = DistribuidorForm(request.POST)

        if form.is_valid():
            distribuidor.agencia = request.POST.get('agencia')
            distribuidor.conta = request.POST.get('conta')
            distribuidor.digito_conta = request.POST.get('digito_conta')
            distribuidor.cnpj = request.POST.get('cnpj')
            distribuidor.banco = request.POST.get('banco')
            distribuidor.tipo_conta = request.POST.get('tipo')
            distribuidor.razao_social = request.POST.get('razao_social')
            distribuidor.save()
            success = True
            msg = 'Distribuidor alterado com sucesso!'
        else:
            success = False
            msg = form.errors

        return render(request, 'dashboard/edit_distribuidor.html',
                      {'form': form, 'success': success, 'msg': msg, 'iddistribuidor': id, 'logged_user': request.user})
