from django.contrib import admin
from .models import Retorno4ward, \
    Lojista, Terminais, \
    LojistaTerminal, Cpf, Protesto, \
    PessoasRelacionadas, Empresas, DividaAtiva, EnderecoLojista, Profile
from pprint import pprint
# Register your models here.

class Retorno4wardAdmin(admin.ModelAdmin):
    list_display = ("id", "id_type", "id_number", "codigo_adquirente", "status", "lojista_id")

class LojistaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "documento",
        "lojista_id",
        "branch_id",
        "affiliation_key",
        "mensagem_idwall",
        "matriz_idwall",
        "numero_idwall",
        "resultado_idwall",
        "status_idwall",
        "pep_idwall",
        "data_cadastro",
        "data_update"
    )


class LojistaTerminalAdmin(admin.ModelAdmin):
    list_display = ("id", "lojista_id", "terminal_id")

    def lojista_id(self, instance):
        return instance.lojista.lojista_id

    def terminal_id(self, instance):
        return instance.terminais.terminal_id_logical

class TerminalAdmin(admin.ModelAdmin):
    list_display = ("id", "terminal_id_logical", "terminal_id_physical", "tecnology_code", "terminal_serial_number")

class CpfAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "numero",
        "data_de_nascimento",
        "nome",
        "renda",
        "pep",
        "situacao_imposto_de_renda",
        "cpf_situacao_cadastral",
        "cpf_data_de_inscricao",
        "cpf_digito_verificador",
        "cpf_anterior_1990",
        "ano_obito",
        "grafia",
        "lojista"
    )


class ProtestoAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "estado_protesto",
        "lojista"
    )


class PessoasRelacionadasAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "cpf",
        "nome",
        "tipo",
        "cargo",
        "cnpj",
        "lojista",
    )



class EmpresasAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "cnpj",
        "nome_empresarial",
        "tipo_relacionamento",
        "cargo",
        "lojista",
    )


class DividaAtivaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "nome",
        "valor_devido",
        "lojista",
    )


class EnderecoLojistaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "principal",
        "cidade",
        "estado",
        "numero",
        "cep",
        "complemento",
        "logradouro",
        "bairro",
        "tipo",
        "pessoas_endereco",
        "lojista",
        "data_cadastro",
        "data_update",
    )

admin.site.register(Retorno4ward, Retorno4wardAdmin)
admin.site.register(Lojista, LojistaAdmin)
admin.site.register(LojistaTerminal, LojistaTerminalAdmin)
admin.site.register(Terminais, TerminalAdmin)
admin.site.register(Cpf, CpfAdmin)
admin.site.register(Protesto, ProtestoAdmin)
admin.site.register(PessoasRelacionadas, PessoasRelacionadasAdmin)
admin.site.register(Empresas, EmpresasAdmin)
admin.site.register(DividaAtiva, DividaAtivaAdmin)
admin.site.register(EnderecoLojista, EnderecoLojistaAdmin)
admin.site.register(Profile)
