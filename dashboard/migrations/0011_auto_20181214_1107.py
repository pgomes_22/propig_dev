# Generated by Django 2.1.4 on 2018-12-14 13:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0010_remove_transacoes_card_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transacoes',
            name='value',
            field=models.FloatField(blank=True, default=0, null=True),
        ),
    ]
