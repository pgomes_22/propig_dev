# Generated by Django 2.1.5 on 2019-01-30 17:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0019_auto_20190130_1501'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distribuidores',
            name='agencia',
            field=models.CharField(default=None, max_length=10, null=True),
        ),
    ]
