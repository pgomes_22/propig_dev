# Generated by Django 2.1.3 on 2018-12-04 16:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0003_lojista_mensagem_integrador'),
    ]

    operations = [
        migrations.AddField(
            model_name='lojista',
            name='num_terminais',
            field=models.IntegerField(default=0),
        ),
    ]
