from django.db import models
import requests
from django.conf import settings
from pprint import pprint
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from requests.auth import HTTPDigestAuth
from django.template import loader, Context
from .search import LojistaIndex






class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

class Departamentos(models.Model):
    codigo_departamento = models.IntegerField(default=0, blank=True, null=True)
    descricao_departamento = models.CharField(max_length=250, default='', null=True)
    id_mcc = models.IntegerField(default=0, blank=True, null=True)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str_(self):
        return 'departamentos'

class Produtos(models.Model):
    descricao = models.CharField(max_length=250, default='', null=False)
    identificador = models.CharField(max_length=250, default='', null=False)
    gprs = models.BooleanField(default=False, blank=True, null=True)
    wifi = models.BooleanField(default=False, blank=True, null=True)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'produtos'

class Distribuidores(models.Model):
    agencia = models.CharField(max_length=10, default=None, null=True)
    conta = models.CharField(max_length=10, default=None, null=False)
    digito_conta = models.CharField(max_length=1, default=None, null=False)
    banco = models.IntegerField(default=0, blank=True, null=True)
    tipo_conta  = models.CharField(max_length=250, default='', null=False)
    cnpj = models.CharField(max_length=250, default='', null=False)
    razao_social = models.CharField(max_length=250, default='', null=False)
    porcentagem_favorecido = models.IntegerField(default=0, blank=True, null=True)
    porcentagem_cliente = models.IntegerField(default=0, blank=True, null=True)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'distribuidores'

class Planos(models.Model):
    nome = models.CharField(max_length=250, default='', null=False)
    tipo_contratacao = models.CharField(max_length=250, default='', null=False)
    valor_contratacao = models.FloatField(max_length=15, default=0.00)
    taxa_debito = models.FloatField(max_length=15, default=0.00)
    taxa_credito = models.FloatField(max_length=15, default=0.00)
    taxa_parcelado = models.FloatField(max_length=15, default=0.00)
    adquirencia = models.CharField(max_length=250, default='', null=False)
    produto = models.ForeignKey(Produtos, on_delete=models.CASCADE, related_name='planos_produtos')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'planos'

class Lojista(models.Model):
    lojista_id = models.IntegerField(default=0, blank=True, null=True)
    nome = models.CharField(max_length=200, default='')
    documento = models.CharField(max_length=20, default='', unique=True)
    branch_id = models.CharField(max_length=20, default='', blank=True, null=True)
    merchant_id = models.CharField(max_length=20, default='', blank=True, null=True)
    affiliation_key = models.CharField(max_length=250, default='', blank=True, null=True)
    mensagem_idwall = models.CharField(max_length=250, default='', blank=True, null=True)
    matriz_idwall = models.CharField(max_length=250, default='', blank=True, null=True)
    numero_idwall = models.CharField(max_length=300, default='', blank=True, null=True)
    resultado_idwall = models.CharField(max_length=250, default='', blank=True, null=True)
    status_idwall = models.CharField(max_length=20, default='', blank=True, null=True)
    pep_idwall = models.BooleanField(max_length=250, default=False, blank=True, null=True)
    fase = models.CharField(max_length=30, default="Em análise na Id Wall")
    com_protesto = models.BooleanField(max_length=250, default=False, blank=True, null=True)
    operadora = models.CharField(max_length=250, default='', blank=True, null=True)
    departamento = models.IntegerField(default=0, null=False)
    aprovacao_manual = models.BooleanField(default=False, blank=True, null=True)
    email_vendedor_responsavel = models.CharField(max_length=250, default='', blank=True, null=True)
    relatorio_a_partir_pj = models.BooleanField(max_length=250, default=False, blank=True, null=True)
    banco_favorecido =  models.IntegerField(default=0, null=True, blank=True)
    agencia_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    conta_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    digito_conta_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    tipo_conta_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    porcentagem_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    porcentagem_cliente = models.CharField(max_length=250, default='', blank=True, null=True)
    mensagem_integrador = models.CharField(max_length=250, default='', blank=True, null=True)
    num_terminais = models.IntegerField(default=0, null=False)
    observacao_liberacao_risco = models.CharField(max_length=1040, default='', blank=True, null=True)
    nome_razao_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    cnpj_favorecido = models.CharField(max_length=250, default='', blank=True, null=True)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)
    plano = models.ForeignKey(Planos, on_delete=models.CASCADE, related_name='planos_lojista', null=True, default=None)
    distribuidor = models.ForeignKey(Distribuidores, on_delete=models.CASCADE, related_name='distribuidores_lojista',
                                       null=True, default=None)


    def __str__(self):
        return 'lojista'

    def indexing(self):
        obj = LojistaIndex(
            meta={'id': self.id},
            nome=self.nome,
            documento=self.documento,
            branch_id=self.branch_id,
            merchant_id=self.merchant_id,
            affiliation_key=self.affiliation_key,
            mensagem_idwall=self.mensagem_idwall,
            matriz_idwall=self.matriz_idwall,
            numero_idwall=self.numero_idwall,
            resultado_idwall=self.resultado_idwall,
            status_idwall=self.status_idwall,
            fase=self.fase,
            com_protesto=self.com_protesto,
            operadora=self.operadora,
            departamento=self.departamento,
            aprovacao_manual=self.aprovacao_manual,
            email_vendedor_responsavel=self.email_vendedor_responsavel,
            relatorio_a_partir_pj=self.relatorio_a_partir_pj,
            banco_favorecido=self.banco_favorecido,
            agencia_favorecido=self.agencia_favorecido,
            conta_favorecido=self.conta_favorecido,
            digito_conta_favorecido=self.digito_conta_favorecido,
            tipo_conta_favorecido=self.tipo_conta_favorecido,
            porcentagem_favorecido=self.porcentagem_favorecido,
            porcentagem_cliente=self.porcentagem_cliente,
            mensagem_integrador=self.mensagem_integrador,
            num_terminais=self.num_terminais,
            observacao_liberacao_risco=self.observacao_liberacao_risco,
            nome_razao_favorecido=self.nome_razao_favorecido,
            cnpj_favorecido=self.cnpj_favorecido,
            data_cadastro=self.data_cadastro,
            data_update=self.data_update
        )
        obj.save()
        return obj.to_dict(include_meta=True)


class Cpf(models.Model):
    sexo = models.CharField(max_length=2, default='')
    numero = models.CharField(max_length=25, default='')
    data_de_nascimento = models.DateField()
    nome = models.CharField(max_length=300, default='')
    renda = models.CharField(max_length=250, default='')
    pep = models.BooleanField(max_length=250, default=False)
    situacao_imposto_de_renda = models.CharField(max_length=250, default='')
    cpf_situacao_cadastral = models.CharField(max_length=250, default='')
    cpf_data_de_inscricao = models.CharField(max_length=250, default='')
    cpf_digito_verificador = models.CharField(max_length=4, default='')
    cpf_anterior_1990 = models.CharField(max_length=1, default='')
    ano_obito = models.CharField(max_length=1, default='')
    grafia = models.CharField(max_length=300, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_cpf')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'cpf_lojista'


class Protesto(models.Model):
    estado_protesto = models.CharField(max_length=2, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_protesto')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'protestos_lojista'


class PessoasRelacionadas(models.Model):
    cpf = models.CharField(max_length=25, default='')
    nome = models.CharField(max_length=300, default='')
    tipo = models.CharField(max_length=20, default='')
    cargo = models.CharField(max_length=20, default='')
    cnpj = models.CharField(max_length=16, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_relacoes')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'relacoes_lojista'


class EnderecoLojista(models.Model):
    principal = models.BooleanField(max_length=250, default=False, blank=True, null=True)
    cidade = models.CharField(max_length=50, default='')
    estado = models.CharField(max_length=2, default='')
    numero = models.CharField(max_length=10, default='')
    cep = models.CharField(max_length=9, default='')
    complemento = models.CharField(max_length=250, default='')
    logradouro = models.CharField(max_length=250, default='')
    bairro = models.CharField(max_length=250, default='')
    tipo = models.CharField(max_length=250, default='')
    pessoas_endereco = models.CharField(max_length=3, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_endereco')
    utilizado_pedido = models.BooleanField(default=False)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'endereco_lojista'


class EmailsLojista(models.Model):
    endereco = models.CharField(max_length=250, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_email')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'email_lojista'


class DomicilioBancario(models.Model):
    banco = models.CharField(max_length=4, default='', null=False)
    agencia = models.IntegerField(default=0, null=False)
    conta = models.IntegerField(default=0, null=False)
    digito_conta = models.IntegerField(default=0, null=False)
    tipo_conta = models.CharField(max_length=10, default='', null=False)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_domicilio_bancario')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'domicilio_bancario_lojista'


class Taxas(models.Model):
    debito = models.FloatField(max_length=15, default=0.00)
    credito_a_vista = models.FloatField(max_length=15, default=0.00)
    credito_parcelado_emissor = models.FloatField(max_length=15, default=0.00)
    debito = models.FloatField(max_length=15, default=0.00)
    parcelado_loja_dois_seis = models.FloatField(max_length=15, default=0.00)
    parcelado_loja_sete_doze = models.FloatField(max_length=15, default=0.00)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_taxas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'taxas_lojista'


class Cessao(models.Model):
    maquina_cessao = models.CharField(max_length=10, default='', null=False)
    foto_documento = models.CharField(max_length=400, default='', null=False)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_cessao')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'cessao_lojista'


class Pagamento(models.Model):
    tipo_pagamento = models.CharField(max_length=25, default='', null=False)
    parcelas = models.CharField(max_length=10, default='', null=True)
    nsu = models.CharField(max_length=250, default='', null=True)
    data_pagamento = models.DateField(null=True)
    pagamento_valido = models.BooleanField(default=False, null=False)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_pagamento')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'pagamento_lojista'


class Pedido(models.Model):
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_pedido')
    numero_pedido = models.IntegerField(null=False, default=0)
    status_pedido = models.CharField(max_length=250, default='', null=False)
    pagamento = models.ForeignKey(Pagamento, on_delete=models.CASCADE, related_name='pagamento_pedido', null=True)
    comercial = models.ForeignKey(User, on_delete=models.CASCADE, related_name='lojista_pagamento', default=None,
                                  null=True)
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'pedido'


class Empresas(models.Model):
    cnpj = models.CharField(max_length=16, default='')
    nome_empresarial = models.CharField(max_length=150, default='')
    tipo_relacionamento = models.CharField(max_length=20, default='')
    cargo = models.CharField(max_length=20, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_empresas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'empresas_lojista'


class DividaAtiva(models.Model):
    nome = models.CharField(max_length=150, default='')
    valor_devido = models.CharField(max_length=20, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_dividas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'dividas_lojista'


class ResumoConsultas(models.Model):
    nome_matriz = models.CharField(max_length=150, default='')
    status_protocolo = models.CharField(max_length=20, default='')
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_consultas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'consultas_lojista'


class NomeFonteConsulta(models.Model):
    nome = models.CharField(max_length=150, default='')
    resumo_consulta = models.ForeignKey(ResumoConsultas, on_delete=models.CASCADE, related_name='resumo_consultas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'consultas_resumo'


class TentativasResumoConsulta(models.Model):
    duracao_tentativa = models.FloatField(max_length=15, default=0.000)
    hora_fim_tentativa = models.DateTimeField(null=True)
    hora_inicio_tentativa = models.DateTimeField(null=True)
    msg_erro_tentativa = models.CharField(max_length=250, default='')
    fonte = models.ForeignKey(NomeFonteConsulta, on_delete=models.CASCADE, related_name='fonte_consultas')
    status_fonte = models.CharField(max_length=10, default='')
    status_tentativa = models.CharField(max_length=25, default='')
    tipo_erro_tentativa = models.CharField(max_length=250, default='', null=True)

    def __str__(self):
        return 'consultas_fonte'


class CertidoesNegativasItens(models.Model):
    emitido_em = models.DateTimeField(null=True)
    fonte = models.CharField(max_length=250, default='', null=True)
    protocolo = models.CharField(max_length=250, default='', null=True)
    url = models.CharField(max_length=250, default='', null=True)
    nada_consta = models.CharField(max_length=250, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_certidoes_negativas')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'certidoes_negativas_lojista'


class Cnpj(models.Model):
    cnpj = models.CharField(max_length=16, default='', null=True)
    nome_fantasia = models.CharField(max_length=150, default='', null=True)
    nome_empresarial = models.CharField(max_length=150, default='', null=True)
    data_abertura = models.DateField(null=True)
    fonte = models.CharField(max_length=50, default='', null=True)
    atividade_principal = models.CharField(max_length=300, default='', null=True)
    faturamento_presumido = models.CharField(max_length=300, default='', null=True)
    numero_funcionarios = models.CharField(max_length=50, default='', null=True)
    numero_funcionarios_filiais = models.CharField(max_length=50, default='')
    regime_tributario = models.CharField(max_length=16, default='', null=True)
    irs_status = models.CharField(max_length=16, default='', null=True)
    capital_social = models.CharField(max_length=16, default='', null=True)
    data_situacao_cadastral = models.DateField(null=True)
    data_situacao_especial = models.DateField(null=True)
    email = models.CharField(max_length=250, default='', null=True)
    ente_federativo_responsavel = models.CharField(max_length=300, default='', null=True)
    motivo_situacao_cadastral = models.CharField(max_length=250, default='', null=True)
    natureza_juridica = models.CharField(max_length=200, default='', null=True)
    numero = models.CharField(max_length=16, default='', null=True)
    situacao_cadastral = models.CharField(max_length=16, default='', null=True)
    situacao_especial = models.CharField(max_length=50, default='', null=True)
    telefone = models.CharField(max_length=150, default='', null=True)
    tipo = models.CharField(max_length=150, default='', null=True)
    localizacao_bairro = models.CharField(max_length=250, default='', null=True)
    localizacao_cep = models.CharField(max_length=10, default='', null=True)
    localizacao_cidade = models.CharField(max_length=50, default='', null=True)
    localizacao_complemento = models.CharField(max_length=150, default='', null=True)
    localizacao_estado = models.CharField(max_length=50, default='', null=True)
    localizacao_logradouro = models.CharField(max_length=150, default='', null=True)
    localizacao_numero = models.CharField(max_length=6, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_cnpj')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'cnpj_lojista'


class AtividadesSecundaria(models.Model):
    atividade = models.CharField(max_length=250, default='', null=True)
    cnpj = models.ForeignKey(Cnpj, on_delete=models.CASCADE, related_name='cpnj_atividades_secundarias')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'atividades_secundarias_cpnj'


class GrafiasCnpj(models.Model):
    grafia = models.CharField(max_length=250, default='')
    cnpj = models.ForeignKey(Cnpj, on_delete=models.CASCADE, related_name='cpnj_grafias')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'grafias_cpnj'


class QsaCnpj(models.Model):
    nome = models.CharField(max_length=250, default='')
    nome_representante = models.CharField(max_length=250, default='', null=True)
    pais_origem = models.CharField(max_length=250, default='', null=True)
    qualificacao = models.CharField(max_length=250, default='')
    qualificacao_representante_legal = models.CharField(max_length=250, default='', null=True)
    cnpj = models.ForeignKey(Cnpj, on_delete=models.CASCADE, related_name='cpnj_qsa')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'qsa_cpnj'


class SintegraItem(models.Model):
    cnpj = models.CharField(max_length=20, default='', null=True)
    inscricao_estadual = models.CharField(max_length=20, default='', null=True)
    razao_social = models.CharField(max_length=250, default='', null=True)
    cnae_principal = models.CharField(max_length=250, default='', null=True)
    situacao_cadastral_vigente = models.CharField(max_length=20, default='', null=True)
    data_situacao_cadastral = models.DateField(null=True)
    regime_apuracao = models.CharField(max_length=250, default='', null=True)
    data_credenciamento_nfe = models.CharField(max_length=250, default='', null=True)
    indicador_obrigatoriedade_nfe = models.CharField(max_length=250, default='', null=True)
    data_obrigatoriedade_nfe = models.CharField(max_length=250, default='', null=True)
    protocolo = models.CharField(max_length=250, default='', null=True)
    tipo = models.CharField(max_length=250, default='', null=True)
    uf = models.CharField(max_length=2, default='')
    cad_icms = models.CharField(max_length=250, default='', null=True)
    cae = models.CharField(max_length=250, default='', null=True)

    nome_fantasia = models.CharField(max_length=250, default='', null=True)
    data_inicio_atividade = models.CharField(max_length=250, default='', null=True)
    natureza_juridica = models.CharField(max_length=250, default='', null=True)
    natureza_estabelecimento = models.CharField(max_length=250, default='', null=True)
    data_abertura = models.CharField(max_length=250, default='', null=True)
    data_baixa = models.CharField(max_length=250, default='', null=True)
    delegacia_fazendaria = models.CharField(max_length=250, default='', null=True)
    enquadramento_empresa = models.CharField(max_length=250, default='', null=True)
    observacao = models.CharField(max_length=250, default='', null=True)
    data_inscricao = models.CharField(max_length=250, default='', null=True)
    ped = models.CharField(max_length=250, default='', null=True)
    data_inicio_ped = models.CharField(max_length=250, default='', null=True)
    ultrapassou_limite_estadual = models.CharField(max_length=250, default='', null=True)
    data_inicio_simples_nacional = models.CharField(max_length=250, default='', null=True)
    condicao = models.CharField(max_length=250, default='', null=True)
    email = models.CharField(max_length=250, default='', null=True)
    indicador_obrigatoriedade_efd = models.CharField(max_length=250, default='', null=True)
    data_obrigatoriedade_efd = models.CharField(max_length=250, default='', null=True)
    opcao_simples = models.CharField(max_length=250, default='', null=True)
    indicador_obrigatoriedade_cte = models.CharField(max_length=250, default='', null=True)
    data_obrigatoriedade_cte = models.CharField(max_length=250, default='', null=True)
    situacao_sintegra = models.CharField(max_length=250, default='', null=True)
    tipo_unidade_auxiliar = models.CharField(max_length=250, default='', null=True)
    regime_pagamento = models.CharField(max_length=250, default='', null=True)
    situacao_contribuinte = models.CharField(max_length=250, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_sintegra_itens')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'sintegra_itens'


class SintegraItemAtividadeSecundaria(models.Model):
    codigo = models.CharField(max_length=50, default='', null=True)
    descricao = models.CharField(max_length=150, default='', null=True)
    sintegra_item = models.ForeignKey(SintegraItem, on_delete=models.CASCADE,
                                      related_name='sintegra_itens_atividade_secundaria')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'atividade_secundaria_sintegra_itens'


class Telefones(models.Model):
    fone_ddd = models.CharField(max_length=2, default='', null=True)
    fone_numero = models.CharField(max_length=10, default='', null=True)
    principal = models.BooleanField(default=False)
    tipo_assinante = models.CharField(max_length=20, default='', null=True)
    pessoas_com_o_mesmo_telefone = models.CharField(max_length=5, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_telefones')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'telefones'


class ParticipacaoEmpresasItens(models.Model):
    cnpj = models.CharField(max_length=20, default='', null=True)
    nome_empresarial = models.CharField(max_length=150, default='', null=True)
    tipo_relacionamento = models.CharField(max_length=200, default='', null=True)
    cargo = models.CharField(max_length=150, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_participacoes')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'participacoes'


class Retorno4ward(models.Model):
    id_type = models.IntegerField(default=0)
    id_number = models.CharField(max_length=20, default='')
    codigo_adquirente = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    lojista_id = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'retorno_4ward'


class Terminais(models.Model):
    terminal_id_logical = models.CharField(max_length=50, default='')
    terminal_id_physical = models.CharField(max_length=50, default='')
    tecnology_code = models.IntegerField(default=0)
    terminal_serial_number = models.CharField(max_length=50, default='')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'terminais'


class LojistaTerminal(models.Model):
    lojista_id = models.ForeignKey(Lojista, on_delete=models.CASCADE, null=False, default=0,
                                   related_name='lojista_terminal')
    terminal_id = models.ForeignKey(Terminais, on_delete=models.CASCADE, null=False, default=0,
                                    related_name='terminal_lojista')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'lojista_terminal'


class Validacao(models.Model):
    mensagem = models.CharField(max_length=250, default='', null=True)
    nome = models.CharField(max_length=250, default='', null=True)
    resultado = models.CharField(max_length=250, default='', null=True)
    status = models.CharField(max_length=250, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_validacao')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'validacao_lojista'


class ItemValidacoes(models.Model):
    regra = models.CharField(max_length=250, default='', null=True)
    nome = models.CharField(max_length=250, default='', null=True)
    descricao = models.CharField(max_length=250, default='', null=True)
    resultado = models.CharField(max_length=250, default='', null=True)
    mensagem = models.CharField(max_length=250, default='', null=True)
    validacao = models.ForeignKey(Validacao, on_delete=models.CASCADE, related_name='validacao_item')
    data_cadastro = models.DateTimeField(null=True)
    data_update = models.DateTimeField(null=True)

    def __str__(self):
        return 'item_validacao'


class Responsavel(models.Model):
    nome = models.CharField(max_length=250, default='', null=True)
    documento = models.CharField(max_length=20, default='', null=True)
    tipo = models.CharField(max_length=40, default='', null=True)
    data_nascimento = models.CharField(max_length=40, default='', null=True)
    sexo = models.CharField(max_length=1, default='', null=True)
    ddd = models.CharField(max_length=2, default='', null=True)
    telefone = models.CharField(max_length=20, default='', null=True)
    email = models.CharField(max_length=250, default='', null=True)
    nacionalidade = models.CharField(max_length=20, default='', null=True)
    lojista = models.ForeignKey(Lojista, on_delete=models.CASCADE, related_name='lojista_responsavel')

    def __str__(self):
        return 'responsavel'


class Transacoes(models.Model):
    nsu = models.IntegerField(default=0, blank=True, null=True)
    acquirer_nsu = models.CharField(max_length=40, default='', null=True)
    value = models.FloatField(default=0, blank=True, null=True)
    status = models.IntegerField(default=0, blank=True, null=True)
    parcels = models.IntegerField(default=0, blank=True, null=True)
    brand = models.CharField(max_length=40, default='', null=True)
    start_date = models.DateTimeField(null=True)
    finish_date = models.DateTimeField(null=True)
    confirmation_date = models.DateTimeField(null=True)
    payment_date = models.DateTimeField(null=True)
    response_code = models.CharField(max_length=10, default='', null=True)
    response_message = models.CharField(max_length=250, default='', null=True)
    authorization_number = models.CharField(max_length=40, default='', null=True)
    terminal = models.CharField(max_length=40, default='', null=True)
    tef_terminal = models.CharField(max_length=40, default='', null=True)
    terminal_serial_number = models.CharField(max_length=40, default='', null=True)
    terminal_manufacturer = models.CharField(max_length=40, default='', null=True)
    terminal_model = models.CharField(max_length=40, default='', null=True)
    terminal_type = models.CharField(max_length=40, default='', null=True)
    acquirer = models.CharField(max_length=40, default='', null=True)
    merchant = models.CharField(max_length=40, default='', null=True)
    tef_merchant = models.CharField(max_length=40, default='', null=True)
    merchant_category_code = models.CharField(max_length=40, default='', null=True)
    merchant_national_type = models.CharField(max_length=40, default='', null=True)
    merchant_national_id = models.CharField(max_length=40, default='', null=True)
    product_name = models.CharField(max_length=40, default='', null=True)
    product_id = models.CharField(max_length=40, default='', null=True)
    card_input_method = models.CharField(max_length=40, default='', null=True)
    requested_password = models.CharField(max_length=40, default='', null=True)
    fallback = models.CharField(max_length=40, default='', null=True)
    origin = models.CharField(max_length=40, default='', null=True)
    authorization_time = models.IntegerField(default=0, blank=True, null=True)
    client_version = models.CharField(max_length=250, default='', null=True)
    server_version = models.CharField(max_length=250, default='', null=True)

    def __str__(self):
        return 'transacoes'


class Departamentos(models.Model):
    codigo_departamento = models.IntegerField(default=0, blank=True, null=True)
    descricao_departamento = models.CharField(max_length=250, default='', null=True)
    id_mcc = models.IntegerField(default=0, blank=True, null=True)

    def __str__(self):
        return 'departamentos'




class Slack(models.Model):
    @classmethod
    def alerta_risco(self, payload):
        headers = {
            'Content-Type': "application/json",
        }
        requests.request("POST", settings.SLACK_WEBHOOK_RISC_ENDPOIT, data=json.dumps(payload),
                         headers=headers)


class Skipper(models.Model):
    @classmethod
    def credenciamento(self, payload):
        headers = {
            'Content-Type': "application/json"
        }
        response = requests.request("POST", settings.SKIPPER_ENDPOINT + "accredit", data=json.dumps(payload),
                                    auth=HTTPDigestAuth(settings.SKIPPER_USER, settings.SKIPPER_PASSWORD),
                                    headers=headers)
        return response.json()


class IdWall(models.Model):

    @classmethod
    def consulta_validacoes(self, numero):
        headers = {
            'Authorization': settings.IDWALL_TOKEN,
            'Content-Type': "application/json"
        }
        endpoint = settings.IDWALL_ENDPOINT + "/" + numero + "/validacoes"
        response = requests.request("GET", endpoint, headers=headers)
        return response.json()

    @classmethod
    def consulta_documento(self, tipo_consulta, numero):
        if tipo_consulta == 'CPF':
            metodo = 'lucree_QSA_pf'
            variavel = 'cpf_numero'
        elif tipo_consulta == 'CNPJ':
            metodo = 'lucree_QSA_pj'
            variavel = 'cnpj_numero'

        dict = {
            "matriz": metodo,
            "opcoes": {},
            "parametros": {
                variavel: numero
            }
        }
        headers = {
            'Authorization': settings.IDWALL_TOKEN,
            'Content-Type': "application/json"
        }
        response = requests.request("POST", settings.IDWALL_ENDPOINT, data=json.dumps(dict), headers=headers)
        return response.json()

    @classmethod
    def consulta_resultado(self, numero):
        headers = {
            'Authorization': settings.IDWALL_TOKEN,
            'Content-Type': "application/json"
        }
        endpoint = settings.IDWALL_ENDPOINT + "/" + numero + "/dados"
        response = requests.request("GET", endpoint, headers=headers)
        return response.json()

    @classmethod
    def relatorio_consulta(self, numero):
        headers = {
            'Authorization': settings.IDWALL_TOKEN,
            'Content-Type': "application/json"
        }
        endpoint = settings.IDWALL_ENDPOINT + "/" + numero + "/consultas"
        response = requests.request("GET", endpoint, headers=headers)
        return response.json()


class Skipper(models.Model):
    @classmethod
    def credenciamento(self, dados_credenciamento):
        headers = {
            'Content-Type': "application/json"
        }
        endpoint = settings.SKIPPER_ENDPOINT + "accredit"
        response = requests.request("POST", endpoint, data=json.dumps(dados_credenciamento), headers=headers,
                                    auth=(settings.SKIPPER_USER, settings.SKIPPER_PASSWORD))
        return response.json()

    @classmethod
    def consulta_credenciamento(self, dados_credenciamento):
        headers = {
            'Content-Type': "application/json"
        }
        endpoint = settings.SKIPPER_ENDPOINT + "consult"

        response = requests.request("POST", endpoint, data=json.dumps(dados_credenciamento), headers=headers,
                                    auth=(settings.SKIPPER_USER, settings.SKIPPER_PASSWORD))
        return response.json()


class PayStore(models.Model):
    @classmethod
    def getTransaction(self, query):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "payments?" + query
        pprint(end_point)
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def allMerchants(self):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "merchant"
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def filterMechant(self, query):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "merchant?" + query
        pprint(end_point)
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def saveMerchant(self, merchant):
        end_point = settings.PAYSTORE_END_POINT + "merchant"  # falta o endpoint
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        pprint('SAVE MERCHANT')
        pprint(merchant)
        results = requests.post(end_point, headers=headers, data=json.dumps(merchant))
        return results.json()

    @classmethod
    def tefConfigMerchant(self, merchant):
        end_point = settings.PAYSTORE_END_POINT + "merchant/tefConfig"  # falta o endpoint
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        pprint('TEF CONFIG MERCHANT')
        pprint(merchant)
        requests.post(end_point, headers=headers, data=json.dumps(merchant))

    @classmethod
    def tefConfigTerminal(self, merchant):
        end_point = settings.PAYSTORE_END_POINT + "terminal/tefConfig"  # falta o endpoint
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        pprint('TEF CONFIG TERMINAL')
        pprint(merchant)
        requests.post(end_point, headers=headers, data=json.dumps(merchant))


    @classmethod
    def allCategories(self):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "merchant/categories"
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def filterCategories(self, query):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "merchant/categories?" + query
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    """ def recursivityCategoriesSearch(sel) """

    @classmethod
    def allTerminals(self):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "terminal"
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def filterTerminals(self, query):
        results = {}
        end_point = settings.PAYSTORE_END_POINT + "terminal?" + query
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        results = requests.request("GET", end_point, headers=headers)
        return results.json()

    @classmethod
    def saveTerminal(self, terminal):
        end_point = settings.PAYSTORE_END_POINT + "terminal"  # falta o endpoint
        headers = {
            'Content-Type': "application/json",
            'Authorization': "Bearer " + settings.PAYSTORE_TOKEN,
        }
        pprint(terminal)
        results = requests.post(end_point, headers=headers, data=json.dumps(terminal))
        return results.json()

    @classmethod
    def py_mail(self, SUBJECT, BODY, TO, FROM):
        # Create message container - the correct MIME type is multipart/alternative here!
        MESSAGE = MIMEMultipart('alternative')
        MESSAGE['subject'] = SUBJECT
        MESSAGE['To'] = TO
        MESSAGE['From'] = FROM

        MESSAGE.preamble = """
    Your mail reader does not support the report format."""

        # Record the MIME type text/html.
        HTML_BODY = MIMEText(BODY, 'html')

        # Attach parts into message container.
        # According to RFC 2046, the last part of a multipart message, in this case
        # the HTML message, is best and preferred.
        MESSAGE.attach(HTML_BODY)

        # The actual sending of the e-mail
        server = smtplib.SMTP('email-smtp.us-west-2.amazonaws.com:587')

        # Print debugging output when testing
        if __name__ == "__main__":
            server.set_debuglevel(1)

        # Credentials (if needed) for sending the mail
        login = 'AKIAIFOZGG6XVC3XSB7Q'
        password = "AqJf/MBKXpNqj+ON1H4BkBLHNqXK0X5AEYTJNAbYke0b"

        server.starttls()
        server.login(login, password)
        server.sendmail(FROM, [TO], MESSAGE.as_string())
        server.quit()

    @classmethod
    def mountEmailAtivacao(self, token):
        template = loader.get_template('dashboard/mail/ativacao_mail.html')
        content = template.render({
            'token': token,
        })

        return content

    @classmethod
    def mountEmailRisco(self, documento=None, nome_cliente=None):
        template = loader.get_template('dashboard/mail/alerta_risco_email.html')
        content = template.render({
            'documento': documento,
            'nome_razao_social': nome_cliente,
            'rota': 'rotaaaa'
        })

        return content

    @classmethod
    def mountEmailRiscoVendedor(self, documento=None, nome_cliente=None):
        template = loader.get_template('dashboard/mail/alerta_alto_risco_vendedor.html')
        content = template.render({
            'documento': documento,
            'nome_razao_social': nome_cliente
        })

        return content

    @classmethod
    def mountEmailRiscoCliente(self, documento=None, nome_cliente=None):
        template = loader.get_template('dashboard/mail/alerta_alto_risco_cliente.html')
        content = template.render({
            'documento': documento,
            'nome_razao_social': nome_cliente
        })

        return content


class Registration(models.Model):
    @classmethod
    def mountEmailNovaSenha(self, senha=None, username=None):
        template = loader.get_template('dashboard/mail/renovacao_senha.html')
        content = template.render({
            'senha': senha,
            'username': username
        })

        return content