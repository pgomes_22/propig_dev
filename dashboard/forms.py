from django import forms
from dashboard.models import PayStore, Departamentos, Produtos, Distribuidores
from pprint import pprint
from django.core.validators import EMPTY_VALUES
from django.forms import ModelForm




class LoginForm(forms.Form):
    next_page = forms.CharField(max_length=455, required=False, widget=forms.HiddenInput())
    username = forms.CharField(max_length=200, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Login'}))
    senha = forms.CharField(max_length=20, required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'type': 'password', 'placeholder': 'Senha'}))


class MerchantForm(forms.Form):
    lista = []
    departamentos = Departamentos.objects.all()

    for departamento in departamentos:
        tupla = (departamento.codigo_departamento, departamento.descricao_departamento)
        lista.append(tupla)

    # PERFIL
    categories_form = forms.CharField(widget=forms.Select(
        choices=lista, attrs={'class': 'form-control'}))

    tipo = forms.CharField(widget=forms.Select(
        choices=[("01", "Física"), ("02", "Jurídica")], attrs={'class': 'form-control'}))
    # DADOS COMERCIAIS
    cpf = forms.CharField(max_length=11, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CPF'}))
    nome_fantasia = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NOME FANTASIA'}))
    email = forms.CharField(max_length=500, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'EMAIL', 'type': 'email'}))
    telefone = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TELEFONE'}))
    pagina_web = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PÁGINA WEB'}))
    inscricao_municipal = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'INSCRIÇÃO MUNICIPAL'}))

    # ENDEREÇO
    cep = forms.CharField(max_length=9, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CEP'}))
    uf = forms.CharField(max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'UF'}))
    cidade = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CIDADE'}))
    endereco = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'ENDEREÇO'}))
    bairro = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'BAIRRO'}))
    numero_endereco = forms.CharField(max_length=8, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NÚMERO'}))
    complemento = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPLEMENTO'}))

    # TITULAR DA CONTA

    nome = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NOME COMPLETO'}))
    email_titular = forms.CharField(max_length=500, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'EMAIL'}))
    telefone_titular = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TELEFONE'}))
    whatsapp = forms.CharField(max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'WHATSAPP'}))


bancos = [
    ('', 'ESCOLHA UM BANCO'),
    ('001', '001 - BANCO DO BRASIL'), ('003', '003 - BANCO DA AMAZÔNIA'),
    ('004', '004 - BANCO DO NORDESTE DO BRASIL'), ('021', '021 - BANESTES - BANCO DO ESTADO DO ESPIRITO SANTO'),
    ('025', '025 - BANCO ALFA'), ('033', '033 - BANCO SANTANDER'),
    ('037', '037 - BANPARÁ'), ('041', '041 - BANCO DO ESTADO DO RIO GRANDE DO SUL'),
    ('047', '047 - BANESE - BANCO DO ESTADO DE SERGIPE'),
    ('070', '070 - BRB - BANCO DE BRASILIA'),
    ('077', '077 - BANCO INTERMEDIUM'), ('082', '082 - BANCO TOPÁZIO'),
    ('084', '084 - BANCO UNIPRIME NORTE DO PARANÁ'),
    ('085', '085 - COOPERATIVA CENTRAL DE CRÉDITO - AILOS'),
    ('089', '089 - CREDISAN'), ('104', '104 - CAIXA ECONOMICA FEDERAL'),
    ('107', '107 - BANCO BOCOM BBM S.A.'),
    ('130', '130 - CARUANA S/A SOCIEDADE DE CRÉDITO, FINANCIAMENTO E INVESTIMENTO'),
    ('136', '136 - BANCO UNICRED DO BRASIL'),
    ('213', '213 - BANCO ARBI'), ('224', '224 - BANCO FIBRA'), ('237', '237 - BANCO BRADESCO'),
    ('243', '243 - BANCO MÁXIMA S.A.'), ('246', '246 - BANCO ABC BRASIL'),
    ('341', '341 - BANCO ITAÚ'), ('376', '376 - BANCO J.P. MORGAN S.A'),
    ('389', '389 - BANCO MERCANTIL DO BRASIL'), ('422', '422 - BANCO SAFRA'),
    ('604', '604 - BANCO INDUSTRIAL DO BRASIL'),
    ('611', '611 - BANCO PAULISTA'), ('612', '612 - BANCO GUANABARA'),
    ('630', '630 - BANCO INTERCAP S.A.'), ('634', '634 - BANCO TRIANGULO'),
    ('637', '637 - BANCO SOFISA'), ('643', '643 - BANCO PINE S.A.'),
    ('655', '655 - BANCO VOTORANTIM'), ('707', '707 - DAYCOVAL'),
    ('741', '741 - BANCO RIBEIRAO PRETO'), ('743', '743 - BANCO SEMEAR'),
    ('745', '745 - BANCO CITIBANK'), ('748', '748 - BANCO COOPERATIVO SICREDI'),
    ('755', '755 - BANCO MERRILL LYNCH'), ('756', '756 - BANCO COOPERATIVO DO BRASIL'),
]

tipo_conta = [
    ("CORRENTE", "CONTA CORRENTE"),
    ("POUPANÇA", "CONTA POUPANÇA")
]

parcelas = [
    ("1x", "1x A vista"),
    ("2x", "2x"),
    ("3x", "4x"),
    ("4x", "4x"),
    ("5x", "5x"),
    ("6x", "6x"),
    ("7x", "7x"),
    ("8x", "8x"),
    ("9x", "9x"),
    ("10x", "10x"),
    ("11x", "11x"),
    ("11x", "11x"),
]


class RiscForm(forms.Form):
    avaliacao = forms.CharField(widget=forms.HiddenInput())
    observacao = forms.CharField(max_length=1040, widget=forms.Textarea(
        attrs={'class': 'form-control', 'placeholder': 'Observações ...'}))


class EnderecoForm(forms.Form):
    tipo_doc = forms.CharField(widget=forms.Select(
        choices=[("CPF", "CPF"), ("CNPJ", "CNPJ")], attrs={'class': 'form-control'}))
    numero = forms.CharField(max_length=14, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Somente os números'}))

    cep = forms.CharField(max_length=9, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CEP'}))
    uf = forms.CharField(max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'UF'}))
    cidade = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CIDADE'}))
    endereco = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'ENDEREÇO'}))
    bairro = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'BAIRRO'}))
    numero_endereco = forms.CharField(max_length=8, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NÚMERO'}))
    complemento = forms.CharField(required=False, max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPLEMENTO'}))

    tipo_endereco = forms.CharField(widget=forms.Select(
        choices=[("1", "PRINCIPAL"), ("2", "CORRESPONDÊNCIA"), ("3", "INSTALAÇÃO")], attrs={'class': 'form-control'}))


class TaxaForm(forms.Form):
    plano = forms.CharField(widget=forms.HiddenInput())


class ResponsavelForm(forms.Form):
    nome_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NOME DO CONTATO RESPONSÁVEL'}))

    documento_responsavel = forms.CharField(required=True, max_length=11, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CPF RESPONSÁVEL'}))

    tipo_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[(9, "SÓCIO"), (10, "CONTATO")], attrs={'class': 'form-control'}))

    nascimento_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative datepicker', 'placeholder': 'DATA DE NASCIMENTO'}))

    sexo_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[("M", "MASCULINO"), ("F", "FEMININO")], attrs={'class': 'form-control'}))

    ddd_telefone_responsavel = forms.CharField(required=True, max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DDD TELEFONE'}))

    telefone_responsavel = forms.CharField(required=True, max_length=9, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TELEFONE'}))

    email_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'EMAIL'}))

    nacionalidade_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[("Brasileira", "Brasileira"), ("Estrangeira", "Estrangeira")], attrs={'class': 'form-control'}))


class ConsultaDocForm(forms.Form):
    def clean(self):
        split = self.cleaned_data.get('split')
        if split == 'sim':
            # validate the activity name
            conta = self.cleaned_data.get('conta')
            agencia = self.cleaned_data.get('agencia')
            digito_conta = self.cleaned_data.get('digito_conta')
            banco = self.cleaned_data.get('banco')
            tipo_conta = self.cleaned_data.get('tipo_conta')
            conta_favorecido = self.cleaned_data.get('conta_favorecido')
            agencia_favorecido = self.cleaned_data.get('agencia_favorecido')
            digito_conta_favorecido = self.cleaned_data.get('digito_conta_favorecido')
            banco_favorecido = self.cleaned_data.get('banco_favorecido')
            tipo_conta_favorecido = self.cleaned_data.get('tipo_conta_favorecido')
            plano = self.cleaned_data.get('plano')

            if agencia in EMPTY_VALUES:
                self._errors['agencia'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo AGÊNCIA  deve ser preenchido'])
            if conta in EMPTY_VALUES:
                self._errors['conta'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo Nº DA CONTA deve ser preenchido'])
            if digito_conta in EMPTY_VALUES:
                self._errors['digito_conta'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo DÍGITO DA CONTA deve ser preenchido'])
            if banco in EMPTY_VALUES:
                self._errors['banco'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo BANCO deve ser preenchido'])
            if tipo_conta in EMPTY_VALUES:
                self._errors['tipo_conta'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo TIPO DE CONTA deve ser preenchido'])

            if conta_favorecido in EMPTY_VALUES:
                self._errors['conta_favorecido'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo CONTA do FAVORECIDO deve ser preenchido'])

            if agencia_favorecido in EMPTY_VALUES:
                self._errors['agencia_favorecido'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo AGÊNCIA DA CONTA do FAVORECIDO deve ser preenchido'])

            if digito_conta_favorecido in EMPTY_VALUES:
                self._errors['digito_conta_favorecido'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo DÍGITO DA CONTA do FAVORECIDO deve ser preenchido'])

            if banco_favorecido in EMPTY_VALUES:
                self._errors['banco_favorecido'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo BANCO DA CONTA do FAVORECIDO deve ser preenchido'])

            if tipo_conta_favorecido in EMPTY_VALUES:
                self._errors['tipo_conta_favorecido'] = self.error_class([
                    'Se o cliente optar pelo "SPLIT DE PAGAMENTO", o campo TIPO DA CONTA do FAVORECIDO deve ser preenchido'])

            if plano in EMPTY_VALUES:
                self._errors['plano'] = self.error_class([
                    'Lembre-se de escolher o PLANO DE TARIFAS para o seu cliente'])

                pprint(self._errors)
        return self.cleaned_data

    listadist = []
    distribuidores = Distribuidores.objects.all()
    for dist in distribuidores:
        tupla = (dist.id, dist.razao_social)
        listadist.append(tupla)

    distribuidores = forms.CharField(required=False, widget=forms.Select(
        choices=listadist, attrs={'class': 'form-control'}))

    tipo_doc = forms.CharField(widget=forms.Select(
        choices=[("CPF", "CPF"), ("CNPJ", "CNPJ")], attrs={'class': 'form-control'}))
    numero = forms.CharField(max_length=14, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Somente os números'}))

    cep = forms.CharField(max_length=9, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CEP'}))
    uf = forms.CharField(max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'UF'}))
    cidade = forms.CharField(max_length=30, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CIDADE'}))
    endereco = forms.CharField(max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'ENDEREÇO'}))
    bairro = forms.CharField(max_length=50, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'BAIRRO'}))
    numero_endereco = forms.CharField(max_length=8, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NÚMERO'}))
    complemento = forms.CharField(required=False, max_length=200, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPLEMENTO'}))

    tipo_endereco = forms.CharField(widget=forms.Select(
        choices=[("1", "PRINCIPAL"), ("2", "CORRESPONDÊNCIA"), ("3", "INSTALAÇÃO")], attrs={'class': 'form-control'}))

    # DADOS DA CONTA BANCÁRIA

    banco = forms.CharField(required=False, widget=forms.Select(
        choices=bancos, attrs={'class': 'form-control'}))

    agencia = forms.CharField(required=False, max_length=4, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'AGÊNCIA'}))

    conta = forms.CharField(required=False, max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CONTA'}))

    digito_conta = forms.CharField(required=False, max_length=1, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DÍGITO DA CONTA'}))

    tipo_conta = forms.CharField(required=False, widget=forms.Select(
        choices=[("", "ESCOLHA UM TIPO"), ("Corrente", "CORRENTE"), ("Poupanca", "POUPANÇA")],
        attrs={'class': 'form-control'}))

    banco_favorecido = forms.CharField(required=False, widget=forms.Select(
        choices=bancos, attrs={'class': 'form-control'}))

    agencia_favorecido = forms.CharField(required=False, max_length=4, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'AGÊNCIA'}))

    conta_favorecido = forms.CharField(required=False, max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CONTA'}))

    digito_conta_favorecido = forms.CharField(required=False, max_length=1, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DÍGITO DA CONTA'}))

    cnpj_conta_favorecido = forms.CharField(required=False, max_length=14, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CNPJ FAVORECIDO (SOMENTE NÚMEROS)'}))

    razao_social_favorecido = forms.CharField(required=False, max_length=455, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'RAZÃO SOCIAL DO FAVORECIDO'}))

    tipo_conta_favorecido = forms.CharField(required=False, widget=forms.Select(
        choices=[("", "ESCOLHA UM TIPO"), ("Corrente", "CORRENTE"), ("Poupanca", "POUPANÇA")],
        attrs={'class': 'form-control'}))

    # maquina_cessao = forms.CharField(widget=forms.Select(
    #     choices=[("", "ESCOLHA"), ("venda", "VENDA"), ("aluguel", "ALUGUEL"), ("cessao", "CESSÃO")],
    #     attrs={'class': 'form-control'}))

    maquina_cessao = forms.CharField(widget=forms.HiddenInput())

    foto_documento = forms.FileField(required=False)

    # pagamento
    tipo_pagamento = forms.CharField(required=False, widget=forms.Select(
        choices=[("cartao", "CARTÃO")], attrs={'class': 'form-control'}))

    parcelamento = forms.CharField(required=False, widget=forms.Select(
        choices=parcelas, attrs={'class': 'form-control'}))

    nsu = forms.CharField(required=False, max_length=25, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPROVANTE DE PAGAMENTO'}))

    data_pagamento = forms.CharField(required=False, max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative datepicker',
               'placeholder': 'DATA EM QUE O PAGAMENTO FOI REALIZADO'}))

    # taxa_debito = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TAXA DE DÉBITO'}))
    # taxa_credito_a_vista = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TAXA DE CRÉDITO A VISTA'}))
    # taxa_credito_parcelado_emissor = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control form-control-alternative',
    #            'placeholder': 'TAXA DE CRÉDITO PARCELADO NO EMISSOR'}))
    # parcelado_loja_dois_seis = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PARCELADO NA LOJA PARCELA 2 A 6'}))
    # parcelado_loja_sete_doze = forms.CharField(widget=forms.TextInput(
    #     attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PARCELADO NA LOJA PARCELA 7 A 12'}))

    plano = forms.CharField(widget=forms.HiddenInput())

    nome_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NOME DO CONTATO RESPONSÁVEL'}))

    documento_responsavel = forms.CharField(required=True, max_length=11, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CPF RESPONSÁVEL'}))

    tipo_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[(9, "SÓCIO"), (10, "CONTATO")], attrs={'class': 'form-control'}))

    nascimento_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative datepicker', 'placeholder': 'DATA DE NASCIMENTO'}))

    sexo_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[("M", "MASCULINO"), ("F", "FEMININO")], attrs={'class': 'form-control'}))

    ddd_telefone_responsavel = forms.CharField(required=True, max_length=2, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DDD TELEFONE'}))

    telefone_responsavel = forms.CharField(required=True, max_length=9, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TELEFONE'}))

    email_responsavel = forms.CharField(required=True, max_length=250, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'EMAIL'}))

    nacionalidade_responsavel = forms.CharField(required=True, widget=forms.Select(
        choices=[("Brasileira", "Brasileira"), ("Estrangeira", "Estrangeira")], attrs={'class': 'form-control'}))

    lista = []
    departamentos = Departamentos.objects.all()
    #
    for departamento in departamentos:
        tupla = (departamento.codigo_departamento, departamento.descricao_departamento)
        lista.append(tupla)

    # PERFIL
    departamento_empresa = forms.CharField(widget=forms.Select(
        choices=lista, attrs={'class': 'form-control'}))

    operadora = forms.CharField(required=True, widget=forms.Select(
        choices=[("Claro", "Claro"), ("Oi", "Oi"), ("TIM", "TIM"), ("Vivo", "Vivo")], attrs={'class': 'form-control'}))

    # MAQUINAS
    num_maquinas = forms.IntegerField(required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NÚMERO DE MÁQUINAS',
               'type': 'number'}))

    # SPLIT

    split = forms.CharField(required=False, widget=forms.Select(
        choices=[("escolha", "Escolha"), ("sim", "Sim"), ("nao", "Não")], attrs={'class': 'form-control'}))

    porcentagem_cliente = forms.IntegerField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PORCENTAGEM %', 'type': 'number'}))

    porcentagem_favorecido = forms.IntegerField(required=False, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'PORCENTAGEM %', 'type': 'number'}))


class PagamentoForm(forms.Form):
    # pagamento
    tipo_pagamento_simples = forms.CharField(required=True, widget=forms.Select(
        choices=[("cartao", "CARTÃO")], attrs={'class': 'form-control'}))

    parcelamento_simples = forms.CharField(required=True, widget=forms.Select(
        choices=parcelas, attrs={'class': 'form-control'}))

    nsu_simples = forms.CharField(required=True, max_length=12, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'COMPROVANTE DE PAGAMENTO'}))

    hidden_client_id = forms.CharField(widget=forms.HiddenInput())

    data_pagamento_simples = forms.CharField(required=True, max_length=20, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative datepicker',
               'placeholder': 'DATA EM QUE O PAGAMENTO FOI REALIZADO'}))


class SenhaForm(forms.Form):
    senha_atual = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'Senha atual'}))
    nova_senha = forms.CharField(widget=forms.PasswordInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'Senha nova'}))


class ResetSenhaForm(forms.Form):
    email = forms.CharField(max_length=500, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'EMAIL'}))


class PlanoForm(forms.Form):
    # PLANOS

    listaprod = []
    produtos = Produtos.objects.all()
    for produto in produtos:
        tupla = (produto.id, produto.descricao)
        listaprod.append(tupla)

    pprint(listaprod)

    prod = forms.CharField(required=True, widget=forms.Select(
        choices=listaprod, attrs={'class': 'form-control'}))

    tipo_plano = forms.CharField(required=True, widget=forms.Select(
        choices=[("ALUGUEL", "ALUGUEL"), ("CESSAO", "CESSÃO"), ("VENDA", "VENDA")], attrs={'class': 'form-control'}))

    adquirente_plano = forms.CharField(required=True, widget=forms.Select(
        choices=[("ADIQ", "ADIQ"), ("REDE", "REDE")], attrs={'class': 'form-control'}))

    nome_plano = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'NOME DO PLANO'}))

    valor_adesao_plano = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'VALOR DE ADESÃO'}))

    taxa_debito = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TAXA DE DÉBITO'}))

    taxa_credito = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TAXA DE CRÉDITO'}))

    taxa_parcelado = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'TAXA CRÉDITO PARCELADO'}))

    # hidden_client_id = forms.CharField(widget=forms.HiddenInput())


class ProdutosForm(forms.Form):

    wifi = forms.BooleanField(required=False, initial=False, widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    gprs = forms.BooleanField(required=False, initial=False, widget=forms.CheckboxInput(attrs={'class': 'form-check-input'}))
    descricao = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DESCRIÇÃO DO PRODUTO'}))
    identificador = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'IDENTIFICADOR DO PRODUTO'}))


class DistribuidorForm(forms.Form):
    agencia = forms.CharField(required=True, max_length=15, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'AGÊNCIA'}))
    conta = forms.CharField(required=True, max_length=15, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CONTA'}))
    digito_conta = forms.CharField(required=True, max_length=1, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'DÍGITO DA CONTA'}))
    banco = forms.CharField(required=True, widget=forms.Select(
        choices=bancos, attrs={'class': 'form-control'}))
    tipo = forms.CharField(required=True, widget=forms.Select(
        choices=tipo_conta, attrs={'class': 'form-control'}))
    cnpj = forms.CharField(required=True, max_length=14, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'CNPJ'}))
    razao_social = forms.CharField(required=True, max_length=255, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-alternative', 'placeholder': 'RAZÃO SOCIAL'}))
