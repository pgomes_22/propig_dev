from rest_framework import serializers
from .models import Retorno4ward, Lojista, Terminais, LojistaTerminal
from pprint import pprint


class LojistaSerializer(serializers.Serializer):
    lojista_id = serializers.IntegerField(required=True)
    branch_id = serializers.CharField(required=True)
    affiliation_key = serializers.CharField(required=False)

    class Meta:
        model = Lojista
        fields = '__all__'

    def create(self, validated_data):
        pprint(validated_data)
        return Lojista.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.lojista_id = validated_data.get('lojista_id', instance.lojista_id)
        instance.branch_id = validated_data.get('branch_id', instance.branch_id)
        instance.affiliation_key = validated_data.get('affiliation_key', instance.affiliation_key)
        instance.save()
        return instance


class Retorno4wardSerializer(serializers.ModelSerializer):
    id_type = serializers.IntegerField(required=True)
    id_number = serializers.CharField(required=True)
    codigo_adquirente = serializers.IntegerField(required=True)
    status = serializers.IntegerField(required=True)
    lojista_id = LojistaSerializer(many=True, read_only=True)

    class Meta:
        model = Retorno4ward
        fields = '__all__'

    def create(self, validated_data):
        validated_data['lojista_id'] = self.context['lojista']
        return super(Retorno4wardSerializer, self).create(validated_data)
        return Retorno4ward.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.id_type = validated_data.get('id_type', instance.id_type)
        instance.id_number = validated_data.get('id_number', instance.id_number)
        instance.codigo_adquirente = validated_data.get('codigo_adquirente', instance.codigo_adquirente)
        instance.status = validated_data.get('status', instance.status)
        instance.save()
        return instance

class TerminaisSerializer(serializers.Serializer):
    terminal_id_logical = serializers.CharField(required=True)
    terminal_id_physical = serializers.CharField(required=True)
    tecnology_code = serializers.IntegerField(required=True)
    terminal_serial_number = serializers.CharField(required=True)

    def create(self, validated_data):
        return Terminais.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.terminal_id_logical = validated_data.get('terminal_id_logical', instance.terminal_id_logical)
        instance.terminal_id_physical = validated_data.get('terminal_id_physical', instance.terminal_id_physical)
        instance.tecnology_code = validated_data.get('tecnology_code', instance.tecnology_code)
        instance.terminal_serial_number = validated_data.get('terminal_serial_number', instance.terminal_serial_number)
        instance.save()
        return instance

class LojistaTerminalSerializer(serializers.ModelSerializer):
    lojista_id = LojistaSerializer(many=True, read_only=True)
    terminal_id = TerminaisSerializer(many=True, read_only=True)

    class Meta:
        model = LojistaTerminal
        fields = '__all__'

    def create(self, validate_data):
        validate_data['lojista_id'] = self.context['lojista_id']
        validate_data['terminal_id'] = self.context['terminal_id']
        return super(LojistaTerminalSerializer, self).create(validate_data)

    '''def update(self, instance, validated_data):
        instance.lojista_id = validated_data.get('lojista_id', instance.lojista_id)
        instance.terminal_id = validated_data.get('terminal_id', instance.terminal_id)
        instance.save()
        return instance'''